import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:async_builder/async_builder.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Notification.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/lets_encrypt.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

import 'firebase_options.dart';

void main() async {
  Logger.root.level = Level.ALL; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    print(
        '${record.level.name}: ${record.loggerName} ${record.time}: ${record.message}');
  });

  WidgetsFlutterBinding.ensureInitialized();

  try {
    SecurityContext.defaultContext.setTrustedCertificatesBytes(
      ascii.encode(LETS_ENCRYPT_CERTIFICATE),
    );
  } on TlsException catch (e) {
    if (e.osError?.message != null &&
        e.osError!.message.contains('CERT_ALREADY_IN_HASH_TABLE')) {
      print('cert already trusted! Skipping.');
    } else {
      print('setTrustedCertificateBytes EXCEPTION: $e');
      rethrow;
    }
  }

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;

  // Pass all uncaught asynchronous errors that aren't handled by the Flutter framework to Crashlytics
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };

  /// to catch all errors outside of flutter context
  Isolate.current.addErrorListener(RawReceivePort((pair) async {
    /// first is the exception, last is the stacktrace as a string
    /// refer to documentation for Isolate.addErrorListener for more information
    FirebaseCrashlytics.instance.recordError(
        pair.first, StackTrace.fromString(pair.last),
        reason: 'Uncaught error in current isolate');
  }).sendPort);

  runApp(PreetiBhojan());
}

class AuthenticationFailed implements Exception {}

class PhoneNumberOrPasswordNotFound implements Exception {}

Future<User> getCredentialsAndAuthenticate() async {
  final _log = Logger('GetCredentialsAndAuthenticate');
  _log.info('getting user');

  _log.info('getting token');

  /// This operation fails on Android 7.0 and older.
  /// Thats fine. Let the app function without notifications
  try {
    await initNotification();
  } catch (e) {}

  final user = await getNullableUser();

  if (user == null) {
    _log.severe('Phone number or password not found');
    throw PhoneNumberOrPasswordNotFound();
  }

  _log.info('authenticating');
  final userType = await authenticate(user);

  if (userType != user.type) {
    throw AuthenticationFailed();
  }

  if (userType == UserType.Customer) {
    final temp = jsonDecode((await isRegularCustomer(user)).body)['result'];

    if (temp != user.isRegularCustomer) {
      final storage = FlutterSecureStorage();

      await storage.write(
        key: 'is_regular_customer',
        value: temp.toString(),
      );
    }

    _log.info('isRegularCustomer ${user.isRegularCustomer}');
  }

  return user;
}

Future<http.Response> isRegularCustomer(User user) async {
  final _log = Logger('isRegularCustomer');
  final response = await http.post(
    Uri.parse(base_url + '/am_i_regular_customer'),
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(
      {
        'phone_number': user.phoneNumber.trim(),
        'password': user.password.trim(),
      },
    ),
  );

  _log.info('status code: ${response.statusCode}');
  _log.info('response: ${response.body}');

  if (response.statusCode != 200) {
    throw StateError(
        'Under no circumstance should isRegularCustomer fail given, customer '
        'is already authenticated');
  }
  return response;
}

Future<UserType> authenticate(User user) async {
  final _log = Logger('authenticate');

  _log.info('authenticating user');
  print('token in authenticate: ${notification.token}');
  final response = await http.post(
    Uri.parse(base_url + '/authenticate'),
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode({
      'phone_number': user.phoneNumber.trim(),
      'password': user.password.trim(),
      'fcm_token': notification.token,
      'apns_token': notification.apnsToken,
      'version': version,
    }),
  );

  _log.info('status code: ${response.statusCode}');
  _log.info('response: ${response.body}');

  if (response.statusCode != 200) {
    return Future.error(AuthenticationFailed());
  }

  final json = jsonDecode(response.body);

  final type = UserType.fromString(json['type']);

  if (type == UserType.Customer) {
    /// initialize config if user type is customer
    config = Config.fromJson(json['config']);
    await FlutterSecureStorage().write(
      key: 'is_regular_customer',
      value: json['is_regular_customer'].toString(),
    );
  }
  if (type == UserType.Staff) {
    setAdminConfigFromJson(json['config']);
    staffPermissions = StaffPermissions.fromJson(json['permissions']);
  }

  return type;
}

class PreetiBhojan extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _PreetiBhojanState createState() => _PreetiBhojanState();
}

class _PreetiBhojanState extends State<PreetiBhojan> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme.apply(
          bodyColor: Colors.white,
          displayColor: Colors.white,
        );

    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Preeti Bhojan',
      theme: ThemeData(
        canvasColor: secondaryColor,
        cardColor: Colors.transparent,
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: secondaryColor,
        ),
        checkboxTheme: CheckboxThemeData(
          checkColor: MaterialStateColor.resolveWith((states) => Colors.white),
          fillColor: MaterialStateColor.resolveWith((states) => primaryColor),
          side: MaterialStateBorderSide.resolveWith(
              (states) => BorderSide(color: Colors.white)),
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: secondaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          textTheme: ButtonTextTheme.accent,
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: secondaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            foregroundColor: Colors.white,
          ),
        ),
        dividerTheme: DividerThemeData(
          color: Colors.white,
          thickness: 2,
        ),
        primaryColor: primaryColor,
        scaffoldBackgroundColor: primaryColor,
        textTheme: textTheme,
        inputDecorationTheme: const InputDecorationTheme(
          labelStyle: TextStyle(color: Colors.white),
          hintStyle: TextStyle(color: Colors.white),
        ),
        dialogBackgroundColor: secondaryColor,
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          selectedLabelStyle: TextStyle(color: primaryColor),
          selectedItemColor: primaryColor,
          unselectedLabelStyle: TextStyle(color: Colors.white),
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
        ),
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.blue,
          accentColor: Colors.white,
        )
            .copyWith(
              secondary: Colors.white,
              brightness: Brightness.dark,
            )
            .copyWith(error: Colors.white),
      ),
      home: _InitialPage(),
    );
  }
}

class _InitialPage extends StatefulWidget {
  @override
  _InitialPageState createState() => _InitialPageState();
}

class _InitialPageState extends State<_InitialPage> {
  late final Future<void> _future = _init();

  Future<void> _init() async {
    try {
      final user = await getCredentialsAndAuthenticate();

      Navigator.of(context).pop();
      if (user.type == UserType.Customer) {
        navigateToCustomerHome();
      } else {
        navigateToAdminHome();
      }
    } on PhoneNumberOrPasswordNotFound catch (e) {
      _log.warning('Could not get user or authentication failed $e');
      Navigator.of(context).pop();
      navigateToLogin();
    } on AuthenticationFailed catch (e) {
      _log.warning('Could not get user or authentication failed $e');
      Navigator.of(context).pop();
      navigateToLogin();
    } catch (e) {
      _log.warning('Could not get user or authentication failed $e');
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );
    }
  }

  final _log = Logger('InitialPageState');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar,
      body: AsyncBuilder<void>(
        future: _future,
        error: (context, _, __) => CustomBackground(
          child: Center(
            child: Text(
              'Something went wrong while authenticating',
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headlineSmall!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        waiting: (context) => CustomBackground(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
        builder: (context, _) => CustomBackground(
          child: Container(),
        ),
      ),
    );
  }
}
