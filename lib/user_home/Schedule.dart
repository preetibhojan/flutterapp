import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import 'Schedule/MealTypeCard.dart';
import 'Schedule/Models.dart';
import 'Schedule/Title.dart';
import 'Schedule/ViewModel.dart';

class ScheduleScreen extends StatefulWidget {
  final ScheduleViewModel viewModel;
  final DeliverableRepository deliverablesRepository;
  final Meal? mealToStart;

  const ScheduleScreen(
    this.viewModel,
    this.deliverablesRepository, {
    this.mealToStart,
  });

  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  static final _log = Logger('Schedule');

  bool showCircularProgressIndicator = false;

  late Future _future;

  @override
  void initState() {
    super.initState();
    _future = widget.viewModel.fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: widget.viewModel,
      child: CustomScaffold(
        showCircularProgressIndicator: showCircularProgressIndicator,
        body: FutureBuilder(
          future: _future,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            _log.info(snapshot);
            if (snapshot.hasError) {
              _log.severe('Something went wrong: ', snapshot.error.toString());
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                    context: context, builder: (_) => SomethingWentWrong());
              });

              return CustomBackground(
                child: Container(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done) {
              return _SchedulePage(
                widget.viewModel,
                showProgressIndicator: (value) {
                  setState(() {
                    showCircularProgressIndicator = value;
                  });
                },
                mealToStart: widget.mealToStart,
              );
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}

class _SchedulePage extends StatefulWidget {
  final ScheduleViewModel viewModel;
  final Function(bool) showProgressIndicator;
  final Meal? mealToStart;

  const _SchedulePage(
    this.viewModel, {
    required this.showProgressIndicator,
    this.mealToStart,
  });

  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<_SchedulePage> {
  static const MinimumDaysAtLeastOnePackageNeedsToBeScheduled = 3;
  static final _log = Logger('Schedule');
  PageController controller = PageController();

  @override
  void initState() {
    super.initState();

    if (widget.mealToStart != null) {
      int index =
          widget.viewModel.mealInfoManager.keys.indexOf(widget.mealToStart!);

      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        controller.jumpToPage(index);
      });
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: controller,
      children: widget.viewModel.mealInfoManager.keys.map((meal) {
        return ListView(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Text(
                  widget.viewModel.isRegularCustomer
                      ? 'Re Schedule'
                      : 'Schedule',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            ),
            Consumer<ScheduleViewModel>(
              builder: (context, scheduleViewModel, _) {
                return CardTile(
                  title: TitleWithSwitch(meal),
                  child: Column(
                    children: <Widget>[
                      MealTypeCard(
                        meal: meal,
                        enabled:
                            scheduleViewModel.mealInfoManager[meal]!.required,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          child: Text('Next'),
                          onPressed: () {
                            onNextPage(meal);
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
            )
          ],
        );
      }).toList(),
    );
  }

  void onNextPage(Meal meal) {
    if (isLastPage()) {
      schedule();
      return;
    }

    final mealInfo = widget.viewModel.mealInfoManager[meal]!;

    if (mealInfo.required) {
      if (mealInfo.requiredPackages.isEmpty) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text(
              'Please select package before continuing',
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }

      if (mealInfo.deliveryInfo.values
              .where((deliveryLocation) => deliveryLocation == null)
              .length ==
          mealInfo.deliveryInfo.length) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content:
                Text('Please set one delivery location for at least 1 day'),
            actions: <Widget>[
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }
    }

    controller.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  bool isLastPage() {
    return controller.page!.toInt() ==
        widget.viewModel.mealInfoManager.length - 1;
  }

  void schedule() async {
    _log.info('Finding required meals');

    final requiredMeals = widget.viewModel.mealInfoManager.internal.entries
        .where((element) => element.value.required)
        .toList();

    _log.info('Required meals: $requiredMeals');

    if (requiredMeals.length == 0) {
      _log.info('Required meals empty');

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Oops'),
          content: Text(
            'Please enable at least one meal by clicking on the switch '
            'at the top right',
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );

      return;
    }

    final daysScheduledFor = <Meal, int>{};

    for (final meal in requiredMeals) {
      if (meal.value.requiredPackages.isEmpty) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text('Please select package for ${meal.key.name}'),
            actions: <Widget>[
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }

      final daysRequired = meal.value.deliveryInfo.entries
          .where(
            (entry) =>
                entry.value != null &&
                entry.value != DeliveryLocation.NotRequired,
          )
          .length;

      if (daysRequired == 0) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text(
              'Please select delivery location for at least one day for '
              '${meal.key.name}',
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }

      daysScheduledFor[meal.key] = daysRequired;
    }

    final maxDaysScheduledFor = daysScheduledFor.values.reduce(max);

    if (maxDaysScheduledFor < MinimumDaysAtLeastOnePackageNeedsToBeScheduled) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Oops'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'At least one meal must be ordered for more than '
                '$MinimumDaysAtLeastOnePackageNeedsToBeScheduled days.',
              ),
              SizedBox(height: 8),
              Text(
                'Please book this week\'s meals requirement by ordering '
                'on a daily basis',
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );

      return;
    }

    setState(() {
      widget.showProgressIndicator(true);
    });

    _log.info('Get user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/save_schedule'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(makeRequest(user, requiredMeals)),
    );

    setState(() {
      widget.showProgressIndicator(false);
    });

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Something went wrong while saving schedule');

      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    navigateToBooking();
  }

  Map<String, dynamic> makeRequest(
    User user,
    List<MapEntry<Meal, MealInfo>> requiredMeals,
  ) {
    final packages = [];
    final result = <String, dynamic>{
      'phone_number': user.phoneNumber,
      'password': user.password,
    };

    /// find the meals where required is true
    requiredMeals.forEach((mapEntry) {
      final mealInfo = mapEntry.value;
      final deliveryInfo = [];

      mealInfo.deliveryInfo.entries
          .where((entry) =>
              entry.value != null &&
              entry.value != DeliveryLocation.NotRequired)
          .forEach((entry) {
        final day = entry.key;
        final deliveryLocation = entry.value;

        if (deliveryLocation != null &&
            deliveryLocation != DeliveryLocation.NotRequired) {
          deliveryInfo.add({
            'day': day.weekday,
            'delivery_location': deliveryLocation.toInt(),
          });
        }
      });

      mealInfo.requiredPackages.forEach((package) {
        packages.add({
          'package_id': package.id,
          'delivery_info': deliveryInfo,
          'quantity': package.quantityAvailable!,
        });
      });
    });

    result['schedule'] = packages;

    return result;
  }
}
