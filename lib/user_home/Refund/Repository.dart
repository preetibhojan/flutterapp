import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

typedef Future<MoneyAvailable> GetCredit();

final _log = Logger('Refund repository');

class CouldNotFetchCredit extends Error {}

class MoneyAvailable {
  final double credit, deposit;

  MoneyAvailable(this.credit, this.deposit);

  factory MoneyAvailable.fromJson(Map<String, dynamic> json) {
    return MoneyAvailable(
      json['credit'],
      json['deposit'],
    );
  }
}

Future<MoneyAvailable> getCredit() async {
  _log.info('getting user');
  final user = await getUser();

  _log.info('sending post request to credit');
  final response = await http.post(
    Uri.parse(base_url + '/credit'),
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode({
      'phone_number': user.phoneNumber,
      'password': user.password,
    }),
  );

  _log.info('status code: ${response.statusCode}');
  _log.info('response: ${response.body}');

  if (response.statusCode != 200) {
    throw CouldNotFetchCredit();
  }

  return MoneyAvailable.fromJson(jsonDecode(response.body));
}
