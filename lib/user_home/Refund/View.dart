import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/user_home/Refund/Repository.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:validators/validators.dart';

class Refund extends StatefulWidget {
  final GetCredit getCredit;

  const Refund(this.getCredit);

  @override
  _RefundState createState() => _RefundState();
}

enum RefundMode {
  UPI,
  NetBanking,
}

enum AccountType {
  Saving,
  Current,
}

class _RefundState extends State<Refund> {
  final _upi = TextEditingController();

  AccountType _accountType = AccountType.Saving;

  final _accountHolderName = TextEditingController(),
      _accountNumber = TextEditingController(),
      _bank = TextEditingController(),
      _ifscCode = TextEditingController();

  RefundMode _refundMode = RefundMode.UPI;

  @override
  void dispose() {
    _upi.dispose();
    _accountHolderName.dispose();
    _accountNumber.dispose();
    _bank.dispose();
    _ifscCode.dispose();

    super.dispose();
  }

  static final _log = Logger('Refund');

  final _netBankingFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(fontSize: 18),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            RadioListTile(
              value: RefundMode.UPI,
              groupValue: _refundMode,
              title: Text('UPI'),
              onChanged: (dynamic value) {
                setState(() {
                  _refundMode = value;
                });
              },
            ),
            Visibility(
              visible: _refundMode == RefundMode.UPI,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                child: TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _upi,
                  decoration: InputDecoration(
                    labelText: 'UPI ID',
                  ),
                  validator: (str) => str!.trim().isValidUPIid
                      ? null
                      : 'Please enter a valid UPI id',
                ),
              ),
            ),
            RadioListTile(
              value: RefundMode.NetBanking,
              groupValue: _refundMode,
              title: Text('Net Banking'),
              onChanged: (dynamic value) {
                setState(() {
                  _refundMode = value;
                });
              },
            ),
            Visibility(
              visible: _refundMode == RefundMode.NetBanking,
              child: Form(
                key: _netBankingFormKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _accountHolderName,
                        decoration: InputDecoration(
                          labelText: 'Account Holder Name',
                        ),
                        textCapitalization: TextCapitalization.words,
                        validator: (str) =>
                            str!.trim().isAlphabeticWithSpaces
                                ? null
                                : 'Account holder name must contain only'
                                    ' A-Z, a-z and spaces',
                      ),
                      TextFormField(
                        controller: _accountNumber,
                        decoration: InputDecoration(
                          labelText: 'Account Number',
                        ),
                        validator: (str) => isAlphanumeric(str!.trim()) &&
                                str.trim().length >= 11
                            ? null
                            : 'Account number must contain only A-Z,'
                                ' a-z and 0-9 and must be 11 '
                                'digits at least',
                      ),
                      TextFormField(
                        controller: _bank,
                        decoration: InputDecoration(
                          labelText: 'Bank Name',
                        ),
                        textCapitalization: TextCapitalization.words,
                        validator: (str) =>
                            isAlphaNumericUnderScoreOrSpace(str!.trim())
                                ? null
                                : 'Bank name must only contain A-Z, '
                                    'a-z, 0-9 and spaces',
                      ),
                      TextFormField(
                        controller: _ifscCode,
                        decoration: InputDecoration(
                          labelText: 'IFSC Code',
                        ),
                        validator: (str) => isAlphanumeric(str!.trim()) &&
                                str.trim().length == 11
                            ? null
                            : 'IFSC must contain only A-Z, a-z and'
                                ' 0-9 and must be 11 digits',
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                        child: Text(
                          'Select Account Type',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      RadioListTile(
                        onChanged: (dynamic value) {
                          _accountType = value;
                        },
                        groupValue: _accountType,
                        value: AccountType.Saving,
                        title: Text('Saving'),
                      ),
                      RadioListTile(
                        onChanged: (dynamic value) {
                          _accountType = value;
                        },
                        groupValue: _accountType,
                        value: AccountType.Current,
                        title: Text('Current'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  child: Text('Refund Balance'),
                  onPressed: _onRefund,
                ),
                ElevatedButton(
                  onPressed: _onDiscontinueServices,
                  child: Text('Discontinue Services'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _onRefund() async {
    if (!_validateForm()) {
      return;
    }

    final success = await _refund();

    if (success) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Refund request submitted successfully'),
          content: Text(
            'As per our refund policy, you will be refunded within '
            '7 working days after due checking',
          ),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        ),
      );
    }
  }

  Future<bool> _refund() async {
    _log.info('getting user to refund');
    final user = await getUser();

    final body = _refundMode == RefundMode.UPI
        ? _createUPIBody(user)
        : _createNetBankingBody(user);

    final response = await http.post(
      Uri.parse(base_url + '/refund'),
      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return false;
    }

    return true;
  }

  Future<bool> _discontinueService() async {
    _log.info('getting user to refund');
    final user = await getUser();

    final body = _refundMode == RefundMode.UPI
        ? _createUPIBody(user)
        : _createNetBankingBody(user);

    final response = await http.post(
      Uri.parse(base_url + '/discontinue_service'),
      headers: {'Content-Type': 'application/json'},
      body: body,
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return false;
    }

    return true;
  }

  void _showEnterValidNetBankingDetails() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Oops'),
        content: Text('Please correct the errors in net banking fields'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  void _showEnterValidUPIid() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Oops'),
        content: Text('Please enter a valid upi id'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
  
  bool _validateForm() {
    if (_refundMode == RefundMode.UPI && !_upi.text.isValidUPIid) {
      _showEnterValidUPIid();

      return false;
    }

    if (_refundMode == RefundMode.NetBanking &&
        !_netBankingFormKey.currentState!.validate()) {
      _showEnterValidNetBankingDetails();

      return false;
    }

    return true;
  }

  void _onDiscontinueServices() async {
    // todo: make non barrier dismissible
    final sure = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Are you sure?'),
        content: Text(
          'By discontinuing service, you will no longer be a '
          'Regular Customer and will be charged Need Based Customer '
          'rates for future orders.',
        ),
        actions: [
          TextButton(
            child: Text('Yes'),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
          TextButton(
            child: Text('No'),
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        ],
      ),
    );

    if (sure == null || !sure) {
      return;
    }

    final success = await _discontinueService();

    if (success) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('You are no more a regular customer'),
          content: Text(
            'As per our refund policy, you will be refunded within '
            '7 working days after due checking',
          ),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        ),
      );
    }
  }

  String _createUPIBody(User user) {
    return jsonEncode({
      'phone_number': user.phoneNumber,
      'password': user.password,
      'payment_mode': RefundMode.UPI.index,
      'upi_id': _upi.text,
    });
  }

  String _createNetBankingBody(User user) {
    return jsonEncode({
      'phone_number': user.phoneNumber,
      'password': user.password,
      'payment_mode': RefundMode.NetBanking.index,
      'net_banking_details': {
        'acc_holder_name': _accountHolderName.text.trim(),
        'acc_number': _accountNumber.text.trim(),
        'acc_type': _accountType.index,
        'bank': _bank.text.trim(),
        'ifsc_code': _ifscCode.text.trim(),
      }
    });
  }
}
