import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:http/http.dart' as http;
import 'package:collection/collection.dart';
import 'package:preetibhojan/util/datetime.dart';

class CartItem {
  final Package package;
  int quantity;

  CartItem(
    this.package,
    this.quantity,
  );

  CartItem clone() {
    return CartItem(package, quantity);
  }

  @override
  String toString() {
    return 'CartItem{package: $package, quantity: $quantity}';
  }

  Map<String, dynamic> toJson() {
    return {
      'package_id': package.id,
      'quantity': quantity,
    };
  }
}

typedef _DeliveryCharge = Map<DeliveryLocation, int>;

class Cart {
  final items = SplayTreeMap<Meal, List<CartItem>>();
  final deliveryLocationOf = <Meal, DeliveryLocation>{};

  final PackageRepository _packageRepository;
  final MealsRepository _mealsRepository;
  final DeliveryCharges _deliveryChargesRepository;
  final DateTime now;

  Map<int, _DeliveryCharge> deliveryChargeOf = {};
  List<Package> _packages = [];

  Cart(
    this._packageRepository,
    this._mealsRepository,
    this._deliveryChargesRepository,
    this.now,
  );

  Future<void> loadData() async {
    _deliveryChargesRepository.invalidateCache();
    _packageRepository.invalidateCache();

    final meals = await _mealsRepository.meals;

    final date = setTimeToZero(dateToUse(meals, now));

    _packages = (await _packageRepository.packages)
        .where((package) => package.date == date)
        .toList();

    deliveryChargeOf = await _deliveryChargesRepository.deliveryCharges;

    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/cart2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    if (response.statusCode != 200) {
      throw StateError('Could not get cart: ${response.body}');
    }

    final json = jsonDecode(response.body);

    deliveryLocationOf.clear();
    for (final element in json['delivery_locations']) {
      final meal = meals.firstWhereOrNull((m) => m.id == element['meal_id']);
      if (meal == null) {
        continue;
      }
      final deliveryLocation =
          DeliveryLocation.fromInt(element['delivery_location']);
      if (deliveryLocation != null) {
        deliveryLocationOf[meal] = deliveryLocation;
      }
    }

    items.clear();
    for (final item in json['packages']) {
      final package = _packages.firstWhereOrNull(
        (package) => package.id == item['package_id'],
      );

      /// Package can be null if package added to cart is no more available.
      /// In that case, skip the package.
      if (package == null || package.quantityAvailable! == 0) {
        _deleteInternal(item['package_id']);
        continue;
      }

      final meal = meals.firstWhereOrNull((meal) => meal.id == package.mealId);
      if (meal == null) {
        _deleteInternal(item['package_id']);
        continue;
      }

      /// Adds ons do not have delivery charge.
      /// So add delivery charge only when package is not add on.
      final cartItem = CartItem(
        package,

        /// if quantity in cart is less than quantity available, update cart
        /// accordingly
        min(item['quantity'], package.quantityAvailable!),
      );

      if (items[meal] == null) {
        items[meal] = [cartItem];
      } else {
        items[meal]!.add(cartItem);
      }
    }
  }

  void set(Meal meal, Package package, int quantity) async {
    if (items[meal] == null) {
      items[meal] = [CartItem(package, quantity)];
    } else {
      final item =
          items[meal]!.firstWhereOrNull((item) => item.package == package);
      if (item == null) {
        items[meal]!.add(CartItem(package, quantity));
      } else {
        item.quantity = quantity;
      }
    }

    final user = await getUser();

    final response = await http.put(
      Uri.parse(base_url + '/cart'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'package_id': package.id,
        'quantity': quantity,
      }),
    );

    if (response.statusCode != 200) {
      throw StateError('Could not update cart: ${response.body}');
    }
  }

  void setDeliveryLocationOf(
      Meal meal, DeliveryLocation deliveryLocation) async {
    deliveryLocationOf[meal] = deliveryLocation;

    final user = await getUser();
    final response = await http.put(
      Uri.parse(base_url + '/cart/delivery_location'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'meal_id': meal.id,
        'delivery_location': deliveryLocation.toInt(),
      }),
    );

    if (response.statusCode != 200) {
      throw StateError('Could not set delivery location: ${response.body}');
    }
  }

  void delete(Meal meal, Package package) async {
    items[meal]!.removeWhere((item) => item.package == package);

    await _deleteInternal(package.id);
  }

  Future<void> _deleteInternal(int packageID) async {
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/delete_from_cart'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'package_id': packageID,
      }),
    );

    if (response.statusCode != 200) {
      throw StateError(
          'Could not delete $packageID from cart: ${response.body}');
    }
  }

  List<Map<String, dynamic>> toJson() {
    final temp = <Map<String, dynamic>>[];
    items.forEach((meal, values) {
      for (final value in values) {
        temp.add({
          'package_id': value.package.id,
          'quantity': value.quantity,
          'delivery_location': deliveryLocationOf[meal],
        });
      }
    });

    return temp;
  }

  void clear() async {
    items.clear();
    deliveryLocationOf.clear();

    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/cart2/clear'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      })
    );

    if (response.statusCode != 200) {
      throw StateError('Could not clear cart');
    }
  }
}
