import 'dart:convert';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/deliverables/Model.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/common/schedule/Models.dart';
import 'package:preetibhojan/common/schedule/Repositories.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/user_home/Payment/Model.dart';
import 'package:preetibhojan/user_home/Payment/PayTM/View.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/FlexTable.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

import 'Models.dart';

class CartScreen extends StatefulWidget {
  final DeliveryLocationsRepository deliveryLocationsRepository;
  final DeliverableRepository deliverableRepository;
  final ScheduleRepository scheduleRepository;
  final MealsRepository mealsRepository;
  final Cart cart;
  final DateTime now;

  CartScreen(
    this.deliveryLocationsRepository,
    this.deliverableRepository,
    this.scheduleRepository,
    this.mealsRepository,
    this.cart,
    this.now,
  );

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  bool showCircularProgressIndicator = false;

  late Future<void> future;

  bool isRegularCustomer = false;

  static final _log = Logger('Cart Screen');

  @override
  void initState() {
    super.initState();
    future = init();
  }

  late List<Deliverable> _deliverables;
  late Schedule _schedule;
  late List<Meal> _meals;

  Future<void> init() async {
    _deliverables = await widget.deliverableRepository.deliverablesFromToday;

    _schedule = await widget.scheduleRepository.schedule;

    _meals = await widget.mealsRepository.meals;

    isRegularCustomer = (await getUser()).isRegularCustomer;
  }

  double totalIncludingTax = 0;

  final currentDeliveryCharge = <Meal, int>{};

  bool loadingData = false;

  @override
  Widget build(BuildContext context) {
    if (loadingData) {
      return CustomScaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return FutureBuilder(
      future: future,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        _log.info(snapshot);

        if (snapshot.hasError) {
          if (snapshot.error is Error) {
            _log.severe(
              'Something went wrong',
              (snapshot.error as Error).stackTrace,
            );
          } else {
            _log.severe('Something went wrong', snapshot.error);
          }

          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
                context: context, builder: (context) => SomethingWentWrong());
          });

          return CustomScaffold(
            body: Container(),
          );
        }

        if (snapshot.connectionState == ConnectionState.done) {
          /// normalPackageTotal and addOnTotal need to be separate
          /// because only normalPackageTotal is taxable. Add ons
          /// are not taxable.
          final normalPackageTotal = <Meal, double>{},
              addOnTotal = <Meal, double>{};

          for (final entry in widget.cart.items.entries) {
            var temp = entry.value.where((item) => !item.package.addOn);

            final meal = entry.key;
            if (temp.length > 0) {
              normalPackageTotal[meal] = temp
                  .map((item) => (item.package.price + widget.cart.deliveryChargeOf[meal.id]![widget.cart.deliveryLocationOf[meal]]!) * item.quantity)
                  .reduce((value, element) => value + element);
            } else {
              normalPackageTotal[meal] = 0;
            }

            temp = entry.value.where((item) => item.package.addOn);

            if (temp.length > 0) {
              addOnTotal[meal] = temp
                  .map((item) => item.package.price * item.quantity)
                  .reduce((value, element) => value + element);
            } else {
              addOnTotal[meal] = 0;
            }
          }

          final now = dateToUse(_meals, widget.now);

          /// Deliverables by default returns only ordered.
          /// And when order before is over, the status of
          /// deliverables is set to ordered. So we can
          /// just filter by date
          final deliverables = _deliverables
              .where(
                (deliverable) =>
                    deliverable.date.isAtSameMomentAs(setTimeToZero(now)),
              )
              .toList();

          final paid = _schedule.creditAvailable;

          final sumOfComplementaryPackage = (List<CartItem> cis) {
            final temp = cis
                .where((ci) => ci.package.addOn && ci.package.complimentary!);

            if (temp.length == 0) {
              return 0;
            }

            return temp.map((ci) => ci.package.price).reduce((a, b) => a + b);
          };

          var complementaryPackageDiscount = 0;

          for (final cartItems in widget.cart.items.values) {
            complementaryPackageDiscount +=
                sumOfComplementaryPackage(cartItems) as int;
          }

          /// If customer if regular, give them need based customer delta
          /// discount per meal
          final regularCustomerDiscount = <Meal, double>{};
          if (isRegularCustomer) {
            final quantities = <int>[];
            _schedule.packages.forEach((meal, packages) {
              quantities
                  .add(packages.map((p) => p.quantity).reduce((a, b) => a + b));
            });

            final quantitiesToDiscountPerMeal =
                quantities.isEmpty ? 0 : quantities.reduce((a, b) => max(a, b));

            for (final entry in widget.cart.items.entries) {
              final quantityToBook = entry.value
                  .where((c) => !c.package.addOn)
                  .map((c) => c.quantity)
                  .reduce((a, b) => a + b);

              final temp = deliverables
                  .where((d) => !d.addOn && d.mealID == entry.key.id)
                  .map((d) => d.quantity)
                  .toList();

              final quantityBooked =
                  temp.isEmpty ? 0 : temp.reduce((a, b) => a + b);

              final count = min(quantityToBook, quantitiesToDiscountPerMeal) -
                  quantityBooked;

              if (count > 0) {
                regularCustomerDiscount[entry.key] =
                    config.needBasedCustomerDelta[entry.key.id]! * count;
              } else {
                regularCustomerDiscount[entry.key] = 0;
              }
            }
          } else {
            for (final meal in widget.cart.items.keys) {
              regularCustomerDiscount[meal] = 0;
            }
          }

          assert(normalPackageTotal.length == regularCustomerDiscount.length);

          double tax = 0;
          for (final meal in widget.cart.items.keys) {
            tax +=
                (normalPackageTotal[meal]! - regularCustomerDiscount[meal]!) *
                    config.gst[widget.cart.deliveryLocationOf[meal]]! /
                    100;
          }

          totalIncludingTax = normalPackageTotal.values.sum +
              tax +
              addOnTotal.values.sum -
              paid -
              regularCustomerDiscount.values.sum;

          return CustomScaffold(
            showCircularProgressIndicator: showCircularProgressIndicator,
            body: ListView(
              children: [
                Visibility(
                  visible: widget.cart.items.length == 0,
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(80, 50, 80, 150),
                    alignment: Alignment.center,
                    child: Text(
                      'Cart is currently empty',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium!
                          .copyWith(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                ...widget.cart.items.entries.map(
                  (cartItems) {
                    final meal = cartItems.key;

                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                meal.name,
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall!
                                    .copyWith(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                            child: Row(
                              children: [
                                Text(
                                  'Delivery Location: ',
                                  style: Theme.of(context).textTheme.titleLarge,
                                ),
                                Spacer(),
                                Text(
                                  widget.cart.deliveryLocationOf[meal]!.name,
                                  style: Theme.of(context).textTheme.titleLarge,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(16),
                            child: FlexTable(
                              cellAlignments: [
                                Alignment.centerLeft,
                                Alignment.center,
                                Alignment.centerRight,
                              ],
                              flexFactors: [4, 5, 3],
                              headings: [
                                Text(
                                  'Package',
                                  style: Theme.of(context).textTheme.titleLarge,
                                ),
                                Text(
                                  'Quantity',
                                  style: Theme.of(context).textTheme.titleLarge,
                                ),
                                Text(
                                  'Price',
                                  style: Theme.of(context).textTheme.titleLarge,
                                ),
                              ],
                              rows: cartItems.value
                                  .map(
                                    (cartItem) => [
                                      Text(
                                        cartItem.package.name,
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        '${cartItem.quantity}',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        '$rupeeSymbol ${((cartItem.package.price + widget.cart.deliveryChargeOf[meal.id]![widget.cart.deliveryLocationOf[meal]]!) * cartItem.quantity).toStringAsFixed(2)}',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.right,
                                      ),
                                    ],
                                  )
                                  .toList(),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ).toList(),
                DefaultTextStyle(
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 20, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text('Total   : '),
                            ),
                            Visibility(
                              visible: isRegularCustomer,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text('Regular Customer Discount   : '),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text('Paid   : '),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text('Tax   : '),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text('Final Bill Amount   : '),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                  '  $rupeeSymbol ${(normalPackageTotal.values.sum + addOnTotal.values.sum).toStringAsFixed(2)}'),
                            ),
                            Visibility(
                              visible: isRegularCustomer,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                    '-  $rupeeSymbol ${(regularCustomerDiscount.values.sum + complementaryPackageDiscount).toStringAsFixed(2)}'),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                  '  $rupeeSymbol ${paid.toStringAsFixed(2)}'),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                  '  $rupeeSymbol ${tax.toStringAsFixed(2)}'),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                  '  $rupeeSymbol ${totalIncludingTax.toStringAsFixed(2)}'),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    if (widget.cart.items.length != 0)
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          child: Text(
                            totalIncludingTax > 0 ? 'Proceed to Pay' : 'Book',
                          ),
                          onPressed: () => _onBook(widget.cart, now),
                        ),
                      ),
                  ],
                ),
              ],
            ),
          );
        }

        return CustomScaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  void _onBook(Cart cart, DateTime now) async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    _log.info('Getting user');
    final user = await getUser();

    late ReservationResult reservationResult;
    try {
      final nextWorkingDay =
          setTimeToZero(now) != setTimeToZero(DateTime.now());
      reservationResult = await _reserve(
        cart,
        user,
        nextWorkingDay: nextWorkingDay,
      );

      /// This could have been an exception too.
      /// But then it would be hard to distinguish
      /// between an error in reserve and failure
      /// to reserve.
      if (!reservationResult.success) {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text(
              'Some packages could not be ordered. '
              'Click on okay to refresh quantities',
            ),
            actions: [
              TextButton(
                child: Text('Okay'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  setState(() {
                    loadingData = true;
                  });
                  await widget.cart.loadData();
                  setState(() {
                    loadingData = false;
                  });
                },
              ),
            ],
          ),
        );

        return;
      }

      if (totalIncludingTax > 0) {
        /// when the backend receives the payment, it automatically
        /// books the packages
        await _pay(totalIncludingTax, reservationResult.reservationID!);
      } else {
        /// but when the net amount is negative, we never pay. So
        /// we need to book manually
        await _book(reservationResult.reservationID!);
      }

      cart.clear();

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Success'),
          content: Text('Tiffin Booked successfully'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            )
          ],
        ),
      );
    } on DeliveryLocationNotSet catch (exception) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops!'),
          content: Text(
            'Please set the delivery location for '
            '${exception.meal.name}',
          ),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } on CouldNotReserve {
      _log.info('Something went wrong in reservation');
      showDialog(
        context: context,
        builder: (context) => SomethingWentWrong(),
      );
    } on CouldNotBook {
      _log.info('Something went wrong while booking');
      showDialog(
        context: context,
        builder: (context) => SomethingWentWrong(),
      );
      await _cancelReservation(user, reservationResult.reservationID!);
    } on CouldNotPay {
      _log.info('Something went wrong in payment');
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Payment Failed'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
      await _cancelReservation(user, reservationResult.reservationID!);
    } on CouldNotInitiateTransaction {
      _log.info('Something went wrong in payment');
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Payment Failed'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
      await _cancelReservation(user, reservationResult.reservationID!);
    } catch (exception, stacktrace) {
      print(exception);
      print(stacktrace);
      _log.severe('Something went wrong', exception);
      showDialog(
        context: context,
        builder: (context) => SomethingWentWrong(),
      );
    } finally {
      setState(() {
        showCircularProgressIndicator = false;
      });
    }
  }

  Future<void> _pay(double totalIncludingTax, String reservationID) async {
    final result = await navigateToPayment(
      totalIncludingTax,
      PaymentSource.SpotBooking,
      reservationID,
    );

    if (result == null || result.status != PaymentStatus.SUCCESS) {
      throw CouldNotPay();
    }
  }

  Future<ReservationResult> _reserve(
    Cart cart,
    User user, {
    bool nextWorkingDay = false,
  }) async {
    _log.info('Sending post request to reserve');

    final url = nextWorkingDay ? '/reserve/next_working_day' : '/reserve';
    final response = await http.post(
      Uri.parse(base_url + url),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'packages_to_reserve': cart.toJson(),
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw CouldNotReserve();
    }

    final reserveJson = jsonDecode(response.body) as Map<String, dynamic>;

    if (reserveJson.containsKey('failed')) {
      return ReservationResult(false, null);
    }

    return ReservationResult(true, reserveJson['reservation_id']);
  }

  Future<void> _cancelReservation(User user, String reservationID) async {
    final response = await http.post(
      Uri.parse(base_url + '/cancel_reservation'),
      headers: {'content-type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'reservation_id': reservationID,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');
  }

  Future<void> _book(String reservationID) async {
    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/book/spot2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'reservation_id': reservationID,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw CouldNotBook();
    }
  }
}

class ReservationResult {
  final bool success;
  final String? reservationID;

  ReservationResult(this.success, this.reservationID);
}

class CouldNotReserve extends Error {}

class CouldNotBook extends Error {}

class CouldNotPay extends Error {}

class DeliveryLocationNotSet extends Error {
  final Meal meal;

  DeliveryLocationNotSet(this.meal);
}
