import 'package:flutter/material.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/util/ui/QuantityPicker.dart';
import 'package:collection/collection.dart' show IterableExtension;

class GroupPackages extends StatefulWidget {
  /// List of all packages
  final List<Package> packages;

  final List<Package> requiredPackages;

  final bool enabled;

  GroupPackages({
    required this.packages,
    required this.requiredPackages,
    required this.enabled,
  });

  @override
  _GroupPackagesState createState() => _GroupPackagesState();
}

class _GroupPackagesState extends State<GroupPackages> {
  late List<String> _groups;
  final _isExpanded = <bool>[];

  @override
  void initState() {
    super.initState();
    widget.packages.sort((a, b) => a.id.compareTo(b.id));

    _groups = widget.packages.map((package) => package.group).toSet().toList();
    _groups.sort();

    _groups.forEach((group) {
      _isExpanded.add(
        widget.requiredPackages.firstWhereOrNull((p) => p.group == group) !=
            null,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
      expansionCallback: (index, isPanelExpanded) {
        setState(() {
          _isExpanded[index] = !isPanelExpanded;
        });
      },
      expandedHeaderPadding: const EdgeInsets.all(8),
      children: !widget.enabled
          ? []
          : _groups
              .map(
                (group) => ExpansionPanel(
                  isExpanded: _isExpanded[_groups.indexOf(group)],
                  canTapOnHeader: true,
                  headerBuilder: (context, isExpanded) {
                    return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              group,
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                          ),
                        ]);
                  },
                  body: Column(
                    children: widget.packages
                        .where((package) => package.group == group)
                        .map((package) {
                      final requiredPackage = widget.requiredPackages
                          .firstWhereOrNull((p) => p.id == package.id);
                      final handleTap = () {
                        if (!widget.enabled) {
                          return;
                        }

                        setState(() {
                          if (requiredPackage != null) {
                            _remove(requiredPackage);
                          } else {
                            _add(package);
                          }
                        });
                      };

                      final price = requiredPackage != null
                          ? package.price *
                              widget.requiredPackages
                                  .firstWhere((p) => p.id == package.id)
                                  .quantityAvailable!
                          : package.price;

                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: handleTap,
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Checkbox(
                                    onChanged: (value) => handleTap(),
                                    value: requiredPackage != null,
                                  ),
                                  Text(
                                    package.name,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  Spacer(),
                                  if (requiredPackage != null)
                                    Container(
                                      margin: const EdgeInsets.fromLTRB(
                                          10, 0, 30, 0),
                                      padding: const EdgeInsets.all(8),
                                      child: QuantityPicker(
                                        onIncreaseQuantity: () {
                                          setState(() {
                                            _increment(requiredPackage);
                                          });
                                        },
                                        onDecreaseQuantity: () {
                                          setState(() {
                                            _decrement(requiredPackage);
                                          });
                                        },
                                        value:
                                            requiredPackage.quantityAvailable!,
                                        borderColor: Colors.white,
                                        textColor: Colors.white,
                                      ),
                                    ),
                                  Text(
                                    rupeeSymbol + price.toString(),
                                    style: TextStyle(fontSize: 16),
                                  ),
                                ],
                                mainAxisSize: MainAxisSize.max,
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                                child: Align(
                                  child: Text(package.description),
                                  alignment: Alignment.centerLeft,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ),
              )
              .toList(),
    );
  }

  void _add(Package package) {
    if (!package.addOn) {
      widget.requiredPackages.add(package.copyWith(quantity: 1));
      return;
    }

    if (_canAdd(package)) {
      widget.requiredPackages.add(package.copyWith(quantity: 1));
    }
  }

  bool _canAdd(Package addOn) {
    final normalPackageCount = _normalPackageCount();
    return addOn.quantityAvailable! < normalPackageCount;
  }

  void _remove(Package package) {
    widget.requiredPackages.remove(package);

    if (package.addOn) {
      return;
    }

    final normalPackageCount = _normalPackageCount();

    widget.requiredPackages
        .where((p) => p.addOn && p.quantityAvailable! > normalPackageCount)
        .forEach((package) {
      package.quantityAvailable = normalPackageCount;
    });

    widget.requiredPackages
        .removeWhere((p) => p.addOn && p.quantityAvailable == 0);
  }

  void _increment(Package package) {
    if (!package.addOn) {
      package.quantityAvailable = package.quantityAvailable! + 1;
      return;
    }

    if (_canAdd(package)) {
      package.quantityAvailable = package.quantityAvailable! + 1;
    }
  }

  void _decrement(Package package) {
    package.quantityAvailable = package.quantityAvailable! - 1;

    widget.requiredPackages.removeWhere((p) => p.quantityAvailable == 0);

    if (package.addOn) {
      return;
    }

    final normalPackageCount = _normalPackageCount();

    widget.requiredPackages
        .where((p) => p.addOn && p.quantityAvailable! > normalPackageCount)
        .forEach((package) {
      package.quantityAvailable = normalPackageCount;
    });

    widget.requiredPackages.removeWhere((p) => p.quantityAvailable == 0);
  }

  int _normalPackageCount() {
    final normalPackages = widget.requiredPackages
        .where((p) => !p.addOn)
        .map((p) => p.quantityAvailable!)
        .toList();

    final normalPackageCount =
        normalPackages.length == 0 ? 0 : normalPackages.reduce((a, b) => a + b);
    return normalPackageCount;
  }
}
