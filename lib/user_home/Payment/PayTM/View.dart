

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:paytm_allinonesdk/paytm_allinonesdk.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/user_home/Payment/Model.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';

class PayTM extends StatefulWidget {
  final double amount;
  final PaymentSource paymentSource;
  final String? reservationID;

  const PayTM(this.amount, this.paymentSource, this.reservationID);

  @override
  _PayTMState createState() => _PayTMState();
}

class CouldNotInitiateTransaction extends Error {}

class Transaction {
  final String id, merchantID, callbackUrl, token;
  final bool isStaging;

  Transaction(
      this.id, this.merchantID, this.callbackUrl, this.token, this.isStaging);

  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      json['transaction_id'],
      json['merchant_id'],
      json['callback_url'] ?? '',
      json['token'],
      json['is_staging'],
    );
  }
}

class _PayTMState extends State<PayTM> {
  @override
  void initState() {
    super.initState();
    pay();
  }

  static final _log = Logger('PayTM');

  bool showCircularProgressIndicator = true;

  Future pay() async {
    await callPayTMSdk(await (initiateTransaction()));
  }

  Future callPayTMSdk(Transaction transaction) async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    try {
      final response = await AllInOneSdk.startTransaction(
        transaction.merchantID,
        transaction.id,
        widget.amount.toStringAsFixed(2),
        transaction.token,
        'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${transaction.id}',
        transaction.isStaging,
        false,
      );

      print(response);
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);
      _log.severe('Something went wrong while payment', e);
      Navigator.of(context).pop(
        PaymentResult(PaymentStatus.FAILED),
      );

      return;
    }

    Navigator.of(context).pop(PaymentResult(PaymentStatus.SUCCESS));

    setState(() {
      showCircularProgressIndicator = false;
    });
  }

  Future initiateTransaction() async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    _log.info('Getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/payment_gateway/initiate_transaction_v2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'amount': widget.amount,
        'source': widget.paymentSource.value,
        'reservation_id': widget.reservationID,
      }),
    );

    setState(() {
      showCircularProgressIndicator = false;
    });

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('something went wrong while initiating transaction');
      throw CouldNotInitiateTransaction();
    }

    return Transaction.fromJson(jsonDecode(response.body));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: CustomScaffold(
        showCircularProgressIndicator: showCircularProgressIndicator,
        body: Container(),
      ),
    );
  }
}
