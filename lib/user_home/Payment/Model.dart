

enum PaymentStatus { FAILED, SUCCESS, PENDING }

class PaymentResult {
  final PaymentStatus status;
  final String? transactionID;

  PaymentResult(this.status, [this.transactionID]);
}

class PaymentSource {
  final int value;

  const PaymentSource._(this.value);

  static const Schedule = PaymentSource._(0);

  /// Spot booking was initially 1. But in an update, I am
  /// changing how spot booking works. So, its new id is 4
  static const SpotBooking = PaymentSource._(4);

  static const Recharge = PaymentSource._(2);
  // static const Resume = PaymentSource._(3);
}