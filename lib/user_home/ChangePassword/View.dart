

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController _controller = TextEditingController();

  static final _log = Logger('Change password');

  bool _loading = false;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: _loading,
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: _controller,
              validator: (str) => isPasswordValid(str!.trim()),
              decoration: InputDecoration(
                  labelText: 'New password', icon: Icon(Icons.security)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                child: Text('Change Password'),
                onPressed: _onChangePassword,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onChangePassword() async {
    final newPassword = _controller.text.trim();

    final errorMessage = isPasswordValid(newPassword);

    if (errorMessage != null) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text(errorMessage),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );

      return;
    }

    setState(() {
      _loading = true;
    });

    _log.info('Getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/change_password'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'new_password': newPassword,
      }),
    );

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    final storage = FlutterSecureStorage();
    await storage.write(key: 'password', value: newPassword);

    setState(() {
      _loading = false;
    });

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Password Changed Successfully'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        ],
      ),
    );
  }
}
