import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/deliverables/Model.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/QuantityPicker.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:preetibhojan/util/ui/BottomNavBar.dart';

class Orders extends StatefulWidget {
  final DeliverableRepository deliverableRepository;
  final PackageRepository packageRepository;
  final MealsRepository mealsRepository;

  const Orders(
    this.deliverableRepository,
    this.packageRepository,
    this.mealsRepository,
  );

  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  late Future<void> future;

  @override
  void initState() {
    super.initState();
    future = _init();
  }

  late List<Package> packages;
  late List<Meal> meals;
  late List<Deliverable> deliverables;

  Future<void> _init() async {
    deliverables = await widget.deliverableRepository.allDeliverables;
    packages = await widget.packageRepository.packages;
    meals = await widget.mealsRepository.meals;
  }

  bool _showProgressIndicator = false;

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: _showProgressIndicator,
      bottomNavBar: BottomNavBar(BottomNavBarItem.Order),
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });

            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            if (deliverables.length == 0) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Oops'),
                    content: Text('No Meals Booked Yet'),
                    actions: [
                      TextButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context).popUntil((route) => route.isFirst);
                        },
                      )
                    ],
                  ),
                );
              });
            }

            final dates = deliverables
                .map((deliverable) => deliverable.date)
                .toSet()
                .toList();

            dates.sort((a, b) => b.compareTo(a));

            return ListView.builder(
              itemCount: dates.length,
              itemBuilder: (context, index) {
                final date = dates[index];
                final deliverablesForDate =
                    deliverables.where((element) => element.date == date);

                if (deliverablesForDate.length == 0) {
                  return Container();
                }

                final packagesForDate = packages.where(
                  (package) =>
                      deliverablesForDate.firstWhereOrNull((deliverable) =>
                          deliverable.packageID == package.id) !=
                      null,
                );

                final mealsForDate = meals
                    .where(
                      (meal) =>
                          packagesForDate.firstWhereOrNull(
                              (package) => package.mealId == meal.id) !=
                          null,
                    )
                    .toList();

                mealsForDate.sort();

                final total = deliverablesForDate
                    .map((deliverable) => deliverable.price)
                    .reduce((value, element) => value + element);

                return CardTile(
                  title: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 8.0),
                    child: Row(
                      children: [
                        Flexible(
                          flex: 9,
                          fit: FlexFit.tight,
                          child: Text(
                            '${DateFormat.yMMMd().format(date)}',
                            style: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Flexible(
                          flex: 4,
                          fit: FlexFit.tight,
                          child: Text(
                            '$rupeeSymbol ${total.toStringAsFixed(2)}',
                            style: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                  ),
                  child: DefaultTextStyle(
                    style: TextStyle(fontSize: 16),
                    child: Column(
                      children: mealsForDate.map(
                        (meal) {
                          return Row(
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 14),
                                  child: Text(meal.name),
                                ),
                              ),
                              Flexible(
                                flex: 5,
                                fit: FlexFit.tight,
                                child: Column(
                                  children: [
                                    ...packagesForDate
                                        .where((package) =>
                                            package.mealId == meal.id)
                                        .map(
                                      (package) {
                                        final deliverable = deliverablesForDate
                                            .firstWhere((p) =>
                                                p.packageID == package.id);
                                        return PackageRow(
                                          package: package,
                                          deliverable: deliverable,
                                          repository:
                                              widget.deliverableRepository,
                                          setProgressIndicator: (value) {
                                            setState(() {
                                              _showProgressIndicator = value;
                                            });
                                          },
                                          onCancelled: (int quantity) {
                                            setState(() {
                                              deliverable.quantity -= quantity;
                                              if (deliverable.quantity < 1) {
                                                deliverables.remove(deliverable);
                                              }
                                            });
                                          },
                                        );
                                      },
                                    ).toList(),
                                    Divider(
                                      indent: 0,
                                      endIndent: 0,
                                      thickness: 1,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        },
                      ).toList(),
                    ),
                  ),
                );
              },
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class PackageRow extends StatefulWidget {
  final Package package;
  final Deliverable deliverable;
  final DeliverableRepository repository;
  final ValueChanged<bool> setProgressIndicator;
  final ValueChanged<int> onCancelled;

  const PackageRow({
    required this.package,
    required this.deliverable,
    required this.repository,
    required this.setProgressIndicator,
    required this.onCancelled,
  });

  @override
  _PackageRowState createState() => _PackageRowState();
}

class _PackageRowState extends State<PackageRow> {
  int _quantityToCancel = 1;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4),
      child: Row(
        children: [
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(widget.package.name),
            ),
          ),
          Flexible(
            fit: FlexFit.tight,
            child: Center(
              child: Text('${widget.deliverable.quantity}'),
            ),
          ),
          if (widget.deliverable.status == DeliverableStatus.Ordered)
            Flexible(
              flex: 5,
              fit: FlexFit.tight,
              child: Center(
                child: QuantityPicker(
                  onDecreaseQuantity: () {
                    if (_quantityToCancel > 1) {
                      setState(() {
                        _quantityToCancel--;
                      });
                    }
                  },
                  onIncreaseQuantity: () {
                    if (_quantityToCancel < widget.deliverable.quantity) {
                      setState(() {
                        _quantityToCancel++;
                      });
                    }
                  },
                  borderColor: Colors.white,
                  textColor: Colors.white,
                  value: _quantityToCancel,
                ),
              ),
            ),
          if (widget.deliverable.status != DeliverableStatus.Ordered)
            Flexible(
              flex: 5,
              fit: FlexFit.tight,
              child: Center(
                child: QuantityPicker(
                  onDecreaseQuantity: () {},
                  onIncreaseQuantity: () {},
                  borderColor: Colors.black,
                  textColor: Colors.black,
                  value: _quantityToCancel,
                ),
              ),
            ),
          Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: Align(
              alignment: Alignment.centerRight,
              child: ElevatedButton(
                onPressed:
                    widget.deliverable.status != DeliverableStatus.Ordered
                        ? null
                        : _cancel,
                child: Text('Cancel'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _cancel() async {
    widget.setProgressIndicator(true);

    try {
      await widget.repository.cancel(
        widget.package,
        _quantityToCancel,
        widget.deliverable.date,
      );

      widget.onCancelled(_quantityToCancel);

      setState(() {
        _quantityToCancel = 1;
      });

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Meal cancelled successfully'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
    } finally {
      widget.setProgressIndicator(false);
    }
  }
}
