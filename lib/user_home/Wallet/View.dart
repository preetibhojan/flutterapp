import 'dart:math';

import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart' hide Title;
import 'package:intl/intl.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/user_home/Payment/Model.dart';
import 'package:preetibhojan/user_home/PaymentHistory/Model.dart';
import 'package:preetibhojan/user_home/PaymentHistory/Repository.dart';
import 'package:preetibhojan/user_home/Refund/Repository.dart';
import 'package:preetibhojan/user_home/Refund/View.dart';
import 'package:preetibhojan/util/ui/BottomNavBar.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/FlexTable.dart';
import 'package:preetibhojan/util/ui/Table.dart';

class Wallet extends StatefulWidget {
  final GetCredit getCredit;
  final PaymentHistoryRepository paymentHistoryRepository;

  const Wallet(
    this.getCredit,
    this.paymentHistoryRepository,
  );

  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  final _controller = TextEditingController()..text = '100';

  late final MoneyAvailable _moneyAvailable;

  late final Future<void> _future = _init();

  late final List<Transaction> _transactions;

  Future<void> _init() async {
    _moneyAvailable = await widget.getCredit();

    if (_moneyAvailable.credit < 0) {
      _controller.text = '${-_moneyAvailable.credit.round()}';
    }

    final transactions = await widget.paymentHistoryRepository.transactions;
    transactions.sort((a, b) => b.created.compareTo(a.created));
    _transactions = transactions.sublist(0, min(3, transactions.length));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      bottomNavBar: BottomNavBar(BottomNavBarItem.Wallet),
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(
          child: CircularProgressIndicator(),
        ),
        builder: (context, _) {
          return ListView(
            padding: const EdgeInsets.all(20),
            children: [
              Row(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 8, bottom: 8, right: 4),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          border: Border.all(color: Colors.white),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text('Balance',
                                  style: Theme.of(context).textTheme.headline5),
                              SizedBox(height: 4),
                              Text(
                                '$rupeeSymbol ${_moneyAvailable.credit.toStringAsFixed(0)}',
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 8, bottom: 8, left: 4),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          border: Border.all(color: Colors.white),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text('Deposit',
                                  style: Theme.of(context).textTheme.headline5),
                              SizedBox(height: 4),
                              Text(
                                '$rupeeSymbol ${_moneyAvailable.deposit.toStringAsFixed(0)}',
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  border: Border.all(color: Colors.white),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Text(
                        'Recharge',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      TextFormField(
                        controller: _controller,
                        decoration: InputDecoration(
                          prefix: Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: Text(
                              rupeeSymbol,
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                        ),
                        validator: (str) => int.tryParse(str!) == null
                            ? 'Please enter a valid number'
                            : null,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      SizedBox(height: 4),
                      ElevatedButton(
                        child: Text('Recharge'),
                        onPressed: _recharge,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    border: Border.all(color: Colors.white),
                  ),
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Column(
                      children: [
                        Text(
                          'Payment History',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: FlexTable(
                            cellAlignments: [
                              Alignment.centerLeft,
                              Alignment.centerRight,
                              Alignment.centerRight,
                            ],
                            headerAlignments: [
                              Alignment.centerLeft,
                              Alignment.center,
                              Alignment.centerRight,
                            ],
                            headings: [
                              Title('Date'),
                              Center(child: Title('Time')),
                              Title('Amount'),
                            ],
                            rows: _transactions
                                .map(
                                  (transaction) => [
                                    Cell(DateFormat.yMMMd()
                                        .format(transaction.created)),
                                    Cell(DateFormat.jm()
                                        .format(transaction.created)),
                                    Cell(
                                      '$rupeeSymbol ${transaction.amount.toStringAsFixed(2)}',
                                    ),
                                  ],
                                )
                                .toList(),
                            flexFactors: [4, 3, 4],
                          ),
                        ),
                        ElevatedButton(
                          child: Text('Show All Transactions'),
                          onPressed: _transactions.isEmpty
                              ? null
                              : navigateToPaymentHistory,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              if (_moneyAvailable.credit > 0)
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      border: Border.all(color: Colors.white),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Column(
                        children: [
                          Text(
                            'Refund Request',
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          Refund(widget.getCredit),
                        ],
                      ),
                    ),
                  ),
                ),
            ],
          );
        },
      ),
    );
  }

  void _recharge() async {
    final amountStr = _controller.text.trim();

    final amount = int.tryParse(amountStr);
    if (amount == null) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please enter a valid number'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    final result = await navigateToPayment(
      amount.toDouble(),
      PaymentSource.Recharge,
      null,
    );

    if (result == null || result.status == PaymentStatus.FAILED) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Recharge failed'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    if (result.status == PaymentStatus.SUCCESS) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Recharge Successful'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
            ),
          ],
        ),
      );

      return;
    }

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Recharge pending'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
