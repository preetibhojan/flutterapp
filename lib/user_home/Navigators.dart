import 'package:flutter/material.dart';
import 'package:preetibhojan/admin_home/AddOrder/OrderRepository.dart';
import 'package:preetibhojan/admin_home/AddOrder/View.dart';
import 'package:preetibhojan/admin_home/AddQuantity/View.dart';
import 'package:preetibhojan/admin_home/CancelOrder/View.dart';
import 'package:preetibhojan/admin_home/NotificationNotSent.dart';
import 'package:preetibhojan/admin_home/Notifications/Repository.dart';
import 'package:preetibhojan/admin_home/Notifications/View.dart';
import 'package:preetibhojan/admin_home/Report/Repository.dart';
import 'package:preetibhojan/admin_home/Report/View.dart';
import 'package:preetibhojan/admin_home/SetMenu/View.dart';
import 'package:preetibhojan/admin_home/UpdatePaymentDetail/View.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/DoorStepDelivery.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/menu/Repositories.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/common/schedule/Repositories.dart';
import 'package:preetibhojan/login_or_register/ForgotPassword.dart';
import 'package:preetibhojan/login_or_register/LogIn.dart';
import 'package:preetibhojan/register/Register.dart';
import 'package:preetibhojan/user_home/Menu/DeliveryLocation.dart';
import 'package:preetibhojan/user_home/Profile/Repository.dart';
import 'package:preetibhojan/util/ui/WebViewWrapper.dart';

import 'Booking/View.dart';
import 'Booking/ViewModel.dart';
import 'Cart/Models.dart';
import 'Cart/View.dart';
import 'ChangePassword/View.dart';
import 'Help.dart';
import 'Menu/PackageGroup.dart';
import 'Menu/PackageGroupRepository.dart';
import 'Menu/Repository.dart';
import 'Menu/View.dart';
import 'Menu/ViewModel.dart';
import 'Orders/View.dart';
import 'Payment/Model.dart';
import 'Payment/PayTM/View.dart';
import 'PaymentHistory/Repository.dart';
import 'PaymentHistory/View.dart';
import 'Profile/View.dart';
import 'Refund/Repository.dart';
import 'Schedule.dart';
import 'Schedule/ViewModel.dart';
import 'Wallet/View.dart';

final navigatorKey = GlobalKey<NavigatorState>();

final _homeAddressRepository = NetworkHomeAddressRepository();
final _officeAddressRepository = NetworkOfficeAddressRepository();
final _packageGroupRepository = PackageGroupRepository();
final _allPackagesRepository = NetworkAllPackagesRepository();
final _allMealsRepository = NetworkAllMealsRepository();
final _regularPackagesRepository = NetworkRegularPackagesRepository();
final _mealsRepository = NetworkMealsRepository();
final _deliveryLocationsRepository = NetworkDeliveryLocationsRepository();
final _scheduleRepository = NetworkScheduleRepository();
final _deliverableRepository = NetworkDeliverableRepository();
final _paymentHistory = NetworkPaymentHistory();
final _configRepository = NetworkConfigRepository();
final _adminPackageForMenu = AdminPackageForMenu();
final _packageForMenuRepository = NetworkPackagesForMenuRepository();
final _cuisineRepository = NetworkCuisineRepository();
final _adminMenuRepository = AdminMenuRepository();
final _menuRepository = NetworkMenuRepository();
final _notificationRepository = NotificationRepository();
final _reportRepository = NetworkReportRepository();
final _reportTimeRepository = NetworkReportTimeRepository();
final _orderRepository = NetworkOrderRepository();
final _allMealsWithImage = NetworkAllMealsWithImageURL();
var _bookedScheduleRepository =
    NetworkScheduleRepository('/booked_payment_info');

Future<void> navigateToChooseDeliveryLocation(
    Meal meal, List<Package> packages, Cart cart) {
  return navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (context) {
        return ChooseDeliveryLocation(
          _homeAddressRepository,
          _officeAddressRepository,
          meal,
          packages,
          cart,
        );
      },
    ),
  );
}

Future<void> navigateToPackageGroup(
    Meal meal, List<Package> packages, Cart cart) {
  return navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (context) => PackageGroup(
        _packageGroupRepository,
        meal,
        packages,
        cart,
      ),
    ),
  );
}

void navigateToSchedule([Meal? mealToStart]) {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (context) => ScheduleScreen(
        ScheduleViewModel(
          packageRepository: _regularPackagesRepository,
          mealRepository: _mealsRepository,
          deliveryLocationsRepository: _deliveryLocationsRepository,
          scheduleRepository: _scheduleRepository,
          date: DateTime.now(),
        ),
        _deliverableRepository,
        mealToStart: mealToStart,
      ),
    ),
  );
}

void navigateToLogin() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(builder: (context) => LoginOrRegister()),
  );
}

void navigateToRegister() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(builder: (_) => Register(doorStepDelivery)),
  );
}

void navigateToBooking() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => Booking(
        BookingViewModel(
          _scheduleRepository,
          _deliverableRepository,
          DateTime.now(),
        ),
      ),
    ),
  );
}

void navigateToHelp() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(builder: (context) => Help()),
  );
}

void navigateToWallet() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => Wallet(getCredit, _paymentHistory),
    ),
  );
}

void navigateToProfile() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => Profile(
        _configRepository,
        doorStepDelivery,
      ),
    ),
  );
}

void navigateToOrders() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => Orders(
        _deliverableRepository,
        _allPackagesRepository,
        _allMealsRepository,
      ),
    ),
  );
}

void navigateToPaymentHistory() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => PaymentHistory(_paymentHistory),
    ),
  );
}

void navigateToChangePassword() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(builder: (_) => ChangePassword()),
  );
}

void navigateToForgotPassword() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => ForgotPassword(),
    ),
  );
}

void navigateToAddQuantity() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => AddQuantity(
        _allMealsRepository,
        _allPackagesRepository,
        _adminPackageForMenu,
        _cuisineRepository,
        DateTime.now(),
      ),
    ),
  );
}

void navigateToSetMenu() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => SetMenu(
        _adminMenuRepository,
        _allMealsRepository,
        _cuisineRepository,
        DateTime.now(),
      ),
    ),
  );
}

void navigateToSetNotifications() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => Notifications(
        _reportRepository,
        _reportTimeRepository,
        _allMealsRepository,
        _cuisineRepository,
        _notificationRepository,
        DateTime.now(),
      ),
    ),
  );
}

void navigateToNotificationsNotSent() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => NotificationsNotSent(
        _notificationRepository,
        _allMealsRepository,
        _reportTimeRepository,
        _reportRepository,
        DateTime.now(),
      ),
    ),
  );
}

void navigateToAddOrder() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => AddOrder(
        _orderRepository,
        _allPackagesRepository,
        _allMealsRepository,
        _cuisineRepository,
        _reportRepository,
        DateTime.now(),
      ),
    ),
  );
}

void navigateToCancelOrder() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => CancelOrder(
        _orderRepository,
        _allMealsRepository,
        _cuisineRepository,
        _reportRepository,
        DateTime.now(),
      ),
    ),
  );
}

void navigateToUpdatePaymentDetail() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => UpdatePaymentDetail(_orderRepository),
    ),
  );
}

Future<PaymentResult?> navigateToPayment(
    double amount, PaymentSource source, String? reservationID) {
  return navigatorKey.currentState!.push(
    MaterialPageRoute<PaymentResult>(
      builder: (_) => PayTM(amount, source, reservationID),
    ),
  );
}

void navigateToWebView(String url) {
  navigatorKey.currentState!.push(
    MaterialPageRoute(builder: (_) => WebViewWrapper(url)),
  );
}

void navigateToAdminHome() {
  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => ReportScreen(
        _allMealsRepository,
        _cuisineRepository,
        _reportRepository,
        _reportTimeRepository,
        DateTime.now(),
      ),
    ),
  );
}

final _viewModel = MenuViewModel(
  DateTime.now(),
  _menuRepository,

  /// Menu should share the package repository with cart so when
  /// cart invalidates cache of package repository, menu gets
  /// updated as well.
  _packageForMenuRepository,
  _allMealsWithImage,
);

late final _cart = Cart(
  _packageForMenuRepository,
  _mealsRepository,
  NetworkDeliveryCharges(),
  DateTime.now(),
);

void navigateToCustomerHome() {
  _packageForMenuRepository.invalidateCache();
  _menuRepository.invalidateCache();

  navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) => MenuScreen(
        _viewModel,
        () => DateTime.now(),
        notServingRepository,
        _cart,
      ),
    ),
  );
}

Future<void> navigateToCart() {
  return navigatorKey.currentState!.push(
    MaterialPageRoute(
      builder: (_) {
        return CartScreen(
          _deliveryLocationsRepository,
          _deliverableRepository,
          _bookedScheduleRepository,
          _mealsRepository,
          _cart,
          DateTime.now(),
        );
      },
    ),
  );
}
