import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/user_home/GroupPackages.dart';
import 'package:provider/provider.dart';

import 'ViewModel.dart';

class MealTypeCard extends StatefulWidget {
  final Meal meal;
  final bool enabled;

  MealTypeCard({required this.meal, required this.enabled});

  @override
  _MealTypeCardState createState() => _MealTypeCardState();
}

class _MealTypeCardState extends State<MealTypeCard> {
  late ScheduleViewModel viewModel;
  late List<Package> packages, addOns;
  final isExpanded = <String, bool>{};

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of<ScheduleViewModel>(context, listen: false);
    packages = viewModel.packagesFor(widget.meal);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: [
            ...viewModel.mealInfoManager[widget.meal]!.deliveryInfo.keys
                .map((day) {
              return Row(
                children: <Widget>[
                  Text(
                    day.name,
                    style: TextStyle(fontSize: 16),
                  ),
                  Spacer(),
                  DropdownButton<DeliveryLocation>(
                    items: viewModel
                        .mealInfoManager[widget.meal]!.deliveryLocations
                        .map(
                          (deliveryLocation) =>
                              DropdownMenuItem<DeliveryLocation>(
                            child: Text(deliveryLocation.name),
                            value: deliveryLocation,
                          ),
                        )
                        .toList(),
                    onChanged: !widget.enabled || viewModel.mealInfoManager[widget.meal]!.disable(day)
                        ? null
                        : (DeliveryLocation? value) {
                            viewModel.mealInfoManager[widget.meal]!
                                .deliveryInfo[day] = value;
                            setState(() {});
                          },
                    value: viewModel
                        .mealInfoManager[widget.meal]!.deliveryInfo[day],
                  ),
                ],
              );
            }).toList(),
          ],
        ),
        Visibility(
          visible: widget.enabled,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: Text(
              'Package',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
        ),
        GroupPackages(
          packages: packages,
          requiredPackages:
              viewModel.mealInfoManager[widget.meal]!.requiredPackages,
          enabled: widget.enabled,
        ),
      ],
    );
  }
}
