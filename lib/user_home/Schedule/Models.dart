

import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/schedule/Models.dart';

class MealInfo with ChangeNotifier {
  Map<Day, DeliveryLocation?> deliveryInfo = {};

  bool _required = false;

  bool get required => _required;

  set required(value) {
    _required = value;
    notifyListeners();
  }

  Package? _requiredPackage;

  final requiredPackages = <Package>[];

  final List<Day> cannotOrderOn;

  List<DeliveryLocation> deliveryLocations;

  final DateTime date;

  MealInfo(this.deliveryLocations, this.date, {this.cannotOrderOn = const []}) {
    assert(
      deliveryLocations.length > 0,
      'Delivery locations should have at least 1 element',
    );

    /// Setting deliveryInfo[i] to null because the list of days is fetched
    /// from deliveryInfo. Without this, days wont be displayed.
    for (final day in validDays()) {
      deliveryInfo[day] = null;
    }
  }

  List<Day> validDays() {
    final days = List.of(Day.values);

    days.removeWhere((day) => cannotOrderOn.contains(day));

    if (date.weekday == DateTime.sunday) {
      return days;
    }

    days.removeWhere((day) => day.weekday <= date.weekday);
    return days;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MealInfo &&
          runtimeType == other.runtimeType &&
          _requiredPackage == other._requiredPackage;

  @override
  int get hashCode => _requiredPackage.hashCode;

  bool disable(Day day) {
    /// If today is sunday, no days are disabled
    if (date.weekday == DateTime.sunday) {
      return false;
    }

    /// Otherwise, disable the day if it has already been passed
    /// Or if it is today.
    ///
    /// This is because schedule is only effective from tomorrow
    return day.weekday <= date.weekday;
  }
}

class MealInfoManager with ChangeNotifier {
  /// Splay Tree Map used to keep _map sorted according to Meal
  /// which by default compares according to priority
  var _map = SplayTreeMap<Meal, MealInfo>();

  get length => _map.length;

  void add(Meal requiredMeal, MealInfo mealInfo) {
    if (!_map.containsKey(requiredMeal)) {
      _map[requiredMeal] = mealInfo;

      // T changes notify listeners of ChangeNotifierMap that it has changed
      _map[requiredMeal]!.addListener(() {
        notifyListeners();
      });

      notifyListeners();
    }
  }

  MealInfo? operator [](Meal key) => _map[key];

  void remove(Meal key) {
    _map.remove(key);
    notifyListeners();
  }

  void clear() {
    _map.clear();
    notifyListeners();
  }

  List<Meal> get keys => _map.keys.toList();

  @override
  String toString() => _map.toString();

  void forEach(void f(Meal key, MealInfo value)) =>
      _map.forEach((k, v) => f(k, v));

  bool contains(Meal key) => _map.containsKey(key);

  Map<Meal, MealInfo> get internal => _map;

  MealInfoManager clone() {
    final copy = MealInfoManager();
    copy._map = SplayTreeMap.from(
      this._map.map(
            (key, value) => MapEntry(key, value),
          ),
    );
    return copy;
  }
}
