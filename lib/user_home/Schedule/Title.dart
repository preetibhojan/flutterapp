

import 'package:flutter/material.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/user_home/Schedule/ViewModel.dart';
import 'package:provider/provider.dart';

class TitleWithSwitch extends StatefulWidget {
  final Meal meal;

  const TitleWithSwitch(this.meal);

  @override
  _TitleWithSwitchState createState() => _TitleWithSwitchState();
}

class _TitleWithSwitchState extends State<TitleWithSwitch> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ScheduleViewModel>(
      builder: (context, scheduleViewModel, _) {
        return SwitchListTile(
          title: Text(
            widget.meal.name,
            style: Theme.of(context).textTheme.headline6,
          ),
          value: scheduleViewModel.mealInfoManager[widget.meal]!.required,
          onChanged: (bool value) {
            scheduleViewModel.mealInfoManager[widget.meal]!.required = value;
          },
        );
      },
    );
  }
}
