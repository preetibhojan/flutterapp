import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/foundation.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/common/schedule/Models.dart';
import 'package:preetibhojan/common/schedule/Repositories.dart';

import 'Models.dart';

class ScheduleViewModel with ChangeNotifier {
  final PackageRepository packageRepository;
  final MealsRepository mealRepository;
  final DeliveryLocationsRepository deliveryLocationsRepository;
  final ScheduleRepository scheduleRepository;
  final DateTime date;

  ScheduleViewModel({
    required this.packageRepository,
    required this.mealRepository,
    required this.deliveryLocationsRepository,
    required this.scheduleRepository,
    required this.date,
  });

  late MealInfoManager mealInfoManager;

  void addMeal(Meal meal) {
    final mealInfo = MealInfo(
      _deliveryLocations,
      date,
      cannotOrderOn: meal.cannotOrderOn!,
    );

    final schedule = _schedule.packages[meal];

    if (schedule != null) {
      mealInfo.required = true;

      for (final packageInfo in schedule) {
        final package =
            _packages.firstWhereOrNull((p) => p.id == packageInfo.packageID);
        if (package == null) {
          continue;
        }

        mealInfo.requiredPackages
            .add(package.copyWith(quantity: packageInfo.quantity));
      }

      final addOns = _schedule.addOns[meal];
      if (addOns != null) {
        addOns.forEach((addOn) {
          mealInfo.requiredPackages.add(
            _packages
                .firstWhere((p) => p.id == addOn.packageID)
                .copyWith(quantity: addOn.quantity),
          );
        });
      }

      final validDays = mealInfo.validDays();
      schedule[0].deliveryInfo.forEach((day, deliveryLocation) {
        if (validDays.contains(day)) {
          mealInfo.deliveryInfo[day] = deliveryLocation.deliveryLocation;
        }
      });
    }

    mealInfoManager.add(meal, mealInfo);
  }

  List<Package> packagesFor(Meal meal) {
    return _packages.where((p) => p.mealId == meal.id).toList();
  }

  late List<Package> _packages;
  late List<Meal> _meals;
  late Schedule _schedule;

  late List<DeliveryLocation> _deliveryLocations;

  late bool isRegularCustomer;

  Future<void> fetchData() async {
    _packages = await packageRepository.packages;
    _meals = await mealRepository.meals;
    _deliveryLocations = await deliveryLocationsRepository.deliveryLocations;
    _schedule = await scheduleRepository.schedule;

    _packages.sort((a, b) => a.name.compareTo(b.name));

    final mealsToRemove = <Meal>[];
    for (final meal in _meals) {
      if (_packages.firstWhereOrNull((package) => package.mealId == meal.id) ==
          null) {
        mealsToRemove.add(meal);
      }
    }

    _meals.removeWhere((meal) => mealsToRemove.contains(meal));

    mealInfoManager = MealInfoManager();
    mealInfoManager.addListener(() => notifyListeners());
    for (var meal in _meals) {
      addMeal(meal);
    }

    final user = await getUser();
    isRegularCustomer = user.isRegularCustomer;
  }

  bool isValid() {
    bool isValid = true;

    if (mealInfoManager.length < 1) {
      return false;
    }

    return isValid;
  }

  void requiredMealsUpdated() {
    notifyListeners();
  }

  @override
  String toString() {
    return 'Meals: $mealInfoManager';
  }
}
