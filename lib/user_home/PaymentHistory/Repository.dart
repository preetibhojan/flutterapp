

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Model.dart';

abstract class PaymentHistoryRepository {
  Future<List<Transaction>> get transactions;
}

class StubPaymentHistory implements PaymentHistoryRepository {
  final List<Transaction> input;

  StubPaymentHistory(this.input);

  @override
  Future<List<Transaction>> get transactions => Future.value(input);
}

class NetworkPaymentHistory implements PaymentHistoryRepository {
  static final _log = Logger('Network Payment History');

  List<Transaction>? _cache;

  @override
  Future<List<Transaction>> get transactions async {
    _log.info('getting transactions');
    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    _log.info('Getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/transaction_history'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');
    _cache = ((jsonDecode(response.body)) as List)
        .map((json) => Transaction.fromJson(json))
        .toList();

    return _cache!;
  }
}
