

class TransactionStatus {
  final int value;
  final String name;

  const TransactionStatus._(this.value, this.name);

  static const Successful = TransactionStatus._(0, 'Successful');
  static const NotUsed = TransactionStatus._(1, 'Not Used');

  factory TransactionStatus.fromInt(int value) {
    if (value == 0) {
      return Successful;
    } else if (value == 1) {
      return NotUsed;
    }

    throw StateError('Value of transaction status is not valid');
  }
}

class Transaction {
  final String id;
  final double amount;
  final TransactionStatus status;
  final DateTime created;

  Transaction(this.id, this.amount, this.status, this.created);

  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      json['id'],
      double.parse(json['amount']),
      TransactionStatus.fromInt(json['status']),
      DateTime.parse(json['created']),
    );
  }
}
