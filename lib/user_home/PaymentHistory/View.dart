import 'package:flutter/material.dart' hide Title;
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/user_home/PaymentHistory/Model.dart';
import 'package:preetibhojan/user_home/PaymentHistory/Repository.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/FlexTable.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:preetibhojan/util/ui/Table.dart';

class PaymentHistory extends StatefulWidget {
  final PaymentHistoryRepository paymentHistoryRepository;

  const PaymentHistory(this.paymentHistoryRepository);

  @override
  _PaymentHistoryState createState() => _PaymentHistoryState();
}

class _PaymentHistoryState extends State<PaymentHistory> {
  late Future<List<Transaction>> future;

  @override
  void initState() {
    super.initState();
    future = widget.paymentHistoryRepository.transactions;
  }

  static final _log = Logger('Payment History');

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          _log.info(snapshot);

          if (snapshot.hasError) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });

            return Container();
          }

          if (snapshot.hasData) {
            final transactions = snapshot.data as List<Transaction>;
            transactions.sort((a, b) => b.created.compareTo(a.created));

            return SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                child: FlexTable(
                  cellAlignments: [
                    Alignment.centerLeft,
                    Alignment.centerRight,
                    Alignment.centerRight,
                  ],
                  headerAlignments: [
                    Alignment.centerLeft,
                    Alignment.centerRight,
                    Alignment.centerRight,
                  ],
                  headings: [
                    Title('Date'),
                    Center(child: Title('Time')),
                    Title('Amount'),
                  ],
                  rows: transactions
                      .map(
                        (transaction) => [
                          Cell(DateFormat.yMMMd().format(transaction.created)),
                          Cell(DateFormat.jm().format(transaction.created)),
                          Cell(
                            '$rupeeSymbol ${transaction.amount.toStringAsFixed(2)}',
                          ),
                        ],
                      )
                      .toList(),
                  flexFactors: [4, 3, 4],
                ),
              ),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
