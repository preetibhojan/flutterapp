import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/DoorStepDelivery.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/cuisines/Model.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:preetibhojan/util/ui/BottomNavBar.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:validators/validators.dart';

import 'Repository.dart';
import 'ViewModel.dart';

class Profile extends StatefulWidget {
  final ConfigRepository configRepository;
  final DoorStepDelivery doorStepDelivery;

  Profile(this.configRepository, this.doorStepDelivery);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  static final _log = Logger('Profile');
  bool showCircularProgressIndicator = false;

  late ProfileViewModel profileViewModel;

  late Future<void> _future;

  @override
  void initState() {
    super.initState();
    profileViewModel = ProfileViewModel(
      NetworkCustomerRepository(),
      NetworkOfficeAddressRepository(),
      NetworkHomeAddressRepository(),
      NetworkCuisineRepository(),
      NetworkAreaRepository(),
    );

    _future = _init();
  }

  late int _doorStepDelivery;

  Future<void> _init() async {
    await profileViewModel.fetchData();
    _doorStepDelivery = await widget.doorStepDelivery();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: showCircularProgressIndicator,
      bottomNavBar: BottomNavBar(BottomNavBarItem.Profile),
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          _log.info(snapshot);
          if (snapshot.hasError) {
            _log.severe('Something went wrong', snapshot.error);

            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });

            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            _log.info(profileViewModel);

            return ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      '${profileViewModel.newCustomer.firstName.capitalize} '
                      '${profileViewModel.newCustomer.lastName.capitalize}',
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ),
                ),
                Divider(),
                DefaultTextStyle(
                  style: TextStyle(
                    fontSize: 16,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                    child: Column(
                      children: <Widget>[
                        ContactDetails(profileViewModel),
                        CustomDropDownRow<Cuisine>(
                          items: profileViewModel.cuisines
                              .map(
                                (cuisine) => DropdownMenuItem(
                                  child: Text(cuisine.name),
                                  value: cuisine,
                                ),
                              )
                              .toList(),
                          onChanged: (Cuisine? value) {
                            setState(() {
                              profileViewModel.newCustomer.cuisineID =
                                  value!.id;
                            });
                          },
                          value: profileViewModel.cuisines.firstWhere(
                              (element) =>
                                  element.id ==
                                  profileViewModel.newCustomer.cuisineID),
                          name: 'Cuisine',
                        ),
                        Padding(padding: const EdgeInsets.all(4)),
                        if (profileViewModel.cuisines
                            .firstWhere((c) =>
                                c.id == profileViewModel.newCustomer.cuisineID)
                            .showHomeAddress)
                          Column(
                            children: [
                              Divider(),
                              HomeAddressDetails(
                                  profileViewModel, _doorStepDelivery),
                            ],
                          ),
                        if (profileViewModel.cuisines
                            .firstWhere((c) =>
                                c.id == profileViewModel.newCustomer.cuisineID)
                            .showOfficeAddress)
                          Column(
                            children: [
                              Divider(),
                              OfficeAddressDetails(profileViewModel),
                            ],
                          ),
                        Padding(
                          padding: const EdgeInsets.all(8),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              flex: 2,
                              child: ElevatedButton(
                                onPressed: updateDetail,
                                child: Text('Save Edits'),
                              ),
                            ),
                            Spacer(),
                            Flexible(
                              flex: 3,
                              child: ElevatedButton(
                                child: Text('Restore Defaults'),
                                onPressed: () {
                                  setState(() {
                                    profileViewModel.reset();
                                  });
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  void updateDetail() async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    final user = await getUser();

    var showSuccess = true;

    final updated = <String>[];

    _log.info('change email');
    var response = await http.post(
      Uri.parse(base_url + '/change_email'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'email': profileViewModel.newCustomer.email,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      showFailure('email');
      showSuccess = false;
    }

    updated.add('Email');

    _log.info('Change cuisine');

    response = await http.post(
      Uri.parse(base_url + '/change_cuisine'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'cuisine_id': profileViewModel.newCustomer.cuisineID,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    /// cuisine changes need based delta. So update the config
    config = await widget.configRepository.config;

    if (response.statusCode != 200) {
      showFailure('cuisine');
      showSuccess = false;
    }

    updated.add('Cuisine');

    if (profileViewModel.newHomeAddress.enabled) {
      if (!profileViewModel.newHomeAddress.isValid()) {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text('Oops'),
            content: Text('Some fields are missing in Home Address'),
          ),
        );

        setState(() {
          showCircularProgressIndicator = false;
        });

        return;
      }

      _log.info('Change home address');
      response = await http.post(
        Uri.parse(base_url + '/change_home_address'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'phone_number': user.phoneNumber,
          'password': user.password,
          'home_address': profileViewModel.newHomeAddress,
        }),
      );

      _log.info('status code: ${response.statusCode}');
      _log.info('response: ${response.body}');

      if (response.statusCode != 200) {
        showFailure('Home Address');
        showSuccess = false;
      }

      updated.add('Home Address');
    }

    if (profileViewModel.newOfficeAddress.enabled) {
      if (!profileViewModel.newOfficeAddress.isValid()) {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text('Oops'),
            content: Text('Some fields are missing in Office Address'),
          ),
        );

        setState(() {
          showCircularProgressIndicator = false;
        });

        return;
      }

      _log.info('Change office address');

      response = await http.post(
        Uri.parse(base_url + '/change_office_address'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'phone_number': user.phoneNumber,
          'password': user.password,
          'office_address': profileViewModel.newOfficeAddress,
        }),
      );

      _log.info('status code: ${response.statusCode}');
      _log.info('response: ${response.body}');

      if (response.statusCode != 200) {
        showFailure('Office Address');
        showSuccess = false;
      }

      updated.add('Office Address');
    }

    String temp = '';

    if (updated.length == 1) {
      temp = updated[0];
    } else {
      for (int i = 0; i < updated.length - 1; i++) {
        temp += '${updated[i]},   ';
      }

      temp += 'and ${updated[updated.length - 1]}';
    }

    if (showSuccess) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => AlertDialog(
          title: Text('Success'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Successfully Changed -- $temp',
                textAlign: TextAlign.justify,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                child: Visibility(
                  visible: profileViewModel.newCustomer.cuisineID !=
                      profileViewModel.customer.cuisineID,
                  child: Text(
                    'Note: As you changed your Cuisine, Your Schedule and '
                    'Cancelled Meals has been deleted. Please Schedule Again',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.lime,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                /**
                     * Cannot use Navigator.of(context).popUntil((route) => route.isFirst);
                     * because the cuisine might have been changed. Which means menu might
                     * need to be reloaded.
                     */
                Navigator.of(context).popUntil((route) => false);
                navigateToCustomerHome();
              },
            ),
          ],
        ),
      );
    }

    setState(() {
      showCircularProgressIndicator = false;
    });
  }

  void showFailure(String name) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text('Oops'),
        content: Text('Could not change $name'),
        actions: <Widget>[
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}

class Header extends StatelessWidget {
  final String title;

  const Header({required this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: Theme.of(context).textTheme.headline6,
    );
  }
}

class ContactDetails extends StatefulWidget {
  final ProfileViewModel profileViewModel;

  const ContactDetails(this.profileViewModel);

  @override
  _ContactDetailsState createState() => _ContactDetailsState();
}

class _ContactDetailsState extends State<ContactDetails> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CustomRow(
          leading: 'Phone No.',
          trailing: widget.profileViewModel.newCustomer.phoneNumber,
          onSave: null,
          validator: null,
        ),
        Padding(
          padding: const EdgeInsets.all(4),
        ),
        CustomRow(
          leading: 'Email',
          trailing: widget.profileViewModel.newCustomer.email,
          onSave: (value) {
            setState(() {
              widget.profileViewModel.newCustomer.email = value;
            });
          },
          validator: (value) =>
              isEmail(value!) ? null : 'Please enter a valid email',
        ),
      ],
    );
  }
}

class HomeAddressDetails extends StatefulWidget {
  final ProfileViewModel profileViewModel;
  final int _doorStepDelivery;

  const HomeAddressDetails(this.profileViewModel, this._doorStepDelivery);

  @override
  _HomeAddressDetailsState createState() => _HomeAddressDetailsState();
}

class _HomeAddressDetailsState extends State<HomeAddressDetails> {
  @override
  Widget build(BuildContext context) {
    print(widget.profileViewModel.newHomeAddress);
    return Column(
      children: <Widget>[
        CheckboxListTile(
          value: widget.profileViewModel.newHomeAddress.enabled,
          onChanged: (bool? value) {
            setState(() {
              if (!widget.profileViewModel.homeAddress.enabled && value!) {
                widget.profileViewModel.homeAddressArea =
                    widget.profileViewModel.areas[0];
              }
              widget.profileViewModel.newHomeAddress.enabled = value!;
            });
          },
          title: Text(
            'Home Address',
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        if (widget.profileViewModel.newHomeAddress.enabled)
          Column(
            children: <Widget>[
              CustomDropDownRow<Area>(
                items: widget.profileViewModel.areas
                    .map(
                      (area) => DropdownMenuItem<Area>(
                        child: Text(area.name),
                        value: area,
                      ),
                    )
                    .toList(),
                onChanged: (Area? value) {
                  setState(() {
                    widget.profileViewModel.homeAddressArea = value!;
                  });
                },
                value: widget.profileViewModel.homeAddressArea,
                name: 'Area',
              ),
              CustomDropDownRow<Locality>(
                items: !widget.profileViewModel.newHomeAddress.enabled
                    ? <DropdownMenuItem<Locality>>[]
                    : widget.profileViewModel.homeLocalities
                        .where((locality) =>
                            locality.areaID ==
                            widget.profileViewModel.homeAddressArea.id)
                        .map(
                          (locality) => DropdownMenuItem<Locality>(
                            child: Text(locality.name),
                            value: locality,
                          ),
                        )
                        .toList(),
                onChanged: (Locality? value) {
                  setState(() {
                    widget.profileViewModel.homeAddressLocality = value!;
                  });
                },
                value: widget.profileViewModel.homeAddressLocality,
                name: 'Locality',
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomDropDownRow<Tower>(
                items: !widget.profileViewModel.newHomeAddress.enabled
                    ? <DropdownMenuItem<Tower>>[]
                    : widget.profileViewModel.societies
                        .where((society) =>
                            society.localityID ==
                            widget.profileViewModel.homeAddressLocality.id)
                        .map(
                          (tower) => DropdownMenuItem<Tower>(
                            child: Text(tower.name),
                            value: tower,
                          ),
                        )
                        .toList(),
                onChanged: (Tower? value) {
                  setState(() {
                    widget.profileViewModel.homeAddressTower = value!;
                  });
                },
                value: widget.profileViewModel.homeAddressTower,
                name: 'Society',
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomRow(
                leading: 'Building',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value!)
                    ? null
                    : 'Building must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newHomeAddress.building = value;
                  });
                },
                trailing: widget.profileViewModel.newHomeAddress.building,
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomRow(
                leading: 'Flat No.',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value!)
                    ? null
                    : 'Flat No. must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newHomeAddress.flatNumber = value;
                  });
                },
                trailing: widget.profileViewModel.newHomeAddress.flatNumber,
              ),
              // Padding(padding: const EdgeInsets.all(4)),
              CheckboxListTile(
                value: widget.profileViewModel.newHomeAddress.doorStepDelivery,
                title: Text('Door Step Delivery'),
                subtitle: Text(
                    'Upto $rupeeSymbol ${widget._doorStepDelivery} per Box.'),
                onChanged: (value) {
                  setState(() {
                    widget.profileViewModel.newHomeAddress.doorStepDelivery =
                        value!;
                  });
                },
              ),
              // Padding(padding: const EdgeInsets.all(4)),
            ],
          ),
      ],
    );
  }
}

class OfficeAddressDetails extends StatefulWidget {
  final ProfileViewModel profileViewModel;

  const OfficeAddressDetails(this.profileViewModel);

  @override
  _OfficeAddressDetailsState createState() => _OfficeAddressDetailsState();
}

class _OfficeAddressDetailsState extends State<OfficeAddressDetails> {
  @override
  Widget build(BuildContext context) {
    print(widget.profileViewModel.newOfficeAddress);
    return Column(
      children: <Widget>[
        CheckboxListTile(
          value: widget.profileViewModel.newOfficeAddress.enabled,
          onChanged: (bool? value) {
            setState(() {
              if (!widget.profileViewModel.officeAddress.enabled && value!) {
                widget.profileViewModel.officeAddressArea =
                    widget.profileViewModel.areas[0];
              }
              widget.profileViewModel.newOfficeAddress.enabled = value!;
            });
          },
          title: Text(
            'Office Address',
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        if (widget.profileViewModel.newOfficeAddress.enabled)
          Column(
            children: <Widget>[
              CustomDropDownRow<Area>(
                items: widget.profileViewModel.areas
                    .map(
                      (area) => DropdownMenuItem<Area>(
                        child: Text(area.name),
                        value: area,
                      ),
                    )
                    .toList(),
                onChanged: (Area? value) {
                  setState(() {
                    widget.profileViewModel.officeAddressArea = value!;
                  });
                },
                value: widget.profileViewModel.officeAddressArea,
                name: 'Area',
              ),
              CustomDropDownRow<Locality>(
                items: !widget.profileViewModel.newOfficeAddress.enabled
                    ? <DropdownMenuItem<Locality>>[]
                    : widget.profileViewModel.officeLocalities
                        .where((locality) =>
                            locality.areaID ==
                            widget.profileViewModel.officeAddressArea.id)
                        .map(
                          (locality) => DropdownMenuItem<Locality>(
                            child: Text(locality.name),
                            value: locality,
                          ),
                        )
                        .toList(),
                onChanged: (Locality? value) {
                  setState(() {
                    widget.profileViewModel.officeAddressLocality = value!;
                  });
                },
                value: widget.profileViewModel.officeAddressLocality,
                name: 'Locality',
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomDropDownRow<Tower>(
                items: !widget.profileViewModel.newOfficeAddress.enabled
                    ? <DropdownMenuItem<Tower>>[]
                    : widget.profileViewModel.towers
                        .where((tower) =>
                            tower.localityID ==
                            widget.profileViewModel.officeAddressLocality.id)
                        .map(
                          (tower) => DropdownMenuItem<Tower>(
                            child: Text(tower.name),
                            value: tower,
                          ),
                        )
                        .toList(),
                onChanged: (Tower? value) {
                  setState(() {
                    widget.profileViewModel.officeAddressTower = value!;
                  });
                },
                value: widget.profileViewModel.officeAddressTower,
                name: 'Tower',
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomRow(
                leading: 'Floor',
                validator: (value) => isNumeric(value!) && int.parse(value) > 0
                    ? null
                    : 'Floor must be a positive number',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.floor = value;
                  });
                },
                trailing:
                    widget.profileViewModel.newOfficeAddress.floor.toString(),
                keyboardType: TextInputType.number,
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomRow(
                leading: 'Office No.',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value!)
                    ? null
                    : 'Office no. must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.officeNumber =
                        value;
                  });
                },
                trailing: widget.profileViewModel.newOfficeAddress.officeNumber,
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
              CustomRow(
                leading: 'Company',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value!)
                    ? null
                    : 'Company must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.company = value;
                  });
                },
                trailing: widget.profileViewModel.newOfficeAddress.company,
              ),
              Padding(
                padding: const EdgeInsets.all(4),
              ),
            ],
          ),
      ],
    );
  }
}

/// Custom Row for profile.
class CustomRow extends StatefulWidget {
  /// The thing to display on the left side of the row
  final String leading;

  /// The thing to the right side of the row
  final String trailing;

  /// What to do when use successfully saves the value
  final ValueChanged<String>? onSave;

  /// a validator to validate the form field
  final FormFieldValidator<String>? validator;

  /// type of keyboard
  final TextInputType? keyboardType;

  const CustomRow({
    Key? key,
    this.leading = '',
    this.trailing = '',
    required this.onSave,
    required this.validator,
    this.keyboardType,
  }) : super(key: key);

  @override
  _CustomRowState createState() => _CustomRowState();
}

class _CustomRowState extends State<CustomRow> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.text = widget.trailing;

    _controller.addListener(() {
      if (widget.validator!(_controller.text) == null) {
        widget.onSave!(_controller.text);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _controller,
      textCapitalization: TextCapitalization.words,
      validator: widget.validator,
      keyboardType: widget.keyboardType,
      decoration: InputDecoration(
        labelText: widget.leading,
      ),
      enabled: widget.onSave != null,
    );
  }
}

class CustomDropDownRow<T> extends StatelessWidget {
  final String name;
  final List<DropdownMenuItem<T>> items;
  final ValueChanged<T?>? onChanged;
  final T value;

  const CustomDropDownRow({
    required this.name,
    required this.items,
    this.onChanged,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<T>(
      items: items,
      onChanged: onChanged,
      decoration: InputDecoration(
        labelText: name,
      ),
      value: value,
    );
  }
}
