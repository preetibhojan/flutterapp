

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Models.dart';

abstract class CustomerRepository {
  Future<Customer> get customer;
}

class StubCustomerRepository implements CustomerRepository {
  final Customer input;

  StubCustomerRepository(this.input);

  @override
  Future<Customer> get customer => Future.value(input);
}

class NetworkCustomerRepository implements CustomerRepository {
  static final _log = Logger('Network Customer Repository');

  Customer? _cache;

  @override
  Future<Customer> get customer async {
    _log.info('Get customer details');

    if (_cache != null) {
      _log.info('Returning _cache');
      return _cache!;
    }

    _log.info('getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/customer_details'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Could not fetch customer details');
      throw Exception('Could not fetch customer details');
    }

    return Customer.fromJson(jsonDecode(response.body));
  }
}

abstract class HomeAddressRepository {
  Future<HomeAddress> get homeAddress;
}

class StubHomeAddressRepository implements HomeAddressRepository {
  final HomeAddress input;

  StubHomeAddressRepository(this.input);

  @override
  Future<HomeAddress> get homeAddress => Future.value(input);
}

class NetworkHomeAddressRepository implements HomeAddressRepository {
  static final _log = Logger('Network Home Address Repository');

  HomeAddress? _cache;

  @override
  Future<HomeAddress> get homeAddress async {
    _log.info('Get home address');

    if (_cache != null) {
      _log.info('Return cache');
      return _cache!;
    }

    _log.info('getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/home_address'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Could not fetch customer details');
      throw Exception('Could not fetch customer details');
    }

    return HomeAddress.fromJson(jsonDecode(response.body));
  }
}

abstract class OfficeAddressRepository {
  Future<OfficeAddress> get officeAddress;
}

class StubOfficeAddressRepository implements OfficeAddressRepository {
  final OfficeAddress input;

  StubOfficeAddressRepository(this.input);

  @override
  Future<OfficeAddress> get officeAddress => Future.value(input);
}

class NetworkOfficeAddressRepository implements OfficeAddressRepository {
  static final _log = Logger('Network office address repository');

  OfficeAddress? _cache;

  @override
  Future<OfficeAddress> get officeAddress async {
    _log.info('Get office address');

    if (_cache != null) {
      _log.info('Return cache');
      return _cache!;
    }

    _log.info('getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/office_address'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Could not fetch customer details');
      throw Exception('Could not fetch customer details');
    }

    return OfficeAddress.fromJson(jsonDecode(response.body));
  }
}
