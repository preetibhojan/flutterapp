import 'package:preetibhojan/common/cuisines/Model.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';

import 'Models.dart';
import 'Repository.dart';

class ProfileViewModel {
  late Customer customer;
  late HomeAddress homeAddress;
  late OfficeAddress officeAddress;

  late Customer newCustomer;
  late HomeAddress newHomeAddress;
  late OfficeAddress newOfficeAddress;

  late List<Area> areas;
  late List<Locality> officeLocalities;
  late List<Locality> homeLocalities;
  late List<Tower> societies;
  late List<Tower> towers;

  late List<Cuisine> cuisines;

  late Tower _homeAddressTower;

  Tower get homeAddressTower => _homeAddressTower;

  set homeAddressTower(Tower value) {
    _homeAddressTower = value;
    newHomeAddress.societyID = _homeAddressTower.id;
  }

  late Locality _homeAddressLocality;

  Locality get homeAddressLocality => _homeAddressLocality;

  set homeAddressLocality(Locality value) {
    /// this setter sets newHomeAddress.societyID
    homeAddressTower = societies.firstWhere(
      (society) => society.localityID == value.id,
    );

    _homeAddressLocality = value;
  }

  late Area _homeAddressArea;

  Area get homeAddressArea => _homeAddressArea;

  set homeAddressArea(Area value) {
    /// this setter sets _homeAddressLocality which in turn sets
    /// newHomeAddress.societyID
    homeAddressLocality = homeLocalities.firstWhere(
      (locality) => locality.areaID == value.id,
    );

    _homeAddressArea = value;
  }

  late Tower _officeAddressTower;

  Tower get officeAddressTower => _officeAddressTower;

  set officeAddressTower(value) {
    _officeAddressTower = value;
    newOfficeAddress.towerID = _officeAddressTower.id;
  }

  late Locality _officeAddressLocality;

  Locality get officeAddressLocality => _officeAddressLocality;

  set officeAddressLocality(value) {
    _officeAddressLocality = value;

    /// this setter sets newHomeAddress.towerID
    officeAddressTower = towers.firstWhere(
      (tower) => tower.localityID == officeAddressLocality.id,
    );
  }

  late Area _officeAddressArea;

  Area get officeAddressArea => _officeAddressArea;

  set officeAddressArea(value) {
    _officeAddressArea = value;

    /// this setter sets officeAddressLocality which in turn sets
    /// newHomeAddress.towerID
    officeAddressLocality = officeLocalities.firstWhere(
      (locality) => locality.areaID == officeAddressArea.id,
    );
  }

  final CustomerRepository customerRepository;
  final OfficeAddressRepository officeAddressRepository;
  final HomeAddressRepository homeAddressRepository;
  final CuisineRepository cuisineRepository;

  final AreaRepository areaRepository;

  ProfileViewModel(
    this.customerRepository,
    this.officeAddressRepository,
    this.homeAddressRepository,
    this.cuisineRepository,
    this.areaRepository,
  );

  Future<void> fetchData() async {
    customer = await customerRepository.customer;
    newCustomer = Customer.clone(customer);

    homeAddress = await homeAddressRepository.homeAddress;
    newHomeAddress = HomeAddress.clone(homeAddress);

    officeAddress = await officeAddressRepository.officeAddress;
    newOfficeAddress = OfficeAddress.clone(officeAddress);

    cuisines = await cuisineRepository.cuisines;

    areas = await areaRepository.area;
    areas.sort((a, b) => a.name.compareTo(b.name));

    final tempLocalities = await areaRepository.allLocalities;

    homeLocalities =
        tempLocalities.where((element) => element.home == true).toList();
    homeLocalities.sort((a, b) => a.name.compareTo(b.name));

    officeLocalities =
        tempLocalities.where((element) => element.office == true).toList();
    officeLocalities.sort((a, b) => a.name.compareTo(b.name));

    final tempTowers = await areaRepository.towers;

    societies = tempTowers.where((element) => element.home == true).toList();
    societies.sort((a, b) => a.name.compareTo(b.name));

    final localityIDsInSocieties = societies.map((e) => e.localityID).toSet();
    homeLocalities.removeWhere((locality) => !localityIDsInSocieties.contains(locality.id));

    towers = tempTowers.where((element) => element.home == false).toList();
    towers.sort((a, b) => a.name.compareTo(b.name));

    final localityIDsInTowers = towers.map((e) => e.localityID).toSet();
    officeLocalities.removeWhere((locality) => !localityIDsInTowers.contains(locality.id));

    setAreaAndLocality();
  }

  void reset() {
    newCustomer = Customer.clone(customer);
    newHomeAddress = HomeAddress.clone(homeAddress);
    newOfficeAddress = OfficeAddress.clone(officeAddress);

    setAreaAndLocality();
  }

  void setAreaAndLocality() {
    if (newHomeAddress.enabled) {
      final _society = societies.firstWhere(
        (society) => society.id == newHomeAddress.societyID,
      );

      final _locality = homeLocalities.firstWhere(
        (locality) => locality.id == _society.localityID,
      );

      homeAddressArea = areas.firstWhere(
        (area) => area.id == _locality.areaID,
      );

      homeAddressLocality = _locality;
      homeAddressTower = _society;
    }

    if (newOfficeAddress.enabled) {
      final _tower = towers.firstWhere(
        (tower) => tower.id == newOfficeAddress.towerID,
      );

      final _locality = officeLocalities.firstWhere(
        (locality) => locality.id == _tower.localityID,
      );

      officeAddressArea = areas.firstWhere(
        (area) => area.id == _locality.areaID,
      );

      officeAddressLocality = _locality;

      officeAddressTower = _tower;
    }
  }
}
