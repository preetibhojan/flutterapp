import 'package:preetibhojan/util/string.dart';

/// If you make any change here, remember to change register and backend
class Customer {
  final String phoneNumber, firstName, lastName;
  int cuisineID;

  /// email is the only field customer is allowed to change
  String email;

  Customer(this.phoneNumber, this.firstName, this.lastName, this.email,
      this.cuisineID);

  factory Customer.clone(Customer customer) {
    return Customer(
      customer.phoneNumber,
      customer.firstName,
      customer.lastName,
      customer.email,
      customer.cuisineID,
    );
  }

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      json['phone_number'],
      json['first_name'],
      json['last_name'],
      json['email'],
      json['cuisine_id'],
    );
  }
}

/// If you make any change here, remember to change register and backend
class HomeAddress {
  String building, flatNumber;
  int societyID;
  bool enabled = true;
  bool doorStepDelivery;
  final String society;
  final String locality;
  final String area;

  HomeAddress(
    this.building,
    this.flatNumber,
    this.societyID,
    this.doorStepDelivery,
    this.society,
    this.locality,
    this.area,
  );

  factory HomeAddress.disabled() {
    final temp = HomeAddress('', '', -1, false, '', '', '');
    temp.enabled = false;
    return temp;
  }

  @override
  String toString() {
    return 'HomeAddress{building: $building, flatNumber: $flatNumber, society: $societyID, enabled: $enabled}';
  }

  factory HomeAddress.clone(HomeAddress homeAddress) {
    final temp = HomeAddress(
      homeAddress.building,
      homeAddress.flatNumber,
      homeAddress.societyID,
      homeAddress.doorStepDelivery,
      homeAddress.society,
      homeAddress.locality,
      homeAddress.area,
    );

    temp.enabled = homeAddress.enabled;

    return temp;
  }

  Map<String, dynamic> toJson() {
    return {
      'building': building,
      'flat_number': flatNumber,
      'society_id': societyID,
      'door_step_delivery': doorStepDelivery,
    };
  }

  factory HomeAddress.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('enabled')) {
      return HomeAddress.disabled();
    }

    return HomeAddress(
      json['building'],
      json['flat_number'],
      json['society_id'],
      json['door_step_delivery'] ?? false,
      json['society'],
      json['locality'],
      json['area'],
    );
  }

  bool isValid() {
    return isAlphaNumericUnderScoreOrSpace(building) &&
        isAlphaNumericUnderScoreOrSpace(flatNumber);
  }
}

/// If you make any change here, remember to change register and backend
class OfficeAddress {
  String officeNumber, company, floor;
  int towerID;
  bool enabled = true;

  final String tower;
  final String locality;
  final String area;

  OfficeAddress(
    this.floor,
    this.officeNumber,
    this.company,
    this.towerID,
    this.tower,
    this.locality,
    this.area,
  );

  factory OfficeAddress.disabled() {
    final temp = OfficeAddress('', '', '', -1, '', '', '');
    temp.enabled = false;
    return temp;
  }

  @override
  String toString() {
    return 'OfficeAddress{officeNumber: $officeNumber, company: $company, floor: $floor, enabled: $enabled}';
  }

  factory OfficeAddress.clone(OfficeAddress officeAddress) {
    final temp = OfficeAddress(
      officeAddress.floor,
      officeAddress.officeNumber,
      officeAddress.company,
      officeAddress.towerID,
      officeAddress.tower,
      officeAddress.locality,
      officeAddress.area,
    );

    temp.enabled = officeAddress.enabled;

    return temp;
  }

  Map<String, dynamic> toJson() {
    return {
      'floor': int.parse(floor),
      'office_number': officeNumber,
      'company': company,
      'tower_id': towerID,
    };
  }

  factory OfficeAddress.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('enabled')) {
      return OfficeAddress.disabled();
    }

    return OfficeAddress(
      json['floor'].toString(),
      json['office_number'],
      json['company'],
      json['tower_id'],
      json['tower'],
      json['locality'],
      json['area'],
    );
  }

  bool isValid() {
    return isAlphaNumericUnderScoreOrSpace(officeNumber) &&
        isAlphaNumericUnderScoreOrSpace(company) &&
        int.parse(floor) > 0;
  }
}
