import 'dart:collection';
import 'dart:math';

import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/schedule/Models.dart';
import 'package:preetibhojan/common/schedule/Repositories.dart';
import 'package:preetibhojan/util/datetime.dart';

DateTime nextSunday(DateTime now) {
  if (now.weekday == DateTime.sunday) {
    return now.add(Duration(days: 7));
  }

  return now.add(Duration(days: 7 - now.weekday));
}

class BookingViewModel {
  static final _log = Logger('Booking view model');

  // ignore: non_constant_identifier_names
  static final GST = config.gst;

  final ScheduleRepository scheduleRepository;
  final DeliverableRepository deliverableRepository;

  var costBreakup = <String, double>{};

  late DateTime dateToBookFrom, now;

  BookingViewModel(
    this.scheduleRepository,
    this.deliverableRepository,
    now,
  ) {
    assert(now != null);
    this.now = now;
    dateToBookFrom = now.add(Duration(days: 1));
  }

  late Schedule schedule;

  /// SplayTreeMap used to maintain priority of meals
  final priceOf = SplayTreeMap<Meal, int>();

  late DateTime till;
  double creditAvailable = 0;

  Future<void> fetchData() async {
    schedule = await scheduleRepository.schedule;

    final deliverables = await deliverableRepository.deliverablesFromTomorrow;
    final costOfDeliverables = deliverables.isNotEmpty
        ? creditAvailable += deliverables
            .map((d) => d.price)
            .reduce((value, element) => value + element)
        : 0;

    creditAvailable =
        schedule.creditAvailable + costOfDeliverables + schedule.depositPaid;

    till = dateToBookFrom;

    if (schedule.packages.length > 0) {
      calculateCost();
    }
  }

  double totalExcludingTaxAndDeposit = 0;
  double grandTotal = 0;
  double tax = 0;
  double deliveryCharge = 0;
  double depositToMaintain = 0;
  double addOnTotal = 0;

  void calculateCost() {
    final _daysRequiredFor = _daysRequired();

    const minimum_days_at_least_one_package_needs_to_be_ordered = 3;

    final maximumDaysAPackageIsOrdered = _daysRequiredFor.values.reduce(max);

    final _nextSunday = nextSunday(now);
    if (maximumDaysAPackageIsOrdered <
        minimum_days_at_least_one_package_needs_to_be_ordered) {
      schedule.packages.forEach((meal, packageInfo) {
        _daysRequiredFor[meal] =
            _daysRequiredFor[meal]! + packageInfo[0].deliveryInfo.length;
      });

      till = _nextSunday.add(Duration(days: 7));
    } else {
      till = _nextSunday;
    }

    priceOf.clear();

    totalExcludingTaxAndDeposit = 0;
    grandTotal = 0;
    deliveryCharge = 0;

    schedule.packages.forEach((meal, packageInfo) {
      final price = packageInfo.map((p) => p.price * p.quantity).reduce((a, b) => a + b);
      priceOf[meal] = price * _daysRequiredFor[meal]!;
      totalExcludingTaxAndDeposit += priceOf[meal]!;
    });

    schedule.addOns.forEach((meal, packageInfo) {
      if (packageInfo.length > 0) {
        final addOnPrice =
            packageInfo.map((package) => package.price * package.quantity).reduce((a, b) => a + b);

        priceOf[meal] = priceOf[meal]! + (addOnPrice * _daysRequiredFor[meal]!);
        addOnTotal += addOnPrice * _daysRequiredFor[meal]!;
      }
    });

    for (var _date = setTimeToZero(dateToBookFrom);
        _date.isBefore(setTimeToZero(till)) || _date.isAtSameMomentAs(setTimeToZero(till));
        _date = _date.add(Duration(days: 1))) {
      /// Add ons don't have delivery charge
      schedule.packages.forEach((_, packages) {
        for (final package in packages) {
          final temp = package.deliveryInfo[Day.fromInt(_date.weekday)];
          if (temp != null) {
            deliveryCharge += temp.deliveryCharge * package.quantity;
            tax += (package.price + temp.deliveryCharge) * package.quantity * config.gst[temp.deliveryLocation]! / 100;
          }
        }
      });
    }

    totalExcludingTaxAndDeposit += deliveryCharge;

    depositToMaintain = schedule.packages.keys
        .map((meal) => meal.deposit)
        .reduce((a, b) => a + b)
        .toDouble();

    grandTotal = totalExcludingTaxAndDeposit +
        addOnTotal +
        tax +
        depositToMaintain -
        creditAvailable;

    _log.info('Days required: $_daysRequiredFor');
  }

  Map<Meal, int> _daysRequired() {
    Map<Meal, int> daysRequired = {};

    schedule.packages.forEach((meal, packageInfo) {
      /// We do not give users the option of choosing days required per
      /// package. So the first is the same as all others.
      daysRequired[meal] = packageInfo[0].deliveryInfo.keys.where((day) {
        return day.weekday >= dateToBookFrom.weekday;
      }).length;
    });

    return daysRequired;
  }
}
