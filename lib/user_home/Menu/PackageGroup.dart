import 'package:async_builder/async_builder.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/user_home/Cart/Models.dart';
import 'package:preetibhojan/user_home/Menu/GroupDetails.dart';
import 'package:preetibhojan/user_home/Menu/PackageGroupRepository.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';

class PackageGroup extends StatefulWidget {
  final PackageGroupRepository _repository;
  final Meal _meal;
  final List<Package> _packages;
  final Cart _cart;

  const PackageGroup(
    this._repository,
    this._meal,
    this._packages,
    this._cart,
  );

  @override
  State<PackageGroup> createState() => _PackageGroupState();
}

class _PackageGroupState extends State<PackageGroup> {
  late Future<void> _future = _init();

  late final List<Group> _groups;

  Future<void> _init() async {
    final temp = await widget._repository.groups;
    final groupsInPackages = widget._packages.map((p) => p.groupID).toSet();
    _groups = temp.where((g) => groupsInPackages.contains(g.id)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: AsyncBuilder<void>(
          future: _future,
          waiting: (context) => Center(
            child: CircularProgressIndicator(),
          ),
          builder: (context, _) => Column(
            children: [
              GridView.count(
                childAspectRatio: 1.2,
                crossAxisCount: 2,
                shrinkWrap: true,
                children: _groups
                    .map(
                      (group) => GestureDetector(
                        onTap: () => navigateToGroup(group),
                        child: Card(
                          child: Column(
                            children: [
                              SizedBox(height: 4),
                              CachedNetworkImage(
                                height: 100,
                                width: 130,
                                imageUrl: group.imageUrl,
                                progressIndicatorBuilder:
                                    (context, url, downloadProgress) => Center(
                                  child: CircularProgressIndicator(
                                    value: downloadProgress.progress,
                                  ),
                                ),
                                errorWidget: (context, url, error) => Container(
                                  height: 100,
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(height: 8),
                              Center(child: Text(group.name))
                            ],
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void navigateToGroup(Group group) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => GroupDetails(
          widget._packages.where((p) => p.groupID == group.id).toList(),
          group,
          widget._cart,
          widget._meal,
        ),
      ),
    );
  }
}
