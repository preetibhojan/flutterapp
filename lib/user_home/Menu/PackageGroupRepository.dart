import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';

class Group {
  final int id;
  final String name;
  final String imageUrl;

  Group(this.id, this.name, this.imageUrl);

  factory Group.fromJson(Map<String, dynamic> json) => Group(
        json['id'],
        json['name'],
        json['image_url'],
      );
}

class PackageGroupRepository {
  static final _log = Logger('Package group repository');

  List<Group> _cache = [];

  Future<List<Group>> get groups async {
    if (_cache.length > 0) {
      _log.info('Package group cache is not empty. Returning cache');
      return _cache;
    }

    final response = await http.get(Uri.parse(base_url + '/package_groups'));

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Could not get package groups');
      throw StateError('Could not get package groups');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => Group.fromJson(json))
        .toList();

    return _cache;
  }
}
