import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/menu/Model.dart';
import 'package:preetibhojan/common/menu/Repositories.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/util/datetime.dart';

class MenuViewModel {
  final MenuRepository menuRepository;
  final PackageRepository packageRepository;
  final MealsRepository mealsRepository;
  final DateTime now;

  MenuViewModel(
    this.now,
    this.menuRepository,
    this.packageRepository,
    this.mealsRepository,
  );

  late List<Package> packages;
  late List<Menu> menu;
  late List<Meal> meals;
  DateTime? date;

  DateTime _dateToUse(DateTime now) {
    if (now.hour < 20) {
      return setTimeToZero(now);
    }

    return now.nextWorkingDay;
  }

  Future<void> fetchData() async {
    meals = await mealsRepository.meals;
    meals.sort();

    date = _dateToUse(now);
    menu = (await menuRepository.menu)
        .where((menu) => menu.date == date)
        .toList();

    final temp = await packageRepository.packages;
    packages = (temp)
        .where((package) => package.date == date)
        .toList();
  }
}
