import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';

class NotServing {
  final DateTime tillDate;
  final String message;
  final String? message2, message3;

  NotServing(this.tillDate, this.message, this.message2, this.message3);

  static NotServing? fromJson(Map<String, dynamic> json) {
    if (json.isEmpty) {
      return null;
    }
    return NotServing(
      DateFormat('yyyy-MM-dd').parse(json['till']),
      json['message'],
      json['message2'],
      json['message3'],
    );
  }
}

typedef Future<NotServing?> NotServingRepository();

final _log = Logger('not serving repository');

Future<NotServing?> notServingRepository() async {
  final response = await http.get(Uri.parse(base_url + '/not_serving'));
  if (response.statusCode != 200) {
    _log.info('could not get not serving');
    throw StateError('Could not get not serving');
  }

  return NotServing.fromJson(jsonDecode(response.body));
}
