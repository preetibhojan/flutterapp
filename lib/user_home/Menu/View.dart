import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:launch_review/launch_review.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/menu/Model.dart';
import 'package:preetibhojan/user_home/Cart/Models.dart';
import 'package:preetibhojan/user_home/Menu/Repository.dart';
import 'package:preetibhojan/user_home/Menu/ViewModel.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/BottomNavBar.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

typedef DateTime GetCurrentDateTime();

class MenuScreen extends StatefulWidget {
  final MenuViewModel menuViewModel;
  final GetCurrentDateTime getCurrentDateTime;
  final NotServingRepository notServingRepository;
  final Cart cart;

  const MenuScreen(
    this.menuViewModel,
    this.getCurrentDateTime,
    this.notServingRepository,
    this.cart,
  );

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

extension on List {
  List pad<T>(int length, T value) {
    final temp = <T>[];
    for (var i = 0; i < this.length; i++) {
      temp.add(this[i]);
    }

    for (var i = 0; i < length - this.length; i++) {
      temp.add(value);
    }

    return temp;
  }
}

class _MenuScreenState extends State<MenuScreen> {
  static final _log = Logger('Menu screen');

  late Future _future;

  late User user;
  NotServing? notServing;

  late final now = widget.getCurrentDateTime();

  Future<void> _init() async {
    user = await getUser();
    await FirebaseCrashlytics.instance.setUserIdentifier(user.phoneNumber);
    await widget.menuViewModel.fetchData();
    notServing = await widget.notServingRepository();
    await widget.cart.loadData();

    dateToUse = widget.menuViewModel.date!;

    menuForAllMeals = widget.menuViewModel.menu
        .where((menu) => menu.date == dateToUse)
        .toList();

    if (notServing != null) {
      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  notServing!.message,
                  textAlign: TextAlign.justify,
                ),
                if (notServing!.message2 != null) ...[
                  SizedBox(height: 8),
                  Text(
                    notServing!.message2!,
                    textAlign: TextAlign.justify,
                  ),
                ],
                if (notServing!.message3 != null) ...[
                  SizedBox(height: 8),
                  Text(
                    notServing!.message3!,
                    textAlign: TextAlign.justify,
                  ),
                ],
              ],
            ),
            actions: [
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      });

      return;
    }

    if (menuForAllMeals.length == 0) {
      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text('Oops'),
            content: Text(
              'We are not serving on ${DateFormat.yMMMd().format(dateToUse)}',
            ),
            actions: [
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
        );
      });
    }
  }

  late DateTime dateToUse;
  late List<Menu> menuForAllMeals;

  @override
  void initState() {
    super.initState();
    _future = _init();

    Future.delayed(Duration(seconds: 1), () {
      if (config.version > version) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('New version of app available'),
            actions: [
              TextButton(
                child: Text('Update Now'),
                onPressed: () {
                  LaunchReview.launch();
                },
              ),
              TextButton(
                child: Text('Update Later'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (context, snapshot) {
        _log.info(snapshot);
        if (snapshot.hasError) {
          if (snapshot.error is Error) {
            _log.severe((snapshot.error as Error).stackTrace);
          } else {
            _log.severe('something went wrong: ', snapshot.error);
          }

          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (_) => SomethingWentWrong(),
            );
          });

          return CustomScaffold(
            body: Container(),
            bottomNavBar: BottomNavBar(BottomNavBarItem.Home),
          );
        }

        if (snapshot.connectionState == ConnectionState.done &&
            !snapshot.hasError) {
          if (notServing != null) {
            return CustomScaffold(
              body: Container(),
              bottomNavBar: BottomNavBar(BottomNavBarItem.Home),
            );
          }

          final tomorrow = setTimeToZero(now.add(Duration(days: 1)));

          final dateAsString = dateToUse == setTimeToZero(now)
              ? 'today'
              : dateToUse == setTimeToZero(tomorrow)
                  ? 'tomorrow'
                  : DateFormat('EEEE').format(dateToUse);

          return CustomScaffold(
            /// duplicated in multiple places
            floatingActionButton: FloatingActionButton(
              onPressed: navigateToCart,
              child: Icon(Icons.shopping_cart),
              foregroundColor: secondaryColor,
            ),
            bottomNavBar: BottomNavBar(BottomNavBarItem.Home),
            body: ListView(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(
                      dateAsString.capitalize + '\'s Menu',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                ...widget.menuViewModel.meals.map<Widget>((meal) {
                  final menuForMeal = menuForAllMeals.firstWhereOrNull(
                    (element) => element.mealID == meal.id,
                  );

                  List<String> menuItems = List<String>.filled(4, '');
                  if (menuForMeal != null) {
                    menuItems = [
                      menuForMeal.sabji,
                      menuForMeal.daal,
                      menuForMeal.rice,
                      menuForMeal.sweet,
                    ]
                        .where((item) => item != null && item != '')
                        .toList()
                        .pad(4, '') as List<String>;
                  }

                  return Hero(
                    tag: meal.name,
                    child: CardTile(
                      padding: const EdgeInsets.all(2),
                      title: Column(
                        children: <Widget>[
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
                            child: Text(
                              '${meal.name}',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleLarge!
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: CachedNetworkImage(
                                  height: 100,
                                  width: 130,
                                  imageUrl: meal.imageUrl!,
                                  progressIndicatorBuilder:
                                      (context, url, downloadProgress) =>
                                          Center(
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress,
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Container(
                                    height: 100,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: DefaultTextStyle(
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(2),
                                        child: Text(menuItems[0]),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(2),
                                        child: Text(menuItems[1]),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(2),
                                        child: Text(menuItems[2]),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(2),
                                        child: Text(menuItems[3]),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Row(
                              children: [
                                Flexible(
                                  fit: FlexFit.tight,
                                  child: Center(
                                    child: ElevatedButton(
                                      // child: Text('Order for Today'),
                                      child: Text('Order'),
                                      onPressed: () => _choosePackage(meal,
                                          dateToUse, menuForMeal, dateAsString),
                                    ),
                                  ),
                                ),
                                Flexible(
                                  fit: FlexFit.tight,
                                  child: Center(
                                    child: ElevatedButton(
                                      child: Text(user.isRegularCustomer
                                          ? 'Re Schedule'
                                          : 'Schedule'),
                                      onPressed:
                                          !meal.enableSchedule(now)
                                              ? null
                                              : () {
                                                  navigateToSchedule(meal);
                                                },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ],
            ),
          );
        }

        return CustomScaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
          bottomNavBar: BottomNavBar(BottomNavBarItem.Home),
        );
      },
    );
  }

  void _choosePackage(
      Meal meal, DateTime dateToUse, Menu? menuForMeal, String dateAsString) {
    if (!meal.active) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text(
            '${meal.name} is not served currently. '
            'Will be starting Soon',
          ),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    if (menuForMeal == null) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text(
            '${meal.name} is not available for $dateAsString',
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    /// Using getCurrentDateTime function instead of
    /// widget.now to ensure I have latest time. Other
    /// uses of widget.now only care about the date so
    /// its fine. But here, I care about the precise
    /// time
    final orderBefore = meal.orderBeforeWRT(dateToUse);
    if (orderBefore.isBefore(widget.getCurrentDateTime())) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '${meal.name} must be ordered from Previous Day '
                '8:00 PM to ${DateFormat('jm').format(orderBefore)}.',
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Wrap(
                  children: [
                    Text(
                      'Please WhatsApp your requirement on ',
                    ),
                    Text(
                      '7030700477',
                      style: TextStyle(
                        color: Colors.lime,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      ' to check availability',
                    )
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    final packagesForMeal = widget.menuViewModel.packages
        .where(
          (package) => package.mealId == meal.id,
        )
        .toList();

    if (packagesForMeal.length == 0) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Sorry'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('${meal.name} is SOLD OUT'),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                child: Wrap(
                  children: [
                    Text(
                      'Please WhatsApp your requirement on ',
                    ),
                    Text(
                      '7030700477',
                      style: TextStyle(
                        color: Colors.lime,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      ' to check availability',
                    )
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    navigateToChooseDeliveryLocation(
      meal,
      packagesForMeal,
      widget.cart,
    );
  }
}
