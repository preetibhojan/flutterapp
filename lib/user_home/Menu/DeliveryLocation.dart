import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/user_home/Cart/Models.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/user_home/Profile/Models.dart';
import 'package:preetibhojan/user_home/Profile/Repository.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';

class ChooseDeliveryLocation extends StatefulWidget {
  final HomeAddressRepository _homeAddressRepository;
  final OfficeAddressRepository _officeAddressRepository;
  final Meal _meal;
  final List<Package> _packages;
  final Cart _cart;

  ChooseDeliveryLocation(
    this._homeAddressRepository,
    this._officeAddressRepository,
    this._meal,
    this._packages,
    this._cart,
  );

  @override
  State<ChooseDeliveryLocation> createState() => _ChooseDeliveryLocationState();
}

class _ChooseDeliveryLocationState extends State<ChooseDeliveryLocation> {
  late final Future<void> _future = _init();

  late HomeAddress _homeAddress;
  late OfficeAddress _officeAddress;

  Future<void> _init() async {
    _homeAddress = await widget._homeAddressRepository.homeAddress;
    _officeAddress = await widget._officeAddressRepository.officeAddress;
  }

  late DeliveryLocation? _deliveryLocation = widget._cart.deliveryLocationOf[widget._meal];

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(
          child: CircularProgressIndicator(),
        ),
        error: (context, error, stacktrace) => Center(
          child: Column(
            children: [
              Text(
                  'Something went wrong while fetching delivery locations: $error'),
              Text('$stacktrace'),
            ],
          ),
        ),
        builder: (context, _) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: [
                Text(
                  'Delivery Location',
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 8),
                if (_homeAddress.enabled)
                  _RadioListTile(
                    'Home',
                    DeliveryLocation.Home,
                    _deliveryLocation,
                    '${_homeAddress.flatNumber}, ${_homeAddress.building}',
                    _homeAddress.society,
                    _homeAddress.locality,
                    _homeAddress.area,
                    onChanged: (value) {
                      setState(() {
                        _deliveryLocation = value;
                      });
                    },
                  ),
                if (_officeAddress.enabled)
                  _RadioListTile(
                    'Office',
                    DeliveryLocation.Office,
                    _deliveryLocation,
                    _officeAddress.company,
                    _officeAddress.tower,
                    _officeAddress.locality,
                    _officeAddress.area,
                    onChanged: (value) {
                      setState(() {
                        _deliveryLocation = value;
                      });
                    },
                  ),
                Center(
                    child:
                        ElevatedButton(onPressed: _next, child: Text('Next'))),
              ],
            ),
          );
        },
      ),
    );
  }

  void _next() {
    if (_deliveryLocation == null) {
      return;
    }

    widget._cart.setDeliveryLocationOf(widget._meal, _deliveryLocation!);

    navigateToPackageGroup(
      widget._meal,
      widget._packages,
      widget._cart,
    );
  }
}

class _RadioListTile extends StatelessWidget {
  final String title;
  final DeliveryLocation value;
  final DeliveryLocation? groupValue;
  final String line1;
  final String line2;
  final String line3;
  final String line4;
  final ValueChanged<DeliveryLocation?> onChanged;

  const _RadioListTile(
    this.title,
    this.value,
    this.groupValue,
    this.line1,
    this.line2,
    this.line3,
    this.line4, {
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Radio<DeliveryLocation>(
              value: value,
              groupValue: groupValue,
              onChanged: onChanged,
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(56, 0, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(line1),
              SizedBox(height: 2),
              Text(line2),
              SizedBox(height: 2),
              Text(line3),
              SizedBox(height: 2),
              Text(line4),
            ],
          ),
        ),
      ],
    );
  }
}
