import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/QuantityManager.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/user_home/Cart/Models.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/QuantityPicker.dart';

import 'PackageGroupRepository.dart';

class GroupDetails extends StatefulWidget {
  final List<Package> _packages;
  final Group _group;
  final Cart _cart;
  final Meal _meal;

  const GroupDetails(
    this._packages,
    this._group,
    this._cart,
    this._meal,
  );

  @override
  State<GroupDetails> createState() => _GroupDetailsState();
}

class _GroupDetailsState extends State<GroupDetails> {
  @override
  void initState() {
    super.initState();
    widget._packages.sort((a, b) => a.id.compareTo(b.id));
  }

  late final _quantityManager = QuantityManager(widget._cart, widget._meal);

  /// Current required package. Needs to be maintained by parent.
  /// Used as group value for radio buttons
  List<CartItem> get cartItems => widget._cart.items[widget._meal] ?? [];

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await navigateToCart();

          setState(() {});
        },
        child: Icon(Icons.shopping_cart),
        foregroundColor: secondaryColor,
      ),
      body: ListView(
        children: [
          CachedNetworkImage(
            imageUrl: widget._group.imageUrl,
            progressIndicatorBuilder: (context, url, downloadProgress) {
              return Center(
                child: CircularProgressIndicator(
                  value: downloadProgress.progress,
                ),
              );
            },
            errorWidget: (context, url, error) => Container(
              height: 100,
              color: Colors.black,
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget._group.name,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
          ),

          ...widget._packages
              .where((package) => package.groupID == widget._group.id)
              .map((package) {
            final cartItem = cartItems.length == 0
                ? null
                : cartItems.firstWhereOrNull(
                  (item) => item.package == package,
            );

            final price = package.price + widget._cart.deliveryChargeOf[widget._meal.id]![widget._cart.deliveryLocationOf[widget._meal]]!;

            var quantity = (cartItem != null ? cartItem.quantity : 1);

            /// If current quantity available for package is less than
            /// the quantity in cart, use the available quantity
            quantity = min(quantity, package.quantityAvailable!);

            final _onTap = () {
              setState(() {
                if (cartItem == null &&
                    _quantityManager.quantityAvailable(package, 0) >
                        0) {
                  widget._cart.set(widget._meal, package, 1);
                } else if (cartItem != null) {
                  widget._cart.delete(widget._meal, package);
                  _quantityManager.updateQuantity();
                }
              });
            };

            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: _onTap,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Checkbox(
                          onChanged: (value) => _onTap(),
                          value: cartItem != null,
                        ),
                        Text(
                          package.name,
                          style: TextStyle(fontSize: 16),
                        ),
                        Spacer(),
                        Container(
                          margin:
                          const EdgeInsets.fromLTRB(10, 0, 30, 0),
                          padding: const EdgeInsets.all(8),
                          child: Visibility(
                            visible: cartItem != null,
                            child: QuantityPicker(
                              onIncreaseQuantity: () {
                                setState(() {
                                  _quantityManager
                                      .increaseQuantity(cartItem!);
                                });
                              },
                              onDecreaseQuantity: () {
                                setState(() {
                                  _quantityManager
                                      .decreaseQuantity(cartItem!);
                                });
                              },
                              value: quantity,
                              borderColor: Colors.white,
                              textColor: Colors.white,
                            ),
                          ),
                        ),
                        Text(
                          rupeeSymbol + (price * quantity).toString(),
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                      child: Align(
                        child: Text(package.description),
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }).toList()
        ],
      ),
    );
  }
}
