import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Storage.dart';

import '../Config.dart';
import 'Model.dart';

abstract class PackageRepository {
  Future<List<Package>> get packages;

  void invalidateCache();
}

class StubPackageRepository implements PackageRepository {
  final List<Package> input;

  StubPackageRepository({required this.input});

  @override
  Future<List<Package>> get packages => Future.value(input);

  @override
  void invalidateCache() {}
}

class NetworkRegularPackagesRepository implements PackageRepository {
  static final _log = Logger('NetworkPackageRepository');

  List<Package> _cache = <Package>[];

  @override
  Future<List<Package>> get packages async {
    _log.info('Getting packages');

    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache.toList();
    }

    _log.info('Getting user');
    final user = await getUser();

    _log.info('Sending post request to packages');
    final response = await http.post(
      Uri.parse(prefix + '/v2/packages'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map((json) => Package.fromJson(json))
          .toList();
      return _cache;
    }

    _log.severe('Something went wrong ${response.statusCode}');
    throw Exception('Failed to load packages');
  }

  @override
  void invalidateCache() {
    _cache.clear();
  }
}

class NetworkAllPackagesRepository implements PackageRepository {
  static final _log = Logger('NetworkPackageRepository');

  List<Package> _cache = <Package>[];

  @override
  Future<List<Package>> get packages async {
    _log.info('Getting packages');

    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Sending get request to packages');

    final response = await http.get(Uri.parse(base_url + '/all_packages'));

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map((json) => Package.fromJson(json))
          .toList();
      return _cache;
    }

    _log.severe('Something went wrong ${response.statusCode}');
    throw Exception('Failed to load packages');
  }

  @override
  void invalidateCache() {
    _cache.clear();
  }
}

/// Adds need based customer delta to price of packages
class NetworkPackagesForMenuRepository implements PackageRepository {
  List<Package> _cache = <Package>[];

  static final _log = Logger('Network packages for menu repository');

  @override
  Future<List<Package>> get packages async {
    _log.info('Getting packages');

    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Getting user');
    final user = await getUser();

    _log.info('sending post request to packages for menu');
    final response = await http.post(
      Uri.parse(base_url + '/packages_for_menu'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map((json) => Package.fromJson(json))

          .map(
            (package) => package.copyWith(
              price: package.price +
                  (package.addOn
                      ? 0
                      : config.needBasedCustomerDelta[package.mealId]!),
            ),
          )
          .toList();
      return _cache;
    }

    _log.severe('Something went wrong ${response.statusCode}');
    throw Exception('Failed to load packages');
  }

  @override
  void invalidateCache() {
    _cache.clear();
  }
}

class AdminPackageForMenu implements PackageRepository {
  List<Package> _cache = <Package>[];

  static final _log = Logger('Admin Package For Menu');

  @override
  Future<List<Package>> get packages async {
    _log.info('getting packages for admin');
    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Getting user');
    final user = await getUser();

    _log.info('sending post request for menu');

    final response = await http.post(
      Uri.parse(base_url + '/admin/packages_for_menu'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch packages for admin');
      throw Exception('Could not fetch packages for admin');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => Package.fromJson(json))
        .toList();

    return _cache;
  }

  @override
  void invalidateCache() {
    _cache.clear();
  }
}
