class Package {
  final int id, cuisineID, mealId, groupID;
  int? quantityAvailable;
  final double price;
  final String name, description, group;
  final DateTime? date;
  final bool? complimentary;
  final bool addOn;

  /// only for admin use
  final String? displayName;

  Package(
    this.id,
    this.name,
    this.description,
    this.price,
    this.cuisineID,
    this.mealId,
    this.group,
    this.quantityAvailable,
    this.date,
    this.complimentary,
    this.addOn,
    this.groupID, [
    this.displayName,
  ]);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Package && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'Name: $name Description: $description Price: $price';
  }

  factory Package.fromJson(Map<String, dynamic> json) {
    return Package(
      json['id'],
      json['name'],
      json['description'],
      json['price'].toDouble(),
      json['cuisine_id'],
      json['meal_id'],
      json['group'],
      json['quantity'] ?? 0,
      json['date'] != null ? DateTime.parse(json['date']) : null,
      json['complimentary'],
      json['add_on'],
      json['group_id'] ?? 0,
      json['display_name'],
    );
  }

  Package copyWith({double? price, int? quantity}) {
    return Package(
      this.id,
      this.name,
      this.description,
      price ?? this.price,
      this.cuisineID,
      this.mealId,
      this.group,
      quantity ?? this.quantityAvailable,
      this.date,
      this.complimentary,
      this.addOn,
      this.groupID,
      this.displayName,
    );
  }
}
