import 'package:preetibhojan/util/datetime.dart';

class Menu {
  final String sabji;
  final String? daal, rice, sweet;
  final int mealID, cuisineID;
  final DateTime date;

  Menu(
    this.mealID,
    this.cuisineID,
    this.sabji,
    this.daal,
    this.rice,
    this.date,
    this.sweet,
  );

  factory Menu.fromJson(Map<String, dynamic> json) {
    return Menu(
      json['meal_id'],
      json['cuisine_id'],
      json['sabji'],
      json['daal'],
      json['rice'],
      setTimeToZero(DateTime.parse(json['date'])),
      json['sweet'],
    );
  }
}
