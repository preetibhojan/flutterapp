import 'dart:convert';
import 'dart:core';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Model.dart';

abstract class MenuRepository {
  Future<List<Menu>> get menu;
  void invalidateCache();
}

class StubMenuRepository implements MenuRepository {
  final List<Menu> input;

  StubMenuRepository({required this.input});

  @override
  Future<List<Menu>> get menu => Future.value(input);

  @override
  void invalidateCache() {
  }
}

class NetworkMenuRepository implements MenuRepository {
  static final _log = Logger('NetworkMenuRepository');

  var _cache = <Menu>[];

  @override
  Future<List<Menu>> get menu async {
    _log.info('Getting menu');
    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Get user');
    final user = await getUser();

    _log.info('Sending post request for menu');
    final response = await http.post(
      Uri.parse(base_url + '/menu'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('statusCode: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map<Menu>((json) => Menu.fromJson(json))
          .toList();

      return _cache;
    }

    _log.severe('Could not load menu');
    throw Exception('Could not load menu');
  }

  @override
  void invalidateCache() {
    _cache = [];
  }
}

class AdminMenuRepository implements MenuRepository {
  List<Menu>? _cache;

  static final _log = Logger('Admin Menu Repository');

  @override
  Future<List<Menu>> get menu async {
    _log.info('get admin menu');
    if (_cache != null) {
      _log.info('returning cache');
      return _cache!;
    }

    final response = await http.get(Uri.parse(base_url + '/admin/menu'));

    _log.info('statusCode: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not load menu');
      throw Exception('Could not load menu');
    }

    _cache = (jsonDecode(response.body) as List)
        .map<Menu>((json) => Menu.fromJson(json))
        .toList();

    return _cache!;
  }

  @override
  void invalidateCache() {
    _cache = null;
  }
}
