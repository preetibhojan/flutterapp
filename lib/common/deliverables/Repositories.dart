import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/util/datetime.dart';

import '../Config.dart';
import '../Storage.dart';
import 'Model.dart';

abstract class DeliverableRepository {
  Future<List<Deliverable>> get deliverablesFromToday;

  Future<List<Deliverable>> get allDeliverables;

  Future<List<Deliverable>> get deliverablesFromTomorrow;

  Future<List<Deliverable>> get cancelledDeliverablesFromTomorrow;

  Future<void> cancel(Package package, int quantity, DateTime date);
}

class StubDeliverableRepository implements DeliverableRepository {
  final List<Deliverable> fromToday, all, fromTomorrow, cancelledFromTomorrow;

  StubDeliverableRepository(
      this.fromToday, this.all, this.fromTomorrow, this.cancelledFromTomorrow);

  @override
  Future<List<Deliverable>> get deliverablesFromToday =>
      Future.value(fromToday);

  @override
  Future<List<Deliverable>> get allDeliverables => Future.value(all);

  @override
  Future<List<Deliverable>> get deliverablesFromTomorrow =>
      Future.value(fromTomorrow);

  @override
  Future<List<Deliverable>> get cancelledDeliverablesFromTomorrow =>
      Future.value(cancelledFromTomorrow);

  @override
  Future<void> cancel(Package package, int quantity, DateTime date) async {}
}

class NetworkDeliverableRepository implements DeliverableRepository {
  List<Deliverable>? _deliverablesFromTodayCache,
      _allDeliverablesCache,
      _cancelledFromTomorrowCache;

  static final _log = Logger('Network deliverable repository');

  @override
  Future<List<Deliverable>> get deliverablesFromToday async {
    _log.info('Getting deliverables for today');
    if (_deliverablesFromTodayCache != null) {
      _log.info('Returning cache');
      return _deliverablesFromTodayCache!;
    }

    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/deliverables_from_today'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while fetching deliverables for today');
      throw Exception(
          'Something went wrong while fetching deliverables for today');
    }

    _deliverablesFromTodayCache = (jsonDecode(response.body) as List)
        .map((json) => Deliverable.fromJson(json))
        .toList();

    return _deliverablesFromTodayCache!;
  }

  @override
  Future<List<Deliverable>> get allDeliverables async {
    _log.info('Getting all deliverables');
    if (_allDeliverablesCache != null) {
      _log.info('Returning cache');
      return _allDeliverablesCache!;
    }

    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/deliverables'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while fetching all deliverables');
      throw Exception('Something went wrong while fetching all deliverables');
    }

    _allDeliverablesCache = (jsonDecode(response.body) as List)
        .map((json) => Deliverable.fromJson(json))
        .toList();

    return _allDeliverablesCache!;
  }

  @override
  Future<List<Deliverable>> get deliverablesFromTomorrow async {
    final currentDate = setTimeToZero(DateTime.now());

    final temp = (await deliverablesFromToday).where((deliverable) {
      /// != cannot be used because if deliverables from today is
      /// fetched at Sep 14 11:59:59, but deliverables from tomorrow
      /// is called at sep 15 00:00:00, deliverables on sep 14 will
      /// not be equal to sep 15 and hence will be returned but that's
      /// not what we want cause it does not fit the definition of
      /// deliverables from tomorrow

      return currentDate.isBefore(deliverable.date) ||
          !currentDate.isAtSameMomentAs(deliverable.date);
    }).toList();

    return temp;
  }

  Future<List<Deliverable>> get cancelledDeliverablesFromTomorrow async {
    _log.info('Getting cancelled deliverables from tomorrow');
    if (_cancelledFromTomorrowCache != null) {
      _log.info('Returning cache');
      return _cancelledFromTomorrowCache!;
    }

    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/cancelled_deliverables_from_tomorrow'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info(
        'Something went wrong while fetching cancelled deliverables '
        'from tomorrow',
      );

      throw Exception(
        'Something went wrong while fetching cancelled deliverables '
        'from tomorrow',
      );
    }

    _cancelledFromTomorrowCache = (jsonDecode(response.body) as List)
        .map((json) => Deliverable.fromJson(json))
        .toList();

    return _cancelledFromTomorrowCache!;
  }

  @override
  Future<void> cancel(Package package, int quantity, DateTime date) async {
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/cancel2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'package_id': package.id,
        'quantity': quantity,
        'date': DateFormat('yyyy-MM-dd').format(date),
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while cancelling');

      throw Exception('Something went wrong while cancelling');
    }
  }
}
