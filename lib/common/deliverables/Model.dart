import 'package:preetibhojan/common/delivery_location/Models.dart';

class DeliverableStatus {
  final int value;

  const DeliverableStatus._(this.value);

  static const Ordered = DeliverableStatus._(0);
  static const Delivered = DeliverableStatus._(1);
  static const Cancelled = DeliverableStatus._(2);

  factory DeliverableStatus.fromInt(int value) {
    switch (value) {
      case 0:
        return Ordered;
      case 1:
        return Delivered;
      case 2:
        return Cancelled;
      default:
        throw StateError(
            'Value could not be converted into Deliverable Status');
    }
  }
}

class Deliverable {
  final int packageID;
  int quantity;
  final double price;
  final DeliveryLocation deliveryLocation;
  final DateTime date;
  final DeliverableStatus status;
  final int mealID;
  final bool addOn;

  Deliverable(
    this.packageID,
    this.price,
    this.quantity,
    this.deliveryLocation,
    this.date,
    this.status,
    this.mealID,
    this.addOn,
  );

  factory Deliverable.fromJson(Map<String, dynamic> json) {
    return Deliverable(
      json['package_id'],
      double.parse(json['price']),
      json['quantity'],
      DeliveryLocation.fromInt(json['delivery_location'])!,
      DateTime.parse(json['date']),
      DeliverableStatus.fromInt(json['status']),
      json['meal_id'],
      json['add_on'],
    );
  }
}
