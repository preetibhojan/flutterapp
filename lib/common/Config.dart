import 'dart:convert';
import 'dart:ui';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';

const domain_name = 'https://preetibhojan.co.in';
// const domain_name = 'http://10.0.2.2:5000';
// const domain_name = 'http://10.0.3.2:5000';
// const domain_name = 'http://localhost:5000';
const prefix = domain_name + '/api';
const base_url = '$domain_name/api/v1';

const rupeeSymbol = '\u20B9';

const version = 62;

const secondaryColor = Color.fromRGBO(39, 43, 75, 1);
const primaryColor = const Color.fromRGBO(0, 122, 164, 1);

class Config {
  final Map<DeliveryLocation, double> gst;
  final Map<int, double> needBasedCustomerDelta;
  final int version;

  Config(this.gst, this.needBasedCustomerDelta, this.version);

  factory Config.fromJson(Map<String, dynamic> json) {
    final delta = <int, double>{};
    for (final entry in json['need_based_customer_delta'].entries) {
      delta[int.parse(entry.key)] = entry.value;
    }

    final gst = <DeliveryLocation, double>{};
    for (final entry in json['gst'].entries) {
      gst[DeliveryLocation.fromInt(int.parse(entry.key))!] = entry.value;
    }

    return Config(
      gst,
      delta,
      json['version'],
    );
  }
}

abstract class ConfigRepository {
  Future<Config> get config;
}

class StubConfigRepository implements ConfigRepository {
  final Config input;

  StubConfigRepository(this.input);

  @override
  Future<Config> get config => Future.value(input);
}

class NetworkConfigRepository implements ConfigRepository {
  static final _log = Logger('Network Config Repository');

  @override
  Future<Config> get config async {
    _log.info('getting config from internet');

    final user = await getUser();

    final response = await http.get(
      Uri.parse(
          base_url + '/front_end_config?phone_number=${user.phoneNumber}'),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('Could not fetch config');
    }

    return Config.fromJson(jsonDecode(response.body));
  }
}

final Map<int, Map<int, double>> adminConfig = {};

void setAdminConfigFromJson(List<dynamic> json) {
  adminConfig.clear();
  for (final map in json) {
    if (adminConfig[map['meal_id']] == null) {
      adminConfig[map['meal_id']] = {};
    }

    adminConfig[map['meal_id']]![map['cuisine_id']] = map['need_based_customer_delta'];
  }
}

late Config config;
