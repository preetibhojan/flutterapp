import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/user_home/Navigators.dart';

class _Notification {
  String? token;
  String? apnsToken;

  static const _channel = MethodChannel('custom notification channel');
  static final _log = Logger('Notification');

  Future<void> configureFirebase() async {
    final _messaging = FirebaseMessaging.instance;

    await _messaging.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    final _notificationSettings = await _messaging.requestPermission(
      provisional: true,
    );

    if (_notificationSettings.authorizationStatus !=
        AuthorizationStatus.authorized) {
      print('Not authorized');
      return;
    }

    token = await _messaging.getToken();
    print('token: $token');

    apnsToken = await _messaging.getAPNSToken();

    FirebaseMessaging.onMessage.listen((message) {
      ScaffoldMessenger.of(navigatorKey.currentContext!).showSnackBar(
        SnackBar(
          backgroundColor: secondaryColor,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                message.notification?.title ?? '',
                style: TextStyle(color: Colors.white),
              ),
              Text(
                message.notification!.body!,
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      );
    });

    /// Firebase automatically displays a notification on ios
    if (Platform.isAndroid) {
      FirebaseMessaging.onBackgroundMessage(_displayNotification);
    }
  }

  static Future<void> _displayNotification(
      RemoteMessage message) async {
    _log.info(notification);

    final result = await _channel.invokeMethod(
      'displayNotification',
      <String, dynamic>{
        'title': message.notification?.title,
        'body': message.notification?.body,
        'highPriority': true,
      },
    );

    _log.info(result);
  }
}

final notification = _Notification();

bool _isInitialized = false;

/// Called by authenticate during startup. All other parts
/// of the app can assume notification is initialized
Future<void> initNotification() async {
  if (_isInitialized) return;

  await Firebase.initializeApp();
  await notification.configureFirebase();
  _isInitialized = true;
}
