import 'package:collection/collection.dart';
import 'package:preetibhojan/common/schedule/Models.dart';
import 'package:preetibhojan/util/datetime.dart';

class Meal implements Comparable {
  final int id, priority, deposit;
  final List<Day>? cannotOrderOn;
  final String name;
  final String? imageUrl;
  final Duration orderBefore;
  final Duration deliveryTime;
  final bool active;

  Meal(
    this.id,
    this.priority,
    this.cannotOrderOn,
    this.name,
    this.orderBefore,
    this.deliveryTime,
    this.active,
    this.imageUrl,
    this.deposit,
  );

  /// Returns if meal can be ordered `now`.
  /// Meal can be ordered right now if
  /// now < (date + delivery time - orderBefore)
  bool canOrder(DateTime now) {
    return now.isBefore(orderBeforeWRT(now)) &&
        !cannotOrderOn!.contains(Day.fromInt(now.weekday)) &&
        active;
  }

  /// Returns order before with respect to now
  DateTime orderBeforeWRT(DateTime now) {
    return setTimeToZero(now).add(deliveryTime).subtract(orderBefore);
  }

  bool isNotDelivered(DateTime now) {
    return now.isBefore(setTimeToZero(now).add(deliveryTime));
  }

  factory Meal.fromJson(Map<String, dynamic> json) {
    return Meal(
      json['id'],
      json['priority'],
      (json['cannot_order_on'] as List).map((day) => Day.fromInt(day)).toList(),
      json['name'],
      Duration(seconds: json['order_before']),
      Duration(seconds: json['delivery_time']),
      json['active'],
      json['image_url'],
      json['deposit'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Meal &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          priority == other.priority &&
          name == other.name &&
          imageUrl == other.imageUrl &&
          orderBefore == other.orderBefore &&
          deliveryTime == other.deliveryTime &&
          ListEquality().equals(cannotOrderOn, other.cannotOrderOn);

  @override
  int get hashCode {
    late int listHashCode;
    if (cannotOrderOn == null || cannotOrderOn!.length == 0) {
      /// 54321 is a random number
      listHashCode = 54321;
    } else {
      /// handle the case where cannotOrderOn is empty or else map will throw
      listHashCode =
          cannotOrderOn!.map((d) => d.hashCode).reduce((a, b) => a ^ b);
    }

    return id.hashCode ^
        priority.hashCode ^
        listHashCode ^
        name.hashCode ^
        imageUrl.hashCode ^
        orderBefore.hashCode ^
        deliveryTime.hashCode;
  }

  /// Used for ordering in cart
  @override
  int compareTo(other) {
    return this.priority.compareTo(other.priority);
  }

  @override
  String toString() {
    return 'Meal{id: $id, priority: $priority, deposit: $deposit, cannotOrderOn: $cannotOrderOn, name: $name, imageUrl: $imageUrl, orderBefore: $orderBefore, deliveryTime: $deliveryTime, active: $active}';
  }

  bool enableSchedule(DateTime today) {
    if (!active) {
      return false;
    }

    if (today.weekday == DateTime.sunday) {
      return true;
    }

    final days = List.of(Day.values);
    days.removeWhere(
        (day) => day.weekday <= today.weekday || cannotOrderOn!.contains(day));
    return days.length >= 3;
  }
}
