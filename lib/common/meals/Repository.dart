

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import '../Config.dart';
import '../Storage.dart';
import 'Model.dart';

abstract class MealsRepository {
  Future<List<Meal>> get meals;
}

class StubMealsRepository implements MealsRepository {
  final List<Meal> input1, input2;

  StubMealsRepository(this.input1, this.input2);

  Future<List<Meal>> get meals => Future.value(input1);
}

class NetworkMealsRepository implements MealsRepository {
  static final _log = Logger('Network meals repository');

  List<Meal>? _cache;

  @override
  Future<List<Meal>> get meals async {
    _log.info('Get meals');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    _log.info('Getting user');
    final user = await getUser();

    _log.info('Sending post request for meals');

    final response = await http.post(
      Uri.parse(base_url + '/meals'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch meals');
      throw Exception('Could not fetch meals');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => Meal.fromJson(json))
        .toList();

    return _cache!;
  }
}

class NetworkAllMealsRepository implements MealsRepository {
  static final _log = Logger('Network all meals repository');

  List<Meal>? _cache;

  @override
  Future<List<Meal>> get meals async {
    _log.info('Get meals');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    _log.info('Sending post request for meals');

    final response = await http.get(Uri.parse(base_url + '/all_meals'));

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch all meals');
      throw Exception('Could not fetch all meals');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => Meal.fromJson(json))
        .toList();

    return _cache!;
  }
}

class NetworkAllMealsWithImageURL implements MealsRepository {
  static final _log = Logger('Network all meals with image url');

  List<Meal>? _cache;

  @override
  Future<List<Meal>> get meals async {
    _log.info('Get meals');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    _log.info('Getting user');
    final user = await getUser();

    _log.info('Sending post request for meals');

    final response = await http.post(
      Uri.parse(base_url + '/meals_for_menu'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch meals');
      throw Exception('Could not fetch meals');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => Meal.fromJson(json))
        .toList();

    return _cache!;
  }
}
