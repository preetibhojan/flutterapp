import 'package:preetibhojan/util/datetime.dart';
import 'package:validators/validators.dart';

import 'meals/Model.dart';

DateTime dateToUse(List<Meal> meals, DateTime now) {
  final mealsThatCanBeOrderedNow =
      meals.where((meal) => meal.canOrder(now)).length;

  if (mealsThatCanBeOrderedNow == 0) {
    /// if today's time is less than 8pm, use today.
    /// Otherwise if dinner is disabled, at 11am, it will
    /// book lunch for tomorrow.
    ///
    /// (This really happened)
    if (now.hour < 20) {
      return now;
    }

    return now.nextWorkingDay;
  }

  return now;
}

String? isPasswordValid(String str) {
  return (str.length < 8 && str.length > 70)
      ? 'Password should be at least at least 8 characters and less than 70'
      : null;
}

String? isPhoneNumberValid(String str) {
  final _int = int.tryParse(str);

  return str.length != 10 || _int == null || _int < 0
      ? 'Phone number should be 10 digits'
      : null;
}

String? isEmailValid(String str) =>
    !isEmail(str) ? 'Please enter a valid email' : null;


const UnauthorizedAccessError = 3;


Iterable<T> sort<T>(Iterable<T> iterable, [Comparator? comparator]) {
  if (comparator == null) {
    comparator = (a, b) => a.compareTo(b);
  }

  final tempList = iterable.toList();
  tempList.sort(comparator);
  return tempList.toList();
}
