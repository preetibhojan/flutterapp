import 'dart:math';

import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/user_home/Cart/Models.dart';

class QuantityManager {
  final Cart cart;
  final Meal meal;

  List<CartItem> get _cartItems => cart.items[meal] ?? [];

  QuantityManager(this.cart, this.meal);

  void increaseQuantity(CartItem cartItem) {
    final quantity = quantityAvailable(cartItem.package, cartItem.quantity);

    if (cartItem.quantity < quantity) {
      cart.set(meal, cartItem.package, cartItem.quantity + 1);
    }
  }

  int quantityAvailable(Package package, int currentQuantity) {
    if (package.addOn) {
      final normalPackages = _cartItems.where((c) => !c.package.addOn);

      final normalPackageCount = normalPackages.length > 0
          ? normalPackages.map((cart) => cart.quantity).reduce((a, b) => a + b)
          : 0;

      return min(package.quantityAvailable!, normalPackageCount);
    }

    return package.quantityAvailable!;
  }

  void decreaseQuantity(CartItem cartItem) {
    if (cartItem.quantity > 1) {
      cart.set(meal, cartItem.package, cartItem.quantity - 1);
    } else {
      cart.delete(meal, cartItem.package);
    }
    updateQuantity();
  }

  void updateQuantity() {
    final packagesToDelete = <Package>[];
    for (final item in _cartItems) {
      final quantity = min(
        item.quantity,
        quantityAvailable(item.package, item.quantity),
      );

      if (quantity == 0) {
        packagesToDelete.add(item.package);
      } else if (quantity != item.quantity) {
        cart.set(meal, item.package, quantity);
      }
    }

    for (final package in packagesToDelete) {
      cart.delete(meal, package);
    }

    cart.items.removeWhere((key, value) => value.length == 0);
  }
}
