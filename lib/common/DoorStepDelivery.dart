import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:preetibhojan/common/Config.dart';
typedef Future<int> DoorStepDelivery();

Future<int> doorStepDelivery() async {
  final response = await http.get(Uri.parse(base_url + '/door_step_delivery'));

  if (response.statusCode != 200) {
    throw StateError('Could not fetch door step delivery: ${response.body}');
  }

  return jsonDecode(response.body)['door_step_delivery'];
}
