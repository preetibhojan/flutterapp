class Cuisine {
  final int id;
  final String name;
  final bool showHomeAddress, showOfficeAddress;

  Cuisine(this.id, this.name, this.showHomeAddress, this.showOfficeAddress);

  @override
  String toString() {
    return 'ID: $id Name: $name';
  }

  factory Cuisine.fromJson(Map<String, dynamic> json) {
    return Cuisine(
      json['id'],
      json['name'],
      json['show_home_address'],
      json['show_office_address'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Cuisine && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
