import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserType {
  final int id;

  const UserType._(this.id);

  static const UserType Customer = UserType._(0);
  static const UserType Staff = UserType._(1);

  factory UserType.fromInt(int value) {
    switch (value) {
      case 0:
        return Customer;
      case 1:
        return Staff;

      default:
        throw StateError('Value could not be converted to user type');
    }
  }

  factory UserType.fromString(String value) {
    switch (value) {
      case 'customer':
        return Customer;
      case 'staff':
        return Staff;

      default:
        throw StateError('Value could not be converted to user type');
    }
  }
}

class User {
  final String phoneNumber, password, name;
  final UserType type;
  bool isRegularCustomer;

  User(this.phoneNumber, this.password, this.name, this.type,
      [this.isRegularCustomer = false]);

  @override
  String toString() {
    return '$phoneNumber $password $isRegularCustomer';
  }
}

final _storage = FlutterSecureStorage();

/// Assumes user exists
Future<User> getUser() async {
  return User(
    (await _storage.read(key: 'phone_number'))!,
    (await _storage.read(key: 'password'))!,
    (await _storage.read(key: 'name'))!,
    UserType.fromInt(int.tryParse((await _storage.read(key: 'type') ?? '0'))!),
    await _storage.read(key: 'is_regular_customer') == 'true',
  );
}

Future<User?> getNullableUser() async {
  /// Only in startup, I am checking if user is authenticated.
  /// The rest of the app assumes user is authenticated.
  /// If phone number exists, I assume user exists. Otherwise
  /// I assume user does not.
  final phoneNumber = await _storage.read(key: 'phone_number');

  if (phoneNumber == null) {
    return null;
  }

  return User(
    phoneNumber,
    (await _storage.read(key: 'password'))!,
    (await _storage.read(key: 'name'))!,
    UserType.fromInt(int.tryParse((await _storage.read(key: 'type') ?? '0'))!),
    await _storage.read(key: 'is_regular_customer') == 'true',
  );
}

Future deleteUser() async {
  final storage = FlutterSecureStorage();

  await storage.delete(key: 'phone_number');
  await storage.delete(key: 'password');
  await storage.delete(key: 'name');
  await storage.delete(key: 'is_regular_customer');
  await storage.delete(key: 'type');
}

class StaffPermissions {
  final bool home,
      addQuantity,
      setMenu,
      notifications,
      notificationsNotSent,
      delivery,
      convert,
      addOrder,
      cancelOrder,
      updatePaymentDetail;

  StaffPermissions(
    this.home,
    this.addQuantity,
    this.setMenu,
    this.notifications,
    this.notificationsNotSent,
    this.delivery,
    this.convert,
    this.addOrder,
    this.cancelOrder,
    this.updatePaymentDetail,
  );

  factory StaffPermissions.fromJson(Map<String, dynamic> json) {
    return StaffPermissions(
      json['home'],
      json['add_quantity'],
      json['set_menu'],
      json['notifications'],
      json['notifications_not_sent'],
      json['delivery'],
      json['convert'],
      json['add_order'],
      json['cancel_order'],
      json['update_payment_detail'],
    );
  }
}

late StaffPermissions staffPermissions;
