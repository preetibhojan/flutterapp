import 'dart:convert';
import 'dart:core';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Models.dart';

abstract class AreaRepository {
  Future<List<Area>> get area;

  Future<List<Locality>> get allLocalities;

  Future<List<Tower>> get towers;
}

class NetworkAreaRepository implements AreaRepository {
  static final _log = Logger('Network area repository');

  List<Area> _areaCache = <Area>[];
  List<Locality> _localityCache = <Locality>[];
  List<Tower> _towerCache = <Tower>[];

  late Future _future;

  NetworkAreaRepository() {
    _future = _initializeCache();
  }

  Future _initializeCache() async {
    final response = await http.get(Uri.parse(base_url + '/delivery_locations'))
        /*.timeout(Duration(seconds: 5))*/;

    _log.info('status code: ${response.statusCode}');
    _log.info('response ${response.body}');

    if (response.statusCode == 200) {
      final temp = json.decode(response.body);
      _areaCache =
          temp['areas'].map<Area>((json) => Area.fromJson(json)).toList();
      _localityCache = temp['localities']
          .map<Locality>((json) => Locality.fromJson(json))
          .toList();
      _towerCache =
          temp['towers'].map<Tower>((json) => Tower.fromJson(json)).toList();
    }
  }

  @override
  Future<List<Area>> get area async {
    await _future;

    return _areaCache;
  }

  @override
  Future<List<Locality>> get allLocalities async {
    await _future;

    return _localityCache;
  }

  @override
  Future<List<Tower>> get towers async {
    await _future;
    return _towerCache;
  }
}

abstract class DeliveryLocationsRepository {
  Future<List<DeliveryLocation>> get deliveryLocations;
}

class StubDeliveryLocationsRepository implements DeliveryLocationsRepository {
  final List<DeliveryLocation> input;

  StubDeliveryLocationsRepository(this.input);

  @override
  Future<List<DeliveryLocation>> get deliveryLocations => Future.value(input);
}

class NetworkDeliveryLocationsRepository
    implements DeliveryLocationsRepository {
  static final _log = Logger('Network delivery locations repository');

  List<DeliveryLocation>? _cache;

  @override
  Future<List<DeliveryLocation>> get deliveryLocations async {
    _log.info('Get delivery locations');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    _log.info('Get user');
    final user = await getUser();

    var response = await http.post(
      Uri.parse(base_url + '/home_address'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('could not fetch home address');
    }

    _cache = [];

    if (!_cache!.contains(DeliveryLocation.Home) &&
        !(jsonDecode(response.body) as Map).containsKey('enabled')) {
      _cache!.add(DeliveryLocation.Home);
    }

    response = await http.post(
      Uri.parse(base_url + '/office_address'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('could not fetch office address');
    }

    if (!_cache!.contains(DeliveryLocation.Office) &&
        !(jsonDecode(response.body) as Map).containsKey('enabled')) {
      _cache!.add(DeliveryLocation.Office);
    }

    if (!_cache!.contains(DeliveryLocation.NotRequired))
      _cache!.add(DeliveryLocation.NotRequired);

    return _cache!;
  }
}

typedef DeliveryCharge = Map<DeliveryLocation, int>;
typedef MealID = int;

abstract class DeliveryCharges {
  Future<Map<MealID, DeliveryCharge>> get deliveryCharges;

  void invalidateCache();
}

class NetworkDeliveryCharges implements DeliveryCharges {
  Map<MealID, DeliveryCharge>? _cache;
  static final _log = Logger('Network Delivery Charges');

  @override
  Future<Map<MealID, DeliveryCharge>> get deliveryCharges async {
    _log.info('Getting delivery charges');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    _log.info('Getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/delivery_charge_v2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Something went wrong');
      throw Error();
    }

    final json = jsonDecode(response.body);

    _cache = {};

    if (json['home'] != null) {
      for (final entry in (json['home'] as Map<String, dynamic>).entries) {
        final mealID = int.parse(entry.key);
        final charge = entry.value as int;

        if (_cache!.containsKey(mealID)) {
          _cache![mealID]![DeliveryLocation.Home] = charge;
        } else {
          _cache![mealID] = {DeliveryLocation.Home: charge};
        }
      }
    }

    if (json['office'] != null) {
      for (final entry in (json['office'] as Map<String, dynamic>).entries) {
        final mealID = int.parse(entry.key);
        final charge = entry.value as int;

        if (_cache!.containsKey(mealID)) {
          _cache![mealID]![DeliveryLocation.Office] = charge;
        } else {
          _cache![mealID] = {DeliveryLocation.Office: charge};
        }
      }
    }

    return _cache!;
  }

  @override
  void invalidateCache() {
    _cache = null;
  }
}
