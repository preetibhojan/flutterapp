class Area {
  final int id;
  final String name;

  Area(this.id, this.name);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Area && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  factory Area.fromJson(Map<String, dynamic> json) {
    return Area(
      json['id'],
      json['area'],
    );
  }
}

class Locality {
  final int id, areaID;
  final String name;
  final bool home, office;

  Locality(this.id, this.name, this.areaID, this.home, this.office);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Locality && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  factory Locality.fromJson(Map<String, dynamic> json) {
    return Locality(
      json['id'],
      json['locality'],
      json['area_id'],
      json['home'],
      json['office'],
    );
  }

  @override
  String toString() {
    return 'Locality{id: $id, areaID: $areaID, name: $name, home: $home, office: $office}';
  }
}

class Tower {
  final int id, localityID;
  final String name;
  final bool home;

  Tower(this.id, this.localityID, this.name, this.home);


  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Tower &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              localityID == other.localityID &&
              name == other.name &&
              home == other.home;

  @override
  int get hashCode =>
      id.hashCode ^ localityID.hashCode ^ name.hashCode ^ home.hashCode;

  factory Tower.fromJson(Map<String, dynamic> json) {
    return Tower(
      json['id'],
      json['locality_id'],
      json['name'],
      json['home'],
    );
  }
}

/// Delivery Location is Kind of like an enum except I wanted to be able to
/// parse and serialize it. Hence a custom class
class DeliveryLocation {
  static const Home = DeliveryLocation._('Home');
  static const Office = DeliveryLocation._('Office');
  static const NotRequired = DeliveryLocation._('Not required');

  static const values = [Home, Office, NotRequired];
  static const valuesWithoutNotRequired = [Home, Office];

  final String name;

  const DeliveryLocation._(this.name);

  static DeliveryLocation? fromInt(int? value) {
    if (value == 0) {
      return DeliveryLocation.Home;
    } else if (value == 1) {
      return DeliveryLocation.Office;
    } else {
      return null;
    }
  }

  int toInt() {
    return this == DeliveryLocation.Home ? 0 : 1;
  }

  int toJson() {
    return toInt();
  }

  @override
  String toString() {
    return 'DeliveryLocation{name: $name}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DeliveryLocation &&
          runtimeType == other.runtimeType &&
          name == other.name;

  @override
  int get hashCode => name.hashCode;
}
