import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/meals/Repository.dart';

import '../Config.dart';
import '../Storage.dart';
import 'Models.dart';

class CouldNotFetchSchedule implements Exception {}

abstract class ScheduleRepository {
  Future<Schedule> get schedule;
}

class StubScheduleRepository implements ScheduleRepository {
  final Schedule input1;

  StubScheduleRepository(this.input1);

  @override
  Future<Schedule> get schedule => Future.value(input1);
}

class NetworkScheduleRepository implements ScheduleRepository {
  static final _log = Logger('Network Payment Info Repository');
  static final mealsRepository = NetworkMealsRepository();

  final String url;

  NetworkScheduleRepository([this.url = '/payment_info']);

  @override
  Future<Schedule> get schedule async {
    _log.info('Getting schedule');

    _log.info('Getting user');
    final user = await getUser();

    final meals = await mealsRepository.meals;

    final response = await http.post(
      Uri.parse(base_url + url),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch payment info');
      throw CouldNotFetchSchedule();
    }

    final scheduleJson = jsonDecode(response.body) as Map<String, dynamic>;

    return Schedule.fromJson(scheduleJson, meals);
  }
}
