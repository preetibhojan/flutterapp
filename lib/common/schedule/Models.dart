import 'package:collection/collection.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';

class Schedule {
  final double depositPaid;

  final double creditAvailable;

  /// Some time you may need the packages with respect to meals
  /// while other times you might only need packages. I chose
  /// this representation because it satisfies both needs.
  final Map<Meal, List<PackageInfo>> packages;

  final Map<Meal, List<PackageInfo>> addOns;

  Schedule(this.depositPaid, this.creditAvailable, this.packages, this.addOns);

  factory Schedule.fromJson(Map<String, dynamic> json, List<Meal> meals) {
    final creditAvailable = double.parse(json['temp_credit']);
    final depositPaid = double.parse(json['deposit']);

    final packageInfo = <Meal, List<PackageInfo>>{};
    json['package_info'].forEach((package) {
      final meal = meals.firstWhereOrNull((m) => m.id == package['meal_id']);

      /// There are situations where a meal is disabled after it has been
      /// scheduled. In this case, the current active meals dont return the
      /// disabled meal.
      ///
      /// If someone has such a meal in their schedule, skip it
      if (meal == null) {
        return;
      }

      final pi = PackageInfo.fromJson(package);

      if (packageInfo[meal] == null) {
        packageInfo[meal] = [pi];
      } else {
        packageInfo[meal]!.add(pi);
      }
    });

    final addOnInfo = <Meal, List<PackageInfo>>{};

    if (json['add_on_info'] != null) {
      json['add_on_info'].forEach((package) {
        final meal = meals.firstWhere((m) => m.id == package['meal_id']);
        final pi = PackageInfo.fromJson(package);

        if (addOnInfo[meal] == null) {
          addOnInfo[meal] = [pi];
        } else {
          addOnInfo[meal]!.add(pi);
        }
      });
    }

    return Schedule(
      depositPaid,
      creditAvailable,
      packageInfo,
      addOnInfo,
    );
  }

  List<dynamic> toJson() {
    /// back end does not care about deposit or credit available.
    /// It knows those values
    return packages.values.toList();
  }
}

class Day {
  final int weekday;
  final String name;

  const Day._(this.weekday, this.name);

  static const Monday = Day._(1, 'Monday');
  static const Tuesday = Day._(2, 'Tuesday');
  static const Wednesday = Day._(3, 'Wednesday');
  static const Thursday = Day._(4, 'Thursday');
  static const Friday = Day._(5, 'Friday');
  static const Saturday = Day._(6, 'Saturday');
  static const Sunday = Day._(7, 'Sunday');

  static const values = [
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
  ];

  factory Day.fromInt(int weekday) {
    return Day.values.firstWhere((element) => element.weekday == weekday);
  }

  Day clone() {
    return Day._(this.weekday, this.name);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Day &&
          runtimeType == other.runtimeType &&
          weekday == other.weekday &&
          name == other.name;

  @override
  int get hashCode => weekday.hashCode ^ name.hashCode;
}

class DeliveryInfo {
  final DeliveryLocation deliveryLocation;
  final int deliveryCharge;

  DeliveryInfo(this.deliveryLocation, this.deliveryCharge);

  factory DeliveryInfo.fromJson(Map<String, dynamic> json) {
    return DeliveryInfo(
      DeliveryLocation.fromInt(json['delivery_location'])!,
      json['delivery_charge'],
    );
  }
}

class PackageInfo {
  final int price, packageID, mealID, quantity;
  final Map<Day, DeliveryInfo> deliveryInfo;

  PackageInfo(this.price, this.deliveryInfo, this.mealID, this.packageID, this.quantity);

  factory PackageInfo.fromJson(Map<String, dynamic> json) {
    final deliveryInfo = <Day, DeliveryInfo>{};

    for (final item in json['delivery_info'] as List) {
      deliveryInfo[Day.fromInt(item['day'])] = DeliveryInfo.fromJson(item);
    }

    return PackageInfo(
      json['price'],
      deliveryInfo,
      json['meal_id'],
      json['package_id'],
      json['quantity'],
    );
  }

  Map<String, dynamic> toJson() {
    final _deliveryInfoJson = [];
    deliveryInfo.forEach((key, value) {
      _deliveryInfoJson.add({
        'day': key.weekday,
        'delivery_location': value.deliveryLocation.toInt(),
      });
    });

    return {
      'package_id': packageID,
      'delivery_info': _deliveryInfoJson,
    };
  }
}
