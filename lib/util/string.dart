

import 'package:validators/validators.dart';

bool isAlphaNumericUnderScoreOrSpace(String str) =>
    matches(str.trim(), r'^[\w\s]+$');

extension StringExtenstion on String {
  String get capitalize {
    if (this.length > 0)
      return '${this[0].toUpperCase()}${this.substring(1).toLowerCase()}';
    return this;
  }

  bool get isAlphaNumeric {
    if (this.length == 0) {
      return false;
    }

    return matches(this, r'^[a-zA-Z0-9]+$');
  }

  bool get isAlphabeticWithSpaces {
    if (this.length == 0) {
      return false;
    }

    return matches(this, r'^[a-zA-Z][a-zA-Z ]+$');
  }

  bool get isValidUPIid {
    if (this.length == 0) {
      return false;
    }

    return matches(this, r'^[a-zA-Z0-9\.\-]{2,256}@[a-zA-Z]{2,64}$');
  }
}
