DateTime setTimeToZero(DateTime d) {
return DateTime(d.year, d.month, d.day);
}

extension Date on DateTime {
  int get timeInSeconds {
    return this.hour * 3600 + this.minute * 60 + this.second;
  }

  DateTime get nextWorkingDay {
    return setTimeToZero(this).add(Duration(days: 1));
  }

  int get daysSinceEpoch {
    final millisecondsInOneDay = 86400000;
    return setTimeToZero(this).millisecondsSinceEpoch ~/ millisecondsInOneDay;
  }
}
