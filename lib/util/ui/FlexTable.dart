

import 'package:flutter/material.dart';

/// A widget that uses flex to make a table instead of figuring out
/// size of its cells which is:
///
/// 1. Great for performance
/// 2. Looks the same on all devices unlike DataTable
///
/// My motivation was DataTable did not look the same on all screens
class FlexTable extends StatefulWidget {
  /// A list of widget each being a heading.
  final List<Widget> headings;

  /// A list of list of widget where each widget is a cell and each
  /// list of widget is a row
  final List<List<Widget>> rows;

  /// A list of flex representing the flex for each column
  final List<int> flexFactors;

  /// Alignment for each column (headers not included)
  final List<Alignment> cellAlignments;

  /// Alignment for headers. If not specified or null,
  /// uses the alignment of cellAlignments
  final List<Alignment>? headerAlignments;

  /// FlexFit for the flex
  final FlexFit fit;

  /// Thickness of divider
  final double dividerThickness;

  FlexTable({
    required this.headings,
    required this.flexFactors,
    required this.rows,
    required this.cellAlignments,
    this.headerAlignments,
    this.fit = FlexFit.tight,
    this.dividerThickness = 1,
  }) {
    assert(
      headings.length != 0,
      "Headings cannot be empty or null",
    );

    assert(
      headings.length == flexFactors.length,
      "Length of flex factor should be same as heading",
    );

    assert(
      headings.length == cellAlignments.length,
      "Length of cell alignments should be same as heading",
    );

    for (final row in rows) {
      assert(
        headings.length == row.length,
        "Length of all rows should be same as heading",
      );
    }
  }

  @override
  _FlexTableState createState() => _FlexTableState();
}

class _FlexTableState extends State<FlexTable> {
  late List<Alignment> headerAlignments;

  @override
  void initState() {
    super.initState();
    headerAlignments = widget.headerAlignments ?? widget.cellAlignments;

    assert(
      widget.headings.length == headerAlignments.length,
      "Length of header alignments should be same as heading",
    );
  }

  @override
  Widget build(BuildContext context) {
    final headingsWithFlex = <Widget>[];
    final rowsWithFlex = List.generate(
      widget.rows.length,
      (_) => List<Widget>.filled(widget.headings.length, Container()),
    );

    for (int column = 0; column < widget.headings.length; column++) {
      headingsWithFlex.add(
        Flexible(
          fit: widget.fit,
          flex: widget.flexFactors[column],
          child: Align(
            alignment: headerAlignments[column],
            child: widget.headings[column],
          ),
        ),
      );

      for (int row = 0; row < widget.rows.length; row++) {
        rowsWithFlex[row][column] = Flexible(
          fit: widget.fit,
          flex: widget.flexFactors[column],
          child: Align(
            alignment: widget.cellAlignments[column],
            child: widget.rows[row][column],
          ),
        );
      }
    }

    return Column(
      children: [
        Row(children: headingsWithFlex),
        Divider(thickness: widget.dividerThickness),
        ...rowsWithFlex
            .map(
              (row) => Column(
                children: [
                  Row(children: row),
                  Divider(thickness: widget.dividerThickness),
                ],
              ),
            )
            .toList(),
      ],
    );
  }
}
