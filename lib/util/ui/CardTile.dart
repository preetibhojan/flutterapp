import 'package:flutter/material.dart';

class CardTile extends StatelessWidget {
  final Widget child;
  final Widget? title;
  final Color color;
  final EdgeInsets padding;

  const CardTile({
    required this.child,
    this.title,
    this.color = Colors.transparent,
    this.padding = const EdgeInsets.all(10),
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(5),
      child: Card(
        elevation: 0,
        color: color,
        child: Padding(
          padding: padding,
          child: Column(
            children: <Widget>[
              if (title != null)
                Center(child: title),
              child,
            ],
          ),
        ),
      ),
    );
  }
}
