import 'package:flutter/material.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/user_home/Navigators.dart';

class BottomNavBarItem {
  final String? route;
  final int index;
  final VoidCallback? onTap;

  const BottomNavBarItem(this.index, {this.route, this.onTap});

  static const Home = BottomNavBarItem(0, onTap: navigateToCustomerHome);
  static const Order = BottomNavBarItem(1, onTap: navigateToOrders);
  static const Profile = BottomNavBarItem(2, onTap: navigateToProfile);
  static const Wallet = BottomNavBarItem(3, onTap: navigateToWallet);
  static const Help = BottomNavBarItem(4, onTap: navigateToHelp);

  static const values = [Home, Order, Profile, Wallet, Help];

  static BottomNavBarItem fromIndex(int index) {
    return values.firstWhere((item) => item.index == index);
  }
}

class BottomNavBar extends StatelessWidget {
  final BottomNavBarItem selectedItem;

  const BottomNavBar(this.selectedItem);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: selectedItem.index,
      onTap: (value) => _onItemTapped(context, value),
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.calendar_today_outlined),
          label: 'Orders',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Profile',
        ),
        BottomNavigationBarItem(
          icon: _Rupee(
            selected: selectedItem.index == BottomNavBarItem.Wallet.index,
          ),
          label: 'Wallet',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.help),
          label: 'Help',
        ),
      ],
    );
  }

  void _onItemTapped(BuildContext context, int value) {
    if (value == BottomNavBarItem.Home.index) {
      Navigator.of(context).popUntil((_) => false);
      navigateToCustomerHome();
    }
    if (value == selectedItem.index) {
      return;
    }

    Navigator.of(context).popUntil((route) => route.isFirst);

    final newSelectedItem = BottomNavBarItem.fromIndex(value);
    if (newSelectedItem.onTap != null) {
      newSelectedItem.onTap!();
      return;
    }

    Navigator.of(context).pushNamed(newSelectedItem.route!);
  }
}

class _Rupee extends StatelessWidget {
  final bool selected;

  const _Rupee({required this.selected});

  @override
  Widget build(BuildContext context) {
    return Text(
      rupeeSymbol,
      style: TextStyle(
          fontSize: 20,
          color: selected
              ? Theme.of(context).bottomNavigationBarTheme.selectedItemColor
              : Theme.of(context).bottomNavigationBarTheme.unselectedItemColor),
    );
  }
}
