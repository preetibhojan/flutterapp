import 'package:flutter/material.dart';

class InvalidInputDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Invalid input'),
      content: Text(
          'Some fields have invalid input. Please go back and correct them before proceeding'),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text('Okay'),
        )
      ],
    );
  }
}
