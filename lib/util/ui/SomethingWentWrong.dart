import 'package:flutter/material.dart';

class SomethingWentWrong extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Something went wrong'),
      content: Text(
        'Please check your internet connection or update the app if '
        'update is available',
      ),
      actions: <Widget>[
        TextButton(
          child: Text('Okay'),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }
}
