import 'package:flutter/material.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewWrapper extends StatelessWidget {
  final String url;

  const WebViewWrapper(this.url);

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: WebViewWidget(
        controller: WebViewController()..loadRequest(Uri.parse(url)),
      ),
    );
  }
}
