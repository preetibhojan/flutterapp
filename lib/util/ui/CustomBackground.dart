import 'package:flutter/material.dart';

class CustomBackground extends StatelessWidget {
  final Widget child;
  final bool showCircularProgressIndicator;

  CustomBackground(
      {required this.child, this.showCircularProgressIndicator = false});

  @override
  Widget build(BuildContext context) {
    if (showCircularProgressIndicator) {
      return Stack(
        children: <Widget>[
          Positioned(
            bottom: 20,
            right: 20,
            child: Image.asset(
              'assets/background.png',
              height: 400,
            ),
          ),
          ...[
            child,
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
        ],
      );
    }

    return Stack(
      children: <Widget>[
        Positioned(
          bottom: 20,
          right: 20,
          child: Image.asset(
            'assets/background.png',
            height: 400,
          ),
        ),
        child,
      ],
    );
  }
}


class CustomForeground extends StatelessWidget {
  final Widget child;
  final bool showCircularProgressIndicator;

  CustomForeground(
      {required this.child, this.showCircularProgressIndicator = false});

  @override
  Widget build(BuildContext context) {
    if (showCircularProgressIndicator) {
      return Stack(
        children: <Widget>[
          ...[
            child,
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
          Positioned(
            bottom: 20,
            right: 20,
            child: Image.asset(
              'assets/background.png',
              height: 400,
            ),
          ),
        ],
      );
    }

    return Stack(
      children: <Widget>[
        child,
        Positioned(
          bottom: 20,
          right: 20,
          child: Image.asset(
            'assets/background.png',
            height: 400,
          ),
        ),
      ],
    );
  }
}
