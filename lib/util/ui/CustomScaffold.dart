import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

final customAppBar = AppBar(
  iconTheme: IconThemeData(color: Colors.white),
  title: Row(
    children: <Widget>[
      Container(
        margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
        ),
        child: Image.asset(
          'assets/app icon.png',
          height: 50,
          width: 50,
        ),
      ),
      Image.asset(
        'assets/app bar.png',
        height: 60,
        width: 180,
      ),
    ],
  ),
);

class _CustomDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 70,
      child: DrawerHeader(
        padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
        decoration: BoxDecoration(
          color: secondaryColor,
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                  child: Image.asset(
                    'assets/app icon.png',
                    height: 50,
                    width: 50,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                  child: Image.asset(
                    'assets/app bar.png',
                    height: 60,
                    width: 180,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CustomScaffold extends StatefulWidget {
  final Widget body;
  final bool showCircularProgressIndicator;
  final Widget? floatingActionButton;
  final Widget? bottomNavBar;

  const CustomScaffold({
    required this.body,
    this.showCircularProgressIndicator = false,
    this.floatingActionButton,
    this.bottomNavBar,
  });

  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _Header extends StatelessWidget {
  final String title;

  const _Header({required this.title});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style:
                  Theme.of(context).textTheme.titleLarge!.copyWith(fontSize: 16),
              textAlign: TextAlign.start,
            ),
          ],
        ),
      ),
    );
  }
}

class _CustomScaffoldState extends State<CustomScaffold> {
  late Future<User?> customerCredentials;

  @override
  void initState() {
    super.initState();
    customerCredentials = getNullableUser();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: customerCredentials,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final user = snapshot.data as User?;

          if (user == null) {
            return buildScaffold('', false);
          } else {
            return buildScaffold(user.name, user.isRegularCustomer);
          }
        }

        if (snapshot.hasError) {
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(context: context, builder: (_) => SomethingWentWrong());
          });
        }

        return buildScaffold('');
      },
    );
  }

  Widget buildScaffold(String name, [bool isRegularCustomer = false]) {
    return Scaffold(
      bottomNavigationBar: widget.bottomNavBar,
      appBar: customAppBar,
      body: SafeArea(
        child: CustomBackground(
          child: this.widget.body,
          showCircularProgressIndicator: widget.showCircularProgressIndicator,
        ),
      ),
      floatingActionButton: widget.floatingActionButton,
      drawer: SizedBox(
        width: 320,
        child: Drawer(
          child: ListView(
            children: <Widget>[
              _CustomDrawerHeader(),
              ExpansionPanelList.radio(
                elevation: 0,
                expandedHeaderPadding: const EdgeInsets.all(0),
                children: [
                  ExpansionPanelRadio(
                    canTapOnHeader: true,
                    value: 'Account',
                    headerBuilder: (context, isExpanded) {
                      return _Header(
                        title: 'Hello, ${name.capitalize}',
                      );
                    },
                    body: Column(
                      children: [
                        CustomTile(
                          title: Text('Change Password'),
                          onTap: (context) {
                            if (Scaffold.of(context).isDrawerOpen) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);

                            navigateToChangePassword();
                          },
                        ),
                      ],
                    ),
                  ),
                  ExpansionPanelRadio(
                    canTapOnHeader: true,
                    value: 'Legal',
                    headerBuilder: (_, __) => _Header(
                      title: 'Legal And Policies',
                    ),
                    body: Column(
                      children: [
                        CustomTile(
                          title: Text('About Us'),
                          onTap: (context) {
                            if (Scaffold.of(context).isDrawerOpen) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                            navigateToWebView(
                                '$domain_name/web/about_us.html?header=false');
                          },
                        ),
                        CustomTile(
                          title: Text('Contact Us'),
                          onTap: (context) {
                            if (Scaffold.of(context).isDrawerOpen) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                            navigateToWebView(
                                '$domain_name/web/contact_us.html?header=false');
                          },
                        ),
                        CustomTile(
                          title: Text('Privacy Policy'),
                          onTap: (context) {
                            if (Scaffold.of(context).isDrawerOpen) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                            navigateToWebView(
                                '$domain_name/web/privacy_policy.html?header=false');
                          },
                        ),
                        CustomTile(
                          title: Text('Cancellation and Refund Policy'),
                          onTap: (context) {
                            if (Scaffold.of(context).isDrawerOpen) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                            navigateToWebView(
                                '$domain_name/web/cancellation_and_refund.html?header=false');
                          },
                        ),
                        CustomTile(
                          title: Text('Terms and Conditions'),
                          onTap: (context) {
                            if (Scaffold.of(context).isDrawerOpen) {
                              Navigator.of(context).pop();
                            }

                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                            navigateToWebView(
                                '$domain_name/web/terms_and_conditions.html?header=false');
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Divider(),
              CustomTile(
                title: Text(
                  'Log out',
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontSize: 16),
                ),
                onTap: (context) async {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }

                  await deleteUser();

                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.of(context).pushReplacementNamed('/');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum Legal {
  PrivacyPolicy,
  CancellationAndRefundPolicy,
  TermsAndConditions,
}

class CustomScaffoldAdmin extends StatelessWidget {
  final Widget body;
  final bool showCircularProgressIndicator;
  final Widget? floatingActionButton;

  const CustomScaffoldAdmin({
    required this.body,
    this.showCircularProgressIndicator = false,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar,
      body: SafeArea(
        child: CustomBackground(
          child: body,
          showCircularProgressIndicator: showCircularProgressIndicator,
        ),
      ),
      floatingActionButton: floatingActionButton,
      drawer: SizedBox(
        width: 320,
        child: Drawer(
          child: ListView(
            children: [
              _CustomDrawerHeader(),
              Visibility(
                visible: staffPermissions.home,
                child: CustomTile(
                  title: Text('Home'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => false);
                    navigateToAdminHome();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.addQuantity,
                child: CustomTile(
                  title: Text('Add Quantity'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }

                    Navigator.of(context).popUntil((route) => route.isFirst);

                    navigateToAddQuantity();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.setMenu,
                child: CustomTile(
                  title: Text('Set Menu'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    navigateToSetMenu();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.notifications,
                child: CustomTile(
                  title: Text('Notifications'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    navigateToSetNotifications();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.notificationsNotSent,
                child: CustomTile(
                  title: Text('Notifications Not Sent'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    navigateToNotificationsNotSent();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.addOrder,
                child: CustomTile(
                  title: Text('Add Order'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    navigateToAddOrder();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.cancelOrder,
                child: CustomTile(
                  title: Text('Cancel Order'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    navigateToCancelOrder();
                  },
                ),
              ),
              Visibility(
                visible: staffPermissions.updatePaymentDetail,
                child: CustomTile(
                  title: Text('Update payment detail'),
                  onTap: (context) {
                    if (Scaffold.of(context).isDrawerOpen) {
                      Navigator.of(context).pop();
                    }
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    navigateToUpdatePaymentDetail();
                  },
                ),
              ),
              CustomTile(
                title: Text('Log out'),
                onTap: (context) async {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }

                  await deleteUser();

                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.of(context).pushReplacementNamed('/');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

typedef void OnTap(context);

class CustomTile extends StatelessWidget {
  final Widget title;
  final OnTap onTap;

  const CustomTile({
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: title,
          onTap: () => onTap(context),
        ),
        Divider(),
      ],
    );
  }
}
