# Model
Model contains classes that represents the data of the view.

# ViewModel
Contain the current values of data stored in view and validation logic. It also contains helps properties. 
**Note:** Validation logic only guarantees that **trimmed** values are valid

# View
View contains display layout and display logic (view updating when data changes or is invalid) 

# Repository
Classes that fetch data (from where is none of our concern). It has Stub classes too which take input from contructor and return
relevant data when needed.  