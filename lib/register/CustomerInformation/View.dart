

import 'package:flutter/material.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:provider/provider.dart';

import 'ViewModel.dart';

// This widget is purely for input.
class CustomerInformation extends StatefulWidget {
  @override
  _CustomerInformationState createState() => _CustomerInformationState();
}

class _CustomerInformationState extends State<CustomerInformation>
    with AutomaticKeepAliveClientMixin {
  final _controllers = {
    'firstName': TextEditingController(),
    'lastName': TextEditingController(),
    'email': TextEditingController(),
    'phoneNumber': TextEditingController(),
    'password': TextEditingController(),
  };

  bool _passwordVisible = false;

  @override
  void dispose() {
    _controllers.forEach((_, controller) {
      controller.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // I don't want to listen to the changes CustomerViewModel is changed only by this class.
    final _viewModel = Provider.of<CustomerViewModel>(context);

    _viewModel.firstName = () => _controllers['firstName']!.text;
    _viewModel.lastName = () => _controllers['lastName']!.text;
    _viewModel.email = () => _controllers['email']!.text;
    _viewModel.phoneNumber = () => _controllers['phoneNumber']!.text;
    _viewModel.password = () => _controllers['password']!.text;

    return CardTile(
      title: Text(
        'Basic Information',
        style: Theme.of(context).textTheme.headline6,
      ),
      child: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: <Widget>[
            TextFormField(
              controller: _controllers['firstName'],
              validator: (_) => _viewModel.isFirstNameValid()
                  ? null
                  : 'First name should only consist of a-z and A-Z',
              decoration: InputDecoration(
                icon: Icon(Icons.person),
                labelText: 'First Name',
              ),
              textCapitalization: TextCapitalization.words,
            ),
            TextFormField(
              controller: _controllers['lastName'],
              validator: (_) =>
              _viewModel.isLastNameValid()
                  ? null
                  : 'Last name should only consist of a-z and A-Z',
              decoration: InputDecoration(
                icon: Icon(Icons.person),
                labelText: 'Last Name',
              ),
              textCapitalization: TextCapitalization.words,
            ),
            TextFormField(
              controller: _controllers['email'],
              validator: (_) =>
              _viewModel.isEmailValid() ? null : 'Invalid email',
              decoration: InputDecoration(
                icon: Icon(Icons.mail),
                labelText: 'Email',
              ),
            ),
            TextFormField(
              controller: _controllers['phoneNumber'],
              validator: (_) => _viewModel.isPhoneNumberValid()
                  ? null
                  : 'Phone No. must be 10 digits only',
              decoration: InputDecoration(
                icon: Icon(Icons.phone),
                labelText: 'Phone No.',
              ),
              keyboardType: TextInputType.phone,
            ),
            TextFormField(
              validator: (_) =>
              _viewModel.isPasswordValid()
                  ? null
                  : 'Password must be 8 at least characters',
              controller: _controllers['password'],
              obscureText: !_passwordVisible,
              decoration: InputDecoration(
                icon: Icon(Icons.security),
                labelText: 'Password',
                suffixIcon: IconButton(
                  icon: Icon(
                    _passwordVisible ? Icons.visibility_off : Icons.visibility,
                    semanticLabel:
                    _passwordVisible ? 'hide password' : 'show password',
                  ),
                  onPressed: () {
                    setState(
                          () {
                        /// Copied from stack overflow. a xor 1 is same as !a. i.e. short hand for a = !a
                        _passwordVisible ^= true;
                      },
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
