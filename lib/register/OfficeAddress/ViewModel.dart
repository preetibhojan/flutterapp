import 'package:collection/collection.dart' show IterableExtension;
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:validators/validators.dart';

class OfficeAddressViewModel {
  final AreaRepository repository;

  late String Function() floor;
  late String Function() officeNumber;
  late String Function() company;

  OfficeAddressViewModel({required this.repository});

  late List<Area> areas;
  late List<Locality> localities;
  late List<Tower> towers;

  Area? currentArea;
  Locality? currentLocality;
  Tower? currentTower;

  Future<void> fetchData() async {
    areas = await repository.area;
    areas.sort((a, b) => a.name.compareTo(b.name));

    localities = (await repository.allLocalities)
        .where((element) => element.office == true)
        .toList();
    localities.sort((a, b) => a.name.compareTo(b.name));

    towers = (await repository.towers)
        .where((tower) => tower.home == false)
        .toList();

    towers.sort((a, b) => a.name.compareTo(b.name));

    currentArea = areas[0];
    currentLocality = localities.firstWhereOrNull(
      (locality) => locality.areaID == currentArea!.id,
    );
    currentTower = towers.firstWhereOrNull(
      (tower) => tower.localityID == currentLocality!.id,
    );
  }

  bool isFloorValid() =>
      isNumeric(floor().trim()) && int.parse(floor().trim()) >= 0;

  bool isOfficeNumberValid() =>
      isAlphaNumericUnderScoreOrSpace(officeNumber().trim());

  bool isCompanyValid() => isAlphaNumericUnderScoreOrSpace(company().trim());

  bool isValid() =>
      currentTower != null &&
          isFloorValid() &&
          isOfficeNumberValid() &&
      isCompanyValid();

  @override
  String toString() {
    return '${officeNumber()}, ${floor()}, $currentArea, ${company()}';
  }
}
