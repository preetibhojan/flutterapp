import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/register/OfficeAddress/ViewModel.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:provider/provider.dart';

class OfficeAddress extends StatefulWidget {
  @override
  _OfficeAddressState createState() => _OfficeAddressState();
}

class _OfficeAddressState extends State<OfficeAddress>
    with AutomaticKeepAliveClientMixin {
  final _controllers = {
    'tower': TextEditingController(),
    'floor': TextEditingController(),
    'officeNumber': TextEditingController(),
    'company': TextEditingController(),
  };

  late OfficeAddressViewModel _viewModel;
  late Future _future;

  @override
  void initState() {
    super.initState();
    _viewModel = Provider.of<OfficeAddressViewModel>(context, listen: false);

    _viewModel.floor = () => _controllers['floor']!.text;
    _viewModel.officeNumber = () => _controllers['officeNumber']!.text;
    _viewModel.company = () => _controllers['company']!.text;

    _future = _viewModel.fetchData();
  }

  @override
  void dispose() {
    _controllers.forEach((_, controller) {
      controller.dispose();
    });

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return CardTile(
      title: Text(
        'Office Address',
        style: Theme.of(context).textTheme.headline6,
      ),
      child: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: FutureBuilder(
          future: _future,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                !snapshot.hasError) {
              return Column(
                children: <Widget>[
                  DropdownButtonFormField<Area>(
                    isDense: true,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Area',
                    ),
                    items: _viewModel.areas.map((Area area) {
                      return DropdownMenuItem<Area>(
                        child: Text(area.name),
                        value: area,
                      );
                    }).toList(),
                    value: _viewModel.currentArea,
                    onChanged: (value) {
                      setState(() {
                        _viewModel.currentArea = value;
                        _viewModel.currentLocality =
                            _viewModel.localities.firstWhere(
                          (locality) =>
                              locality.areaID == _viewModel.currentArea!.id,
                        );
                        _viewModel.currentTower = _viewModel.towers.firstWhereOrNull(
                          (tower) =>
                              tower.localityID == _viewModel.currentLocality!.id,
                        );
                      });
                    },
                  ),
                  DropdownButtonFormField<Locality>(
                    isDense: true,
                    value: _viewModel.currentLocality,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Locality',
                    ),
                    items: _viewModel.localities
                        .where((element) =>
                            element.areaID == _viewModel.currentArea!.id)
                        .map((Locality locality) {
                      return DropdownMenuItem<Locality>(
                        child: Text(locality.name),
                        value: locality,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _viewModel.currentLocality = value;
                        _viewModel.currentTower = _viewModel.towers.firstWhereOrNull(
                          (tower) =>
                              tower.localityID == _viewModel.currentLocality!.id,
                        );
                      });
                    },
                  ),
                  DropdownButtonFormField<Tower>(
                    isDense: true,
                    value: _viewModel.currentTower,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Tower',
                    ),
                    items: _viewModel.towers
                        .where((element) =>
                            element.localityID == _viewModel.currentLocality!.id)
                        .map((Tower tower) {
                      return DropdownMenuItem<Tower>(
                        child: Text(tower.name),
                        value: tower,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _viewModel.currentTower = value;
                      });
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Floor',
                    ),

                    // Pretty much useless to have a validator because the virtual
                    // keyboard will do that for us but still ...
                    // Oops is guess dart programmers only use .. ;P

                    validator: (_) => _viewModel.isFloorValid()
                        ? null
                        : 'Floor must be a positive number',
                    controller: _controllers['floor'],
                    keyboardType: TextInputType.number,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Office No.',
                    ),
                    validator: (_) => _viewModel.isOfficeNumberValid()
                        ? null
                        : 'Office No. must contain only a-z, A-Z, 0-9, _ or space',
                    controller: _controllers['officeNumber'],
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.computer),
                      labelText: 'Company',
                    ),
                    validator: (_) => _viewModel.isCompanyValid()
                        ? null
                        : 'Company must contain only a-z, A-Z, 0-9, _ or space',
                    controller: _controllers['company'],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Container();
            }

            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
