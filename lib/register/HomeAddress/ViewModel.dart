import 'package:collection/collection.dart' show IterableExtension;
import 'package:preetibhojan/util/string.dart';

import '../../common/delivery_location/Models.dart';
import '../../common/delivery_location/Repositories.dart';

class HomeAddressViewModel {
  final AreaRepository repository;

  HomeAddressViewModel({required this.repository});

  late String Function() building;
  late String Function() flatNumber;

  late List<Area> areas;
  late List<Locality> localities;
  late List<Tower> towers;
  bool doorStepDelivery = false;

  Area? currentArea;
  Locality? currentLocality;
  Tower? currentTower;

  Future<void> fetchData() async {
    areas = await repository.area;
    areas.sort((a, b) => a.name.compareTo(b.name));

    localities = (await repository.allLocalities)
        .where((element) => element.home == true)
        .toList();

    localities.sort((a, b) => a.name.compareTo(b.name));

    towers =
        (await repository.towers).where((tower) => tower.home == true).toList();

    towers.sort((a, b) => a.name.compareTo(b.name));

    currentArea = areas[0];
    currentLocality = localities.firstWhereOrNull(
      (locality) => locality.areaID == currentArea!.id,
    );
    currentTower = towers.firstWhereOrNull(
      (tower) => tower.localityID == currentLocality!.id,
    );
  }

  bool isBuildingValid() => isAlphaNumericUnderScoreOrSpace(building());

  bool isFlatNumberValid() => isAlphaNumericUnderScoreOrSpace(flatNumber());

  @override
  String toString() {
    return '${building()}, ${flatNumber()}, $currentArea, $currentLocality';
  }

  bool isValid() =>
      currentTower != null && isBuildingValid() && isFlatNumberValid();
}
