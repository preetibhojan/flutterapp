import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/DoorStepDelivery.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import '../../common/delivery_location/Models.dart';
import 'ViewModel.dart';

class HomeAddress extends StatefulWidget {
  final DoorStepDelivery doorStepDelivery;

  const HomeAddress(this.doorStepDelivery);

  @override
  _HomeAddressState createState() => _HomeAddressState();
}

class _HomeAddressState extends State<HomeAddress>
    with AutomaticKeepAliveClientMixin {
  final _controllers = {
    'society': TextEditingController(),
    'building': TextEditingController(),
    'flatNumber': TextEditingController(),
  };

  @override
  void dispose() {
    _controllers.forEach((_, controller) {
      controller.dispose();
    });
    super.dispose();
  }

  late Future _future;
  late HomeAddressViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = Provider.of<HomeAddressViewModel>(context, listen: false);

    _viewModel.building = () => _controllers['building']!.text;
    _viewModel.flatNumber = () => _controllers['flatNumber']!.text;

    _future = _init();
  }

  late int doorStepDelivery;

  Future<void> _init() async {
    await _viewModel.fetchData();
    doorStepDelivery = await widget.doorStepDelivery();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return CardTile(
      title: Text(
        'Home Address',
        style: Theme.of(context).textTheme.titleLarge,
      ),
      child: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: FutureBuilder(
          future: _future,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                !snapshot.hasError) {
              return Column(
                children: <Widget>[
                  DropdownButtonFormField<Area>(
                    isDense: true,
                    value: _viewModel.currentArea,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Area',
                    ),
                    items: _viewModel.areas.map((Area area) {
                      return DropdownMenuItem<Area>(
                        child: Text(area.name),
                        value: area,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _viewModel.currentArea = value;
                        _viewModel.currentLocality =
                            _viewModel.localities.firstWhere(
                                  (locality) =>
                              locality.areaID == _viewModel.currentArea!.id,
                            );
                        _viewModel.currentTower =
                            _viewModel.towers.firstWhereOrNull(
                                  (tower) =>
                              tower.localityID ==
                                  _viewModel.currentLocality!.id,
                            );
                      });
                    },
                  ),
                  DropdownButtonFormField<Locality>(
                    isDense: true,
                    value: _viewModel.currentLocality,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Locality',
                    ),
                    items: _viewModel.localities
                        .where((element) =>
                    element.areaID == _viewModel.currentArea!.id)
                        .map((Locality locality) {
                      return DropdownMenuItem<Locality>(
                        child: Text(locality.name),
                        value: locality,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _viewModel.currentLocality = value;
                        _viewModel.currentTower =
                            _viewModel.towers.firstWhereOrNull(
                                  (tower) =>
                              tower.localityID ==
                                  _viewModel.currentLocality!.id,
                            );
                      });
                    },
                  ),
                  DropdownButtonFormField<Tower>(
                    isDense: true,
                    value: _viewModel.currentTower,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Society',
                    ),
                    items: _viewModel.towers
                        .where((element) =>
                            element.localityID ==
                            _viewModel.currentLocality!.id)
                        .map((Tower tower) {
                      return DropdownMenuItem<Tower>(
                        child: Text(tower.name),
                        value: tower,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _viewModel.currentTower = value;
                      });
                    },
                  ),
                  TextFormField(
                    controller: _controllers['building'],
                    validator: (_) => _viewModel.isBuildingValid()
                        ? null
                        : 'Building must only contain a-z, A-Z, 0-9, _ and space',
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Building',
                    ),
                    textCapitalization: TextCapitalization.words,
                  ),
                  TextFormField(
                    controller: _controllers['flatNumber'],
                    validator: (_) => _viewModel.isFlatNumberValid()
                        ? null
                        : 'Flat no. must only contain a-z, A-Z, 0-9, _ and space',
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Flat No.',
                    ),
                  ),
                  CheckboxListTile(
                    value: _viewModel.doorStepDelivery,
                    title: Text('Door Step Delivery'),
                    subtitle:
                        Text('Upto $rupeeSymbol $doorStepDelivery per Box'),
                    onChanged: (value) {
                      setState(() {
                        _viewModel.doorStepDelivery = value!;
                      });
                    },
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });

              print(snapshot.error);
              return Container();
            }

            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
