import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Notification.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/register/CuisineInformation/View.dart';
import 'package:preetibhojan/register/CuisineInformation/ViewModel.dart';
import 'package:preetibhojan/common/DoorStepDelivery.dart';
import 'package:preetibhojan/register/MakeRequest.dart';
import 'package:preetibhojan/user_home/Navigators.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/InvalidInputDialog.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import '../common/delivery_location/Repositories.dart';
import 'CustomerInformation/View.dart';
import 'CustomerInformation/ViewModel.dart';
import 'HomeAddress/View.dart';
import 'HomeAddress/ViewModel.dart';
import 'OfficeAddress/View.dart';
import 'OfficeAddress/ViewModel.dart';

class Register extends StatefulWidget {
  final DoorStepDelivery doorStepDelivery;

  const Register(this.doorStepDelivery);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool homeAddressEnabled = false, officeAddressEnabled = false;
  final _log = Logger('Register');
  final pageController = PageController();

  bool showCircularProgressIndicator = false;

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<CustomerViewModel>(
          create: (_) => CustomerViewModel(),
        ),
        Provider<HomeAddressViewModel>(
          create: (_) {
            return HomeAddressViewModel(
              repository: NetworkAreaRepository(),
            );
          },
        ),
        Provider<OfficeAddressViewModel>(
          create: (_) {
            return OfficeAddressViewModel(
              repository: NetworkAreaRepository(),
            );
          },
        ),
        ChangeNotifierProvider<CuisineInformationViewModel>(
          create: (BuildContext context) => CuisineInformationViewModel(
            NetworkCuisineRepository(),
          ),
        ),
      ],
      child: Scaffold(
        appBar: customAppBar,
        body: SafeArea(
          child: CustomBackground(
            showCircularProgressIndicator: showCircularProgressIndicator,
            child: PageView(
              controller: pageController,
              children: <Widget>[
                ListView(
                  children: <Widget>[
                    CustomerInformation(),
                    Consumer<CuisineInformationViewModel>(
                      builder: (context, cuisineInformationViewModel, _) {
                        return FutureBuilder(
                          future: cuisineInformationViewModel.fetchCuisines(),
                          builder: (context, snapshot) {
                            if (snapshot.hasError) {
                              SchedulerBinding.instance
                                  .addPostFrameCallback((timeStamp) {
                                showDialog(
                                  context: context,
                                  builder: (_) => SomethingWentWrong(),
                                );
                              });

                              return Container();
                            }

                            if (snapshot.connectionState ==
                                ConnectionState.done) {
                              return CuisineInformation();
                            }

                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          },
                        );
                      },
                    ),
                    Consumer<CustomerViewModel>(
                      builder: (context, customerViewModel, _) {
                        return Column(
                          children: <Widget>[
                            ElevatedButton(
                              child: Text('Next'),
                              onPressed: () {
                                if (customerViewModel.isValid()) {
                                  pageController.nextPage(
                                    duration: Duration(milliseconds: 250),
                                    curve: Curves.easeInOut,
                                  );
                                  return;
                                }

                                showDialog(
                                  context: context,
                                  builder: (_) => InvalidInputDialog(),
                                );
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
                ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: Center(
                        child: Text(
                          'Delivery Address',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                    ),
                    Consumer<CuisineInformationViewModel>(
                      builder: (context, viewModel, _) {
                        if (viewModel.requiredCuisine != null &&
                            !viewModel.requiredCuisine!.showHomeAddress) {
                          return Container();
                        }

                        return Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                              child: CheckboxListTile(
                                onChanged: (bool? value) {
                                  setState(() {
                                    homeAddressEnabled = value!;
                                  });
                                },
                                value: homeAddressEnabled,
                                title: Text(
                                  'Home Address',
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                              ),
                            ),
                            Visibility(
                              visible: homeAddressEnabled,
                              child: HomeAddress(doorStepDelivery),
                            ),
                          ],
                        );
                      },
                    ),
                    Consumer<CuisineInformationViewModel>(
                      builder: (context, viewModel, _) {
                        if (viewModel.requiredCuisine != null &&
                            !viewModel.requiredCuisine!.showOfficeAddress) {
                          return Container();
                        }

                        return Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                              child: CheckboxListTile(
                                onChanged: (bool? value) {
                                  setState(() {
                                    officeAddressEnabled = value!;
                                  });
                                },
                                value: officeAddressEnabled,
                                title: Text(
                                  'Office Address',
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                              ),
                            ),
                            Visibility(
                              visible: officeAddressEnabled,
                              child: OfficeAddress(),
                            ),
                          ],
                        );
                      },
                    ),

                    /// Any random view model. I only care about the context
                    Consumer<CuisineInformationViewModel>(
                      builder: (context, _, __) {
                        return Column(
                          children: <Widget>[
                            ElevatedButton(
                              child: Text(
                                'Register',
                              ),
                              onPressed: () =>
                                  validateInputAndRegister(context),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  static const CUSTOMER_ALREADY_EXISTS = 2;

  void validateInputAndRegister(BuildContext context) async {
    final customerViewModel =
        Provider.of<CustomerViewModel>(context, listen: false);
    final homeAddressViewModel =
        Provider.of<HomeAddressViewModel>(context, listen: false);
    final officeAddressViewModel =
        Provider.of<OfficeAddressViewModel>(context, listen: false);
    final cuisineInformationViewModel =
        Provider.of<CuisineInformationViewModel>(context, listen: false);

    _log.info('Home address enabled: $homeAddressEnabled');
    _log.info('Office address enabled: $officeAddressEnabled');

    if (!homeAddressEnabled && !officeAddressEnabled) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please select either home address or office address'),
          actions: <Widget>[
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    if (!customerViewModel.isValid() ||
        (officeAddressEnabled && !officeAddressViewModel.isValid()) ||
        (homeAddressEnabled && !homeAddressViewModel.isValid())) {
      showDialog(
        context: context,
        builder: (_) => InvalidInputDialog(),
      );

      return;
    }

    setState(() {
      showCircularProgressIndicator = true;
    });

    try {
      final result = await register(
        customerViewModel,
        homeAddressEnabled ? homeAddressViewModel : null,
        officeAddressEnabled ? officeAddressViewModel : null,
        cuisineInformationViewModel,
        notification.token,
      );

      final storage = FlutterSecureStorage();

      await storage.write(
        key: 'phone_number',
        value: customerViewModel.phoneNumber().trim(),
      );

      await storage.write(
        key: 'password',
        value: customerViewModel.password().trim(),
      );

      await storage.write(
        key: 'name',
        value: customerViewModel.firstName().trim(),
      );

      /// need based delta depends on the cuisine. So set config to the updated
      /// config obtained during registration
      config = result.config!;

      Navigator.of(context).popUntil((route) => false);
      navigateToCustomerHome();
    } catch (on, stacktrace) {
      print(on);
      print(stacktrace);
      if (on is! Result) {
        _log.severe('Unknown exception', on, stacktrace);
        throw on;
      }

      final result = on;

      if (result.errorCode == CUSTOMER_ALREADY_EXISTS) {
        SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Oops'),
              content: Text(
                'A customer with the given phone number '
                'already exists. Try to log in',
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).popUntil((route) => false);
                    navigateToLogin();
                  },
                  child: Text('Log in'),
                )
              ],
            ),
          );
        });
        _log.info('Customer already exists');
        return;
      }

      _log.severe(
          'Unknown error returned by backend. Error code: ${result.errorCode}');

      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );
    } finally {
      setState(() {
        showCircularProgressIndicator = false;
      });
    }
  }
}
