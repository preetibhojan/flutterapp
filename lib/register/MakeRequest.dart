

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/register/CuisineInformation/ViewModel.dart';
import 'package:preetibhojan/register/HomeAddress/ViewModel.dart';
import 'package:preetibhojan/register/OfficeAddress/ViewModel.dart';

import 'CustomerInformation/ViewModel.dart';

/// I have given numbers instead of long names because this is only expected to be used internally

/// Note to self: MAke sure to trim textual inputs from user
Map<String, dynamic> makeRequest1(CustomerViewModel customerViewModel) {
  return {
    'first_name': customerViewModel.firstName().trim(),
    'last_name': customerViewModel.lastName().trim(),
    'email': customerViewModel.email().trim(),
    'phone_number': customerViewModel.phoneNumber().trim(),
    'password': customerViewModel.password().trim()
  };
}

Map<String, dynamic> makeRequest2(HomeAddressViewModel homeAddressViewModel) {
  return {
    'flat_number': homeAddressViewModel.flatNumber().trim(),
    'building': homeAddressViewModel.building().trim(),
    'society_id': homeAddressViewModel.currentTower!.id,
    'door_step_delivery': homeAddressViewModel.doorStepDelivery,
  };
}

Map<String, dynamic> makeRequest3(
    OfficeAddressViewModel officeAddressViewModel) {
  return {
    'office_number': officeAddressViewModel.officeNumber().trim(),
    'floor': int.parse(officeAddressViewModel.floor().trim()),
    'tower_id': officeAddressViewModel.currentTower!.id,
    'company': officeAddressViewModel.company().trim(),
  };
}

/// Untested because easy to reason about. It just uses the above functions and combines the output
Map<String, dynamic> makeRequest(
  CustomerViewModel customerViewModel,
  HomeAddressViewModel? homeAddressViewModel,
  OfficeAddressViewModel? officeAddressViewModel,
  CuisineInformationViewModel cuisineInformationViewModel,
  String? token,
) {
  /// make sure if home address is null, office address is not null and vice versa
  assert(homeAddressViewModel != null || officeAddressViewModel != null);

  final temp = <String, dynamic>{
    'customer': makeRequest1(customerViewModel),
    'cuisine_id': cuisineInformationViewModel.requiredCuisine!.id,
    'version': version,
  };

  if (homeAddressViewModel != null) {
    temp['home_address'] = makeRequest2(homeAddressViewModel);
  }

  if (officeAddressViewModel != null) {
    temp['office_address'] = makeRequest3(officeAddressViewModel);
  }

  temp['token'] = token;

  return temp;
}

class Result {
  final bool success;
  final int? errorCode;
  final Config? config;

  Result(this.success, this.config, this.errorCode);

  @override
  String toString() => '$success, $errorCode';
}

Future<Result> register(
  CustomerViewModel customerViewModel,
  HomeAddressViewModel? homeAddressViewModel,
  OfficeAddressViewModel? officeAddressViewModel,
  CuisineInformationViewModel cuisineInformationViewModel,
  String? token,
) async {
  final requestBody = makeRequest(
    customerViewModel,
    homeAddressViewModel,
    officeAddressViewModel,
    cuisineInformationViewModel,
    token,
  );

  var response;
  var url = Uri.parse(base_url + '/register');

  print(requestBody);

  response = await http.post(
    url,
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(requestBody),
  );

  final json = jsonDecode(response.body);

  if (response.statusCode == 200) {
    return Result(
      true,
      Config.fromJson(json['config']),
      null,
    );
  }

  throw Result(
    false,
    null,
    json['error'],
  );
}
