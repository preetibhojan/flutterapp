import 'package:async_builder/async_builder.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:preetibhojan/admin_home/Notifications/Repository.dart';
import 'package:preetibhojan/admin_home/Report/Repository.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Report/Model.dart';

class NotificationsNotSent extends StatefulWidget {
  final NotificationRepository _repository;
  final MealsRepository _mealsRepository;
  final ReportTimeRepository _reportTimeRepository;
  final ReportRepository _reportRepository;
  final DateTime now;

  const NotificationsNotSent(
    this._repository,
    this._mealsRepository,
    this._reportTimeRepository,
    this._reportRepository,
    this.now,
  );

  @override
  State<NotificationsNotSent> createState() => _NotificationsNotSentState();
}

class _NotificationsNotSentState extends State<NotificationsNotSent> {
  late Future<NotificationHistory> _future = _init();

  late List<Meal> _meals;
  late DateTime _date = setTimeToZero(widget.now);
  late Meal _currentMeal;

  Future<NotificationHistory> _init() async {
    _meals = await widget._mealsRepository.meals;

    _meals = await widget._mealsRepository.meals;
    final _reportTime = await widget._reportTimeRepository.reportTime;

    _meals.sort();
    _reportTime.sort();

    final report = _reportTime.firstWhereOrNull(
      /// first report time whose till date is yet to come
      (reportTime) => widget.now.isBefore(
        setTimeToZero(widget.now).add(reportTime.till),
      ),
    );

    _date = widget.now;

    if (report == null) {
      _date = widget.now.nextWorkingDay;
      _currentMeal = _meals[0];
    } else {
      _currentMeal = _meals.firstWhere((meal) => meal.id == report.mealID);
    }

    return _history(_currentMeal, _date);
  }

  late List<CustomerPackage> _customerWisePackages;

  late List<String> _towers;

  Future<NotificationHistory> _history(Meal meal, DateTime date) async {
    _customerWisePackages =
        await widget._reportRepository.customerWisePackagesForDeliveryExecutive(
      meal,
      date,
    );

    final notificationHistory =
        await widget._repository.notificationHistory(date, meal);

    _customerWisePackages.removeWhere(
      (customer) =>
          !notificationHistory.notSent.contains(customer.customerID),
    );

    _towers = _customerWisePackages.map((cp) => cp.tower).toSet().toList();

    return notificationHistory;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      body: AsyncBuilder<NotificationHistory>(
          future: _future,
          error: (context, error, stacktrace) {
            return Column(
              children: [
                Text("Error: " + error.toString()),
                Text(stacktrace.toString()),
              ],
            );
          },
          waiting: (context) => Center(child: CircularProgressIndicator()),
          builder: (context, notificationHistory) {
            if (notificationHistory == null) {
              return Container();
            }

            return ListView(
              padding: const EdgeInsets.all(16),
              children: [
                Center(
                  child: TextButton(
                    child: Text(
                      DateFormat.yMMMd().format(_date),
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    onPressed: () async {
                      final temp = await showDatePicker(
                        context: context,
                        firstDate: DateTime(2000),
                        initialDate: widget.now,
                        lastDate: DateTime(2100),
                      );

                      if (temp != null) {
                        setState(() {
                          _date = temp;
                          _future = _history(_currentMeal, _date);
                        });
                      }
                    },
                  ),
                ),
                DropdownButtonFormField<Meal>(
                  items: _meals
                      .map(
                        (meal) => DropdownMenuItem<Meal>(
                          child: Text(meal.name),
                          value: meal,
                        ),
                      )
                      .toList(),
                  onChanged: (meal) async {
                    setState(() {
                      _currentMeal = meal!;

                      _future = _history(_currentMeal, _date);
                    });
                  },
                  value: _currentMeal,
                  decoration: InputDecoration(
                    labelText: 'Meal',
                  ),
                ),
                SizedBox(height: 20,),
                ..._towers.map(
                  (tower) {
                    final customers = _customerWisePackages
                        .where((cp) => cp.tower == tower)
                        .toList();

                    if (customers.isEmpty) {
                      return Container();
                    }

                    final groupings =
                        customers.map((p) => p.grouping).toSet().toList();

                    groupings.sort((a, b) => b.compareTo(a));

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          tower,
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        SizedBox(height: 8),
                        ...groupings.map(
                          (g) {
                            /// Display customers who have only booked add ons
                            final packages = customers
                                .where((p) => p.grouping == g)
                                .toList();

                            final tempGroupedCustomers =
                                customers.where((p) => p.grouping == g);

                            final groupedCustomers = <CustomerPackage>[];
                            final uniqueCustomerIDs = <String>{};

                            for (final customer in tempGroupedCustomers) {
                              if (!uniqueCustomerIDs
                                  .contains(customer.customerID)) {
                                groupedCustomers.add(customer);
                                uniqueCustomerIDs.add(customer.customerID);
                              }
                            }

                            if (packages.isEmpty) {
                              return Container();
                            }

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 8),
                                Text(
                                  g,
                                  style: Theme.of(context).textTheme.titleLarge,
                                ),
                                ...groupedCustomers
                                    .map(
                                      (customer) => Row(
                                        children: [
                                          Text(
                                            '${customer.firstName} ${customer.lastName}',
                                            style: TextStyle(fontSize: 17),
                                          ),
                                          Spacer(),
                                          IconButton(
                                            icon: Icon(Icons.call),
                                            onPressed: () async {
                                              final url =
                                                  'tel:+91${customer.phoneNumber}';
                                              if (await canLaunchUrl(
                                                  Uri.parse(url))) {
                                                await launchUrl(Uri.parse(url));
                                              } else {
                                                throw 'Could not launch $url';
                                              }
                                            },
                                          ),
                                          if (customer.doorStepDelivery)
                                            Icon(Icons.door_front_door_outlined)
                                        ],
                                      ),
                                    )
                                    .toList(),
                              ],
                            );
                          },
                        ).toList(),
                      ],
                    );
                  },
                ).toList(),
              ],
            );
          }),
    );
  }
}
