import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:preetibhojan/admin_home/AddOrder/OrderRepository.dart';
import 'package:preetibhojan/admin_home/Report/Model.dart';
import 'package:preetibhojan/admin_home/Report/Repository.dart';
import 'package:preetibhojan/common/cuisines/Model.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';

class CancelOrder extends StatefulWidget {
  final OrderRepository orderRepository;
  final MealsRepository mealsRepository;
  final CuisineRepository cuisineRepository;
  final ReportRepository reportRepository;
  final DateTime now;

  const CancelOrder(
    this.orderRepository,
    this.mealsRepository,
    this.cuisineRepository,
    this.reportRepository,
    this.now,
  );

  @override
  _CancelOrderState createState() => _CancelOrderState();
}

class _CancelOrderState extends State<CancelOrder> {
  late Future<void> _initFuture;
  Future<List<CustomerPackage>>? _customerPackagesFuture;

  final _priceController = TextEditingController();
  final _quantityController = TextEditingController();

  @override
  void dispose() {
    _priceController.dispose();
    _quantityController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _initFuture = _init();
  }

  late List<Customer> _customers;
  late List<Meal> _meals;
  late List<Cuisine> _cuisines;

  late DateTime _date;

  Future<void> _init() async {
    // _customers = await widget.orderRepository.customers;
    // _customers.sort((a, b) => a.name.compareTo(b.name));

    _meals = await widget.mealsRepository.meals;
    _meals.sort();

    _cuisines = await widget.cuisineRepository.cuisines;
    _date = widget.now;
  }

  Customer? _customer;
  CustomerPackage? _package;
  Meal? _meal;
  Cuisine? _cuisine;

  bool _errorShown = false;

  bool _showProgressIndicator = false;

  String? _quantity, _price;
  bool _credit = true;

  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: _showProgressIndicator,
      body: AsyncBuilder(
        future: _initFuture,
        error: (context, error, stacktrace) {
          if (_errorShown) return Container();

          _errorShown = true;
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Oops!'),
                content: Text('Could not fetch data'),
                actions: [
                  TextButton(
                    child: Text('Retry'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop();
                        _errorShown = false;
                        _initFuture = _init();
                      });
                    },
                  ),
                ],
              ),
            );
          });

          return Container();
        },
        waiting: (context) => Center(
          child: CircularProgressIndicator(),
        ),
        builder: (context, dynamic value) {
          return Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _key,
            child: ListView(
              padding: const EdgeInsets.all(8),
              children: [
                TextButton(
                  child: Text(
                    DateFormat.yMMMMd().format(_date),
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  onPressed: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: _date,
                      firstDate: DateTime(2020),
                      lastDate: DateTime(2030),
                    );

                    if (date != null) {
                      setState(() {
                        _date = date;
                      });
                    }
                  },
                ),
                DropdownButtonFormField<Meal>(
                  decoration: InputDecoration(
                    labelText: 'Meals',
                  ),
                  items: _meals
                      .map(
                        (m) => DropdownMenuItem(
                          child: Text(m.name),
                          value: m,
                        ),
                      )
                      .toList(),
                  value: _meal,
                  onChanged: (meal) {
                    setState(() {
                      _meal = meal;
                      _package = null;
                      _customerPackagesFuture = _fetchCustomerPackages();
                    });
                  },
                  validator: (_) =>
                      _meal == null ? 'Please select a meal' : null,
                ),
                DropdownButtonFormField<Cuisine>(
                  decoration: InputDecoration(
                    labelText: 'Cuisines',
                  ),
                  items: _cuisines
                      .map(
                        (m) => DropdownMenuItem(
                          child: Text(m.name),
                          value: m,
                        ),
                      )
                      .toList(),
                  value: _cuisine,
                  onChanged: (cuisine) {
                    setState(() {
                      _cuisine = cuisine;
                      _package = null;
                      _customerPackagesFuture = _fetchCustomerPackages();
                    });
                  },
                  validator: (_) =>
                      _cuisine == null ? 'Please select a cuisine' : null,
                ),
                Container(height: 10),
                AsyncBuilder(
                  future: _customerPackagesFuture,
                  waiting: (_) => _customerPackagesFuture != null
                      ? Center(child: CircularProgressIndicator())
                      : Container(),
                  error: (context, error, stackTrace) {
                    if (_errorShown) return Container();

                    print(error);
                    print(stackTrace);
                    _errorShown = true;
                    SchedulerBinding.instance
                        .addPostFrameCallback((timeStamp) {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: Text('Oops!'),
                          content: Text('Could not fetch customers'),
                          actions: [
                            TextButton(
                              child: Text('Retry'),
                              onPressed: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                  _customerPackagesFuture =
                                      _fetchCustomerPackages();
                                });
                              },
                            ),
                          ],
                        ),
                      );
                    });

                    return Container();
                  },
                  builder:
                      (BuildContext context, List<CustomerPackage>? result) {
                    if (result == null || result.isEmpty) {
                      return Container();
                    }

                    List<CustomerPackage> packages = result;
                    if (_customer != null) {
                      packages = result
                          .where((cp) => cp.customerID == _customer?.id)
                          .toList();
                    }

                    return Column(
                      children: [
                        Autocomplete<Customer>(
                          fieldViewBuilder: (context, controller, focusNode,
                                  onFieldSubmitted) =>
                              TextFormField(
                            controller: controller,
                            validator: (_) => _customer == null
                                ? 'Please Select a customer'
                                : null,
                            decoration: InputDecoration(
                              labelText: 'Customer',
                            ),
                            onFieldSubmitted: (value) => onFieldSubmitted(),
                            focusNode: focusNode,
                          ),
                          optionsBuilder: (TextEditingValue textEditingValue) {
                            if (textEditingValue.text.isEmpty) {
                              return [];
                            }

                            return _customers.where((c) => c.name
                                .toLowerCase()
                                .contains(textEditingValue.text.toLowerCase()));
                          },
                          displayStringForOption: (Customer c) => c.name,
                          onSelected: (Customer c) {
                            setState(() {
                              _customer = c;
                            });
                          },
                        ),
                        DropdownButtonFormField<CustomerPackage>(
                          decoration: InputDecoration(
                            labelText: 'Packages',
                          ),
                          items: sort(
                            packages,
                            (a, b) => a.package.compareTo(b.package),
                          )
                              .map((p) => DropdownMenuItem(
                                    child: Text(p.displayName),
                                    value: p,
                                  ))
                              .toList(),
                          value: _package,
                          onChanged: (package) {
                            setState(() {
                              _package = package;
                              _priceController.text =
                                  _package!.price.toInt().toString();
                              _price = _priceController.text;
                              _quantityController.text =
                                  package!.quantity.toString();
                              _quantity = package.quantity.toString();
                            });
                          },
                          validator: (_) => _package == null
                              ? 'Please select a package'
                              : null,
                        ),
                        TextFormField(
                          controller: _quantityController,
                          decoration: InputDecoration(
                            labelText: 'Quantity',
                          ),
                          onChanged: (value) {
                            _quantity = value;
                          },
                          validator: (str) => int.tryParse(str!) == null ||
                                  int.tryParse(str)! < 1
                              ? 'Please enter a valid quantity'
                              : null,
                        ),
                        TextFormField(
                          controller: _priceController,
                          decoration: InputDecoration(
                            labelText: 'Price',
                          ),
                          onChanged: (value) {
                            _price = value;
                          },
                          validator: (str) => int.tryParse(str!) == null ||
                                  int.tryParse(str)! < 1
                              ? 'Please enter a valid quantity'
                              : null,
                        ),
                        SwitchListTile(
                          title: Text('Credit'),
                          value: _credit,
                          onChanged: (value) {
                            setState(() {
                              _credit = value;
                            });
                          },
                        ),
                        ElevatedButton(
                          child: Text('Cancel'),
                          onPressed: _cancel,
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Future<List<CustomerPackage>> _fetchCustomerPackages() async {
    if (_meal == null || _cuisine == null) {
      return [];
    }

    final temp =
        (await widget.reportRepository.allCustomerWisePackages(_meal!, _date))
            .where((c) => c.cuisineID == _cuisine!.id)
            .toList();

    _customers = temp
        .map(
          (customerPackage) => Customer(
            customerPackage.customerID,
            '${customerPackage.firstName} ${customerPackage.lastName}',
          ),
        )
        .toSet()
        .toList();

    return temp;
  }

  Future<void> _cancel() async {
    if (!_key.currentState!.validate()) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please correct the errors before clicking cancel'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
      return;
    }

    setState(() {
      _showProgressIndicator = true;
    });

    await widget.orderRepository.cancel(
      _customer!,
      _package!,
      _date,
      int.parse(_quantity!),
      int.parse(_price!),
      _credit,
    );

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Success'),
        content: Text('Package cancelled'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Okay'),
          ),
        ],
      ),
    );

    setState(() {
      _showProgressIndicator = false;
    });
  }
}
