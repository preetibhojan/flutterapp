import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:preetibhojan/admin_home/Report/Model.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/packages/Model.dart';

class Customer {
  final String id, name;
  final bool? doorStepDelivery;
  final Map<DeliveryLocation, Map<int, int>>? deliveryCharge;
  final List<DeliveryLocation> enabledDeliveryLocations = [];
  final bool? isRegular;
  final int? regularPackages;
  final double? balance;
  final String? phoneNumber;

  Customer(
    this.id,
    this.name, [
    this.doorStepDelivery,
    this.deliveryCharge,
    this.isRegular,
    this.regularPackages,
    this.balance,
    this.phoneNumber,
  ]) {
    if (deliveryCharge == null) {
      return;
    }

    if (deliveryCharge!.containsKey(DeliveryLocation.Home)) {
      enabledDeliveryLocations.add(DeliveryLocation.Home);
    }

    if (deliveryCharge!.containsKey(DeliveryLocation.Office)) {
      enabledDeliveryLocations.add(DeliveryLocation.Office);
    }
  }

  factory Customer.fromJson(Map<String, dynamic> json) {
    final deliveryChargePerDeliveryLocation =
        <DeliveryLocation, Map<int, int>>{};
    if (json['home_address_delivery_charge'] != null) {
      final deliveryChargePerMeal = <int, int>{};

      for (final entry in json['home_address_delivery_charge'].entries) {
        deliveryChargePerMeal[int.parse(entry.key)] = entry.value;
      }

      deliveryChargePerDeliveryLocation[DeliveryLocation.Home] =
          deliveryChargePerMeal;
    }
    if (json['office_address_delivery_charge'] != null) {
      final deliveryChargePerMeal = <int, int>{};

      for (final entry in json['office_address_delivery_charge'].entries) {
        deliveryChargePerMeal[int.parse(entry.key)] = entry.value;
      }

      deliveryChargePerDeliveryLocation[DeliveryLocation.Office] =
          deliveryChargePerMeal;
    }

    return Customer(
      json['id'],
      json['first_name'] + ' ' + json['last_name'],
      json['door_step_delivery'],
      deliveryChargePerDeliveryLocation,
      json['regular_customer'],
      json['regular_packages'],
      json['temp_credit'],
      json['phone_number'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Customer &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          doorStepDelivery == other.doorStepDelivery &&
          deliveryCharge == other.deliveryCharge &&
          enabledDeliveryLocations == other.enabledDeliveryLocations;

  @override
  int get hashCode =>
      id.hashCode ^
      name.hashCode ^
      doorStepDelivery.hashCode ^
      deliveryCharge.hashCode ^
      enabledDeliveryLocations.hashCode;
}

abstract class OrderRepository {
  Future<List<Customer>> get customers;

  Future<void> add(
    Customer customer,
    Package package,
    DateTime date,
    DeliveryLocation deliveryLocation,
    int quantity,
    int price,
    bool debit,
    bool doorStepDelivery,
  );

  Future<void> cancel(Customer customer, CustomerPackage package, DateTime date,
      int quantity, int price, bool credit);
}

class NetworkOrderRepository implements OrderRepository {
  @override
  Future<void> add(
    Customer customer,
    Package package,
    DateTime date,
    DeliveryLocation deliveryLocation,
    int quantity,
    int price,
    bool debit,
    bool doorStepDelivery,
  ) async {
    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/admin/add_order'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'customer_id': customer.id,
        'package_id': package.id,
        'date': DateFormat('yyyy-MM-dd').format(date),
        'delivery_location': deliveryLocation.toInt(),
        'quantity': quantity,
        'price': price,
        'debit': debit,
        'door_step_delivery': doorStepDelivery,
      }),
    );

    if (response.statusCode != 200) {
      throw Error();
    }
  }

  @override
  Future<List<Customer>> get customers async {
    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/admin/get_customers'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
      }),
    );

    if (response.statusCode != 200) {
      throw Error();
    }

    return (jsonDecode(response.body)['customers'] as List)
        .map((c) => Customer.fromJson(c))
        .toList();
  }

  @override
  Future<void> cancel(Customer customer, CustomerPackage package, DateTime date,
      int quantity, int price, bool credit) async {
    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/admin/cancel_order'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'customer_id': customer.id,
        'package_id': package.id,
        'date': DateFormat('yyyy-MM-dd').format(date),
        'quantity': quantity,
        'price': price,
        'credit': credit,
      }),
    );

    if (response.statusCode != 200) {
      throw Error();
    }
  }
}
