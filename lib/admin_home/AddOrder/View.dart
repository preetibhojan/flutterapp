import 'dart:math';

import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:preetibhojan/admin_home/Report/Model.dart';
import 'package:preetibhojan/admin_home/Report/Repository.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/cuisines/Model.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';

import 'OrderRepository.dart';

class AddOrder extends StatefulWidget {
  final OrderRepository orderRepository;
  final PackageRepository packageRepository;
  final MealsRepository mealsRepository;
  final CuisineRepository cuisineRepository;
  final ReportRepository reportRepository;
  final DateTime now;

  const AddOrder(
    this.orderRepository,
    this.packageRepository,
    this.mealsRepository,
    this.cuisineRepository,
    this.reportRepository,
    this.now,
  );

  @override
  _AddOrderState createState() => _AddOrderState();
}

class _AddOrderState extends State<AddOrder> {
  late Future<void> _initFuture;
  Future<List<CustomerPackage>>? _customerPackagesFuture;

  final _priceController = TextEditingController();

  @override
  void dispose() {
    _priceController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _initFuture = _init();
  }

  late List<Customer> _customers;
  late List<Package> _packages;
  late List<Meal> _meals;
  late List<Cuisine> _cuisines;

  late DateTime _date;

  Future<void> _init() async {
    _customers = await widget.orderRepository.customers;
    _customers.sort((a, b) => a.name.compareTo(b.name));

    _packages = await widget.packageRepository.packages;
    _packages.sort((a, b) => a.id.compareTo(b.id));

    _meals = await widget.mealsRepository.meals;
    _meals.sort();

    _cuisines = await widget.cuisineRepository.cuisines;
    _date = widget.now;
  }

  Customer? _customer;
  Package? _package;
  Meal? _meal;
  Cuisine? _cuisine;

  bool _errorShown = false;

  bool _showProgressIndicator = false;

  String? _quantity;
  bool _debit = true;
  DeliveryLocation? _deliveryLocation;

  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: _showProgressIndicator,
      body: AsyncBuilder(
        future: _initFuture,
        error: (context, error, stacktrace) {
          if (_errorShown) return Container();

          _errorShown = true;
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Oops!'),
                content: Text('Could not fetch data'),
                actions: [
                  TextButton(
                    child: Text('Retry'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop();
                        _errorShown = false;
                        _initFuture = _init();
                      });
                    },
                  ),
                ],
              ),
            );
          });

          return Container();
        },
        waiting: (context) => Center(
          child: CircularProgressIndicator(),
        ),
        builder: (context, dynamic value) {
          return Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _key,
            child: ListView(
              padding: const EdgeInsets.all(8),
              children: [
                TextButton(
                  child: Text(
                    DateFormat.yMMMMd().format(_date),
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  onPressed: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: _date,
                      firstDate: DateTime(2020),
                      lastDate: DateTime(2030),
                    );

                    if (date != null) {
                      setState(() {
                        _date = date;
                      });
                    }
                  },
                ),
                Autocomplete<Customer>(
                  fieldViewBuilder:
                      (context, controller, focusNode, onFieldSubmitted) =>
                          TextFormField(
                    controller: controller,
                    validator: (_) =>
                        _customer == null ? 'Please Select a customer' : null,
                    decoration: InputDecoration(
                      labelText: 'Customer',
                    ),
                    onFieldSubmitted: (value) => onFieldSubmitted(),
                    focusNode: focusNode,
                  ),
                  optionsBuilder: (TextEditingValue textEditingValue) {
                    if (textEditingValue.text.isEmpty) {
                      return [];
                    }

                    return _customers.where((c) => c.name
                        .toLowerCase()
                        .contains(textEditingValue.text.toLowerCase()));
                  },
                  displayStringForOption: (Customer c) => c.name,
                  onSelected: (Customer c) {
                    setState(() {
                      _customer = c;
                      _deliveryLocation = null;
                      _priceController.text = '';
                      _customerPackagesFuture = _fetchCustomerPackages();
                      _recalculatePrice();
                    });
                  },
                ),
                DropdownButtonFormField<Meal>(
                  decoration: InputDecoration(
                    labelText: 'Meals',
                  ),
                  items: _meals
                      .map(
                        (m) => DropdownMenuItem(
                          child: Text(m.name),
                          value: m,
                        ),
                      )
                      .toList(),
                  value: _meal,
                  onChanged: (meal) {
                    setState(() {
                      _meal = meal;
                      _package = null;
                      _customerPackagesFuture = _fetchCustomerPackages();
                      _recalculatePrice();
                    });
                  },
                  validator: (_) =>
                      _meal == null ? 'Please select a meal' : null,
                ),
                DropdownButtonFormField<Cuisine>(
                  decoration: InputDecoration(
                    labelText: 'Cuisines',
                  ),
                  items: _cuisines
                      .map(
                        (m) => DropdownMenuItem(
                          child: Text(m.name),
                          value: m,
                        ),
                      )
                      .toList(),
                  value: _cuisine,
                  onChanged: (cuisine) {
                    setState(() {
                      _cuisine = cuisine;
                      _package = null;
                      _customerPackagesFuture = _fetchCustomerPackages();
                      _recalculatePrice();
                    });
                  },
                  validator: (_) =>
                      _cuisine == null ? 'Please select a cuisine' : null,
                ),
                Container(height: 10),
                AsyncBuilder(
                  future: _customerPackagesFuture,
                  waiting: (_) => _customerPackagesFuture != null
                      ? Center(child: CircularProgressIndicator())
                      : Container(),
                  error: (context, error, stackTrace) {
                    if (_errorShown) return Container();

                    print(error);
                    print(stackTrace);
                    _errorShown = true;
                    SchedulerBinding.instance
                        .addPostFrameCallback((timeStamp) {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: Text('Oops!'),
                          content: Text('Could not fetch customers'),
                          actions: [
                            TextButton(
                              child: Text('Retry'),
                              onPressed: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                  _customerPackagesFuture =
                                      _fetchCustomerPackages();
                                });
                              },
                            ),
                          ],
                        ),
                      );
                    });

                    return Container();
                  },
                  builder:
                      (BuildContext context, List<CustomerPackage>? result) {
                    return Column(
                      children: [
                        Text(
                          'Packages ordered:',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        ...result!
                            .map(
                              (r) => Row(
                                children: [
                                  Text(r.package),
                                  Spacer(),
                                  Text('Qty. ${r.quantity}'),
                                  Spacer(),
                                  Text('Rs. ${r.price}'),
                                ],
                              ),
                            )
                            .toList()
                      ],
                    );
                  },
                ),
                DropdownButtonFormField<Package>(
                  decoration: InputDecoration(
                    labelText: 'Packages',
                  ),
                  items: _meal == null || _cuisine == null
                      ? []
                      : _packages
                          .where((p) => p.mealId == _meal!.id)
                          .where((p) => p.cuisineID == _cuisine!.id)
                          .map(
                            (p) => DropdownMenuItem(
                              child: Text(p.displayName!),
                              value: p,
                            ),
                          )
                          .toList(),
                  value: _package,
                  onChanged: _meal == null || _cuisine == null
                      ? null
                      : (package) {
                          setState(() {
                            _package = package;
                            _recalculatePrice();
                          });
                        },
                  validator: (_) =>
                      _package == null ? 'Please select a package' : null,
                ),
                DropdownButtonFormField<DeliveryLocation>(
                  decoration: InputDecoration(
                    labelText: 'Delivery Location',
                  ),
                  items: (_customer?.enabledDeliveryLocations ??
                          DeliveryLocation.valuesWithoutNotRequired)
                      .map((d) => DropdownMenuItem(
                            child: Text(d.name),
                            value: d,
                          ))
                      .toList(),
                  value: _deliveryLocation,
                  onChanged: (deliveryLocation) {
                    setState(() {
                      _deliveryLocation = deliveryLocation;
                      _recalculatePrice();
                    });
                  },
                  validator: (_) => _deliveryLocation == null
                      ? 'Please select a delivery location'
                      : null,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Quantity',
                  ),
                  onChanged: (value) {
                    _quantity = value;
                    _recalculatePrice();
                  },
                  validator: (str) =>
                      int.tryParse(str!) == null || int.tryParse(str)! < 1
                          ? 'Please enter a valid quantity'
                          : null,
                ),
                TextFormField(
                  controller: _priceController,
                  decoration: InputDecoration(
                    labelText: 'Price',
                  ),
                  validator: (str) =>
                      int.tryParse(str!) == null || int.tryParse(str)! < 1
                          ? 'Please enter a valid quantity'
                          : null,
                ),
                SwitchListTile(
                  title: Text('Debit'),
                  value: _debit,
                  onChanged: (value) {
                    setState(() {
                      _debit = value;
                    });
                  },
                ),
                Column(
                  children: [
                    ElevatedButton(
                      child: Text('Add'),
                      onPressed: _add,
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _recalculatePrice() {
    if (_package == null ||
        _customer == null ||
        _quantity == null ||
        _deliveryLocation == null ||
        int.tryParse(_quantity!) == null) {
      return;
    }

    setState(() {
      double priceForOneBox = _package!.price;

      if (_package!.addOn) {
        const addOnsDontHaveNeedBasedDelta = 0.0;
        _setPrice(priceForOneBox, addOnsDontHaveNeedBasedDelta);
        return;
      }

      priceForOneBox +=
          _customer!.deliveryCharge![_deliveryLocation]![_package!.mealId] ?? 0;

      final needBasedDelta = adminConfig[_meal!.id]![_cuisine!.id]!;

      priceForOneBox += needBasedDelta;

      _setPrice(priceForOneBox, needBasedDelta);
    });
  }

  void _setPrice(double priceForOneBox, double needBasedDelta) {
    final quantity = int.parse(_quantity!);
    int totalPrice = priceForOneBox.toInt() * quantity;

    totalPrice -=
        (needBasedDelta * min(quantity, _customer!.regularPackages!)).toInt();

    _priceController.text = totalPrice.toString();
  }

  Future<List<CustomerPackage>> _fetchCustomerPackages() async {
    if (_customer == null || _meal == null || _cuisine == null) {
      return [];
    }

    return (await widget.reportRepository.allCustomerWisePackages(_meal!, _date))
        .where((c) => c.cuisineID == _cuisine!.id)
        .where((c) => c.customerID == _customer!.id)
        .toList();
  }

  Future<void> _add() async {
    if (!_key.currentState!.validate()) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please correct the errors before clicking add'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
      return;
    }

    setState(() {
      _showProgressIndicator = true;
    });

    await widget.orderRepository.add(
      _customer!,
      _package!,
      _date,
      _deliveryLocation!,
      int.parse(_quantity!),
      int.parse(_priceController.text),
      _debit,
      _customer!.doorStepDelivery ?? false,
    );

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Success'),
        content: Text('Package added'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Okay'),
          ),
        ],
      ),
    );

    setState(() {
      _showProgressIndicator = false;
    });
  }
}
