import 'dart:convert';

import 'package:async_builder/async_builder.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/admin_home/AddOrder/OrderRepository.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';

class UpdatePaymentDetail extends StatefulWidget {
  final OrderRepository orderRepository;

  const UpdatePaymentDetail(this.orderRepository);

  @override
  _UpdatePaymentDetailState createState() => _UpdatePaymentDetailState();
}

enum _UpdateType {
  debit,
  credit,
}

class _Remark {
  final String text;

  const _Remark(this.text);

  static const gpay = _Remark('G Pay');
  static const phonePe = _Remark('Phone Pe');
  static const whatsapp = _Remark('Whats App');
  static const paytm = _Remark('Pay TM');
  static const bankTransfer = _Remark('Bank Transfer');
  static const others = _Remark('Others');

  static const values = [
    gpay,
    phonePe,
    whatsapp,
    bankTransfer,
    paytm,
    others,
  ];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _Remark &&
          runtimeType == other.runtimeType &&
          text == other.text;

  @override
  int get hashCode => text.hashCode;
}

class _UpdatePaymentDetailState extends State<UpdatePaymentDetail> {
  late Future _future;

  final _amount = TextEditingController();
  _Remark _remark = _Remark.others;

  @override
  void dispose() {
    _amount.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _future = _init();
  }

  late List<Customer> _customers;

  Future<void> _init() async {
    _customers = await widget.orderRepository.customers;
    _customers.sort((a, b) => a.name.compareTo(b.name));
  }

  bool _errorShown = false;
  Customer? _customer;
  _UpdateType _updateType = _UpdateType.debit;

  final _key = GlobalKey<FormState>();
  bool _showProgressIndicator = false;

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: _showProgressIndicator,
      body: AsyncBuilder(
        future: _future,
        waiting: (context) => Center(child: CircularProgressIndicator()),
        error: (context, error, stackTrace) {
          if (_errorShown) return Container();

          print(error);
          print(stackTrace);

          _errorShown = true;
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Oops!'),
                content: Text('Could not fetch data'),
                actions: [
                  TextButton(
                    child: Text('Retry'),
                    onPressed: () {
                      setState(() {
                        Navigator.of(context).pop();
                        _errorShown = false;
                        _future = _init();
                      });
                    },
                  ),
                ],
              ),
            );
          });

          return Container();
        },
        builder: (context, dynamic value) => Form(
          key: _key,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: ListView(
            padding: const EdgeInsets.all(8),
            children: [
              Autocomplete<Customer>(
                fieldViewBuilder:
                    (context, controller, focusNode, onFieldSubmitted) =>
                        TextFormField(
                  controller: controller,
                  validator: (_) =>
                      _customer == null ? 'Please Select a customer' : null,
                  decoration: InputDecoration(
                    labelText: 'Customer',
                  ),
                  onFieldSubmitted: (value) => onFieldSubmitted(),
                  focusNode: focusNode,
                ),
                optionsBuilder: (TextEditingValue textEditingValue) {
                  if (textEditingValue.text.isEmpty) {
                    return [];
                  }

                  return _customers.where((c) => c.name
                      .toLowerCase()
                      .contains(textEditingValue.text.toLowerCase()));
                },
                displayStringForOption: (Customer c) => c.name,
                onSelected: (Customer c) {
                  setState(() {
                    _customer = c;
                  });
                },
              ),
              SizedBox(height: 12),
              Text(
                'Balance: ${_customer == null ? 0 : _customer!.balance}',
                style: TextStyle(fontSize: 16),
              ),
              DropdownButtonFormField<_UpdateType>(
                items: [
                  DropdownMenuItem<_UpdateType>(
                    child: Text('Debit'),
                    value: _UpdateType.debit,
                  ),
                  DropdownMenuItem<_UpdateType>(
                    child: Text('Credit'),
                    value: _UpdateType.credit,
                  ),
                ],
                value: _updateType,
                decoration: InputDecoration(labelText: 'Update type'),
                onChanged: (value) {
                  setState(() {
                    _updateType = value!;
                  });
                },
                validator: (c) => c == null ? 'Choose a customer' : null,
              ),
              TextFormField(
                controller: _amount,
                decoration: InputDecoration(labelText: 'Amount'),
                validator: (str) =>
                    int.tryParse(str!) == null || int.tryParse(str)! < 1
                        ? 'Please enter a valid amount'
                        : null,
              ),
              DropdownButtonFormField<_Remark>(
                value: _remark,
                onChanged: (_Remark? newValue) {
                  _remark = newValue!;
                },
                items: _Remark.values
                    .map(
                      (e) => DropdownMenuItem<_Remark>(
                        child: Text(e.text),
                        value: e,
                      ),
                    )
                    .toList(),
                decoration: InputDecoration(labelText: 'Remark'),
              ),
              Column(
                children: [
                  ElevatedButton(
                    child: Text(
                        _updateType == _UpdateType.debit ? 'Debit' : 'Credit'),
                    onPressed: _debitOrCredit,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _debitOrCredit() async {
    if (!_key.currentState!.validate()) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please correct the errors'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
      return;
    }

    setState(() {
      _showProgressIndicator = true;
    });

    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/admin/update_payment_detail'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'customer_id': _customer!.id,
        'update_type': _updateType.index,
        'amount': int.parse(_amount.text),
        'remark': _remark.text,
      }),
    );

    setState(() {
      _showProgressIndicator = false;
    });

    print(response.statusCode);
    print(response.body);

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Could not update payment detail'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Payment detail updated'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }
  }
}
