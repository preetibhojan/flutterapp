import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/admin_home/Notifications/Model.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/misc.dart';

enum Result {
  UnauthorizedAccess,
  Success,
  UnknownError,
}

class NotificationHistory {
  final List<String> sent;
  final List<String> notSent;

  NotificationHistory(this.sent, this.notSent);

  factory NotificationHistory.fromJson(Map<String, dynamic> json) {
    return NotificationHistory(
      json['sent'].map((e) => e['customer_id']).cast<String>().toList(),
      json['not_sent'].map((e) => e['customer_id']).cast<String>().toList(),
    );
  }
}

class NotificationRepository {
  Future<NotificationHistory> notificationHistory(
      DateTime date, Meal meal) async {
    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/admin/notifications_sent2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'meal_id': meal.id,
        'date': DateFormat('yyyy-MM-dd').format(date),
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('body: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('Could not fetch notifications sent');
    }

    return NotificationHistory.fromJson(
        jsonDecode(response.body)['notifications_sent']);
  }

  static final _log = Logger('BackendNotificationRepository');

  Future<Result> sendNotification(List<String> customerIDs, Meal meal,
      NotificationToSend notification, bool addToNotificationLog) async {
    final user = await getUser();
    final response = await http.post(
      Uri.parse(base_url + '/admin/notify2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'title': notification.title,
        'body': notification.body,
        'customers': customerIDs,
        'meal_id': meal.id,
        'add_to_notification_log': addToNotificationLog,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('body: ${response.body}');

    if (response.statusCode != 200) {
      final error = jsonDecode(response.body)["error"];

      if (error == UnauthorizedAccessError) {
        return Result.UnauthorizedAccess;
      }

      return Result.UnknownError;
    }

    return Result.Success;
  }
}
