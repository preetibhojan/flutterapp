class NotificationToSend {
  final String title, body;

  NotificationToSend(this.title, this.body);

  bool get isValid => title.isNotEmpty && body.isNotEmpty;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is NotificationToSend &&
              runtimeType == other.runtimeType &&
              title == other.title &&
              body == other.body;

  @override
  int get hashCode => title.hashCode ^ body.hashCode;
}