import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/admin_home/Notifications/Repository.dart';
import 'package:preetibhojan/admin_home/Report/Model.dart';
import 'package:preetibhojan/admin_home/Report/Repository.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Model.dart';

late NotificationToSend _notification1, _notification2;

class Notifications extends StatefulWidget {
  final ReportRepository reportRepository;
  final MealsRepository mealsRepository;
  final CuisineRepository cuisineRepository;
  final NotificationRepository notificationSentRecords;
  final ReportTimeRepository reportTimeRepository;
  final DateTime now;

  const Notifications(
    this.reportRepository,
    this.reportTimeRepository,
    this.mealsRepository,
    this.cuisineRepository,
    this.notificationSentRecords,
    this.now,
  );

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  late Future _future;

  var _customNotification = NotificationToSend('', '');
  final _customNotificationTitle = TextEditingController();
  final _customNotificationBody = TextEditingController();

  @override
  void initState() {
    super.initState();
    _future = init();

    _customNotificationBody.addListener(() {
      _customNotification = NotificationToSend(
        _customNotificationTitle.text.trim(),
        _customNotificationBody.text.trim(),
      );

      setState(() {
        _notification = _customNotification;
      });
    });

    _customNotificationTitle.addListener(() {
      _customNotification = NotificationToSend(
        _customNotificationTitle.text.trim(),
        _customNotificationBody.text.trim(),
      );

      setState(() {
        _notification = _customNotification;
      });
    });
  }

  late List<CustomerPackage> _customerWisePackages;

  late List<Meal> _meals;
  late Meal _currentMeal;

  late DateTime _date;

  bool showProgressIndicator = false;

  late List<ReportTime> _reportTime;

  Future init() async {
    setState(() {
      showProgressIndicator = true;
    });

    _meals = await widget.mealsRepository.meals;
    _reportTime = await widget.reportTimeRepository.reportTime;

    _meals.sort();
    _reportTime.sort();

    final report = _reportTime.firstWhereOrNull(
      /// first report time whose till date is yet to come
      (reportTime) => widget.now.isBefore(
        setTimeToZero(widget.now).add(reportTime.till),
      ),
    );

    _date = widget.now;

    if (report == null) {
      _date = widget.now.nextWorkingDay;
      _currentMeal = _meals[0];
    } else {
      _currentMeal = _meals.firstWhere((meal) => meal.id == report.mealID);
    }

    await _customerWisePackagesFor(_currentMeal, _date);

    setState(() {
      showProgressIndicator = false;
    });
  }

  late List<String> towers;

  Future _customerWisePackagesFor(
    Meal meal,
    DateTime date,
  ) async {
    _customerWisePackages =
        await widget.reportRepository.customerWisePackagesForDeliveryExecutive(
      meal,
      date,
    );

    final notificationHistory =
        await widget.notificationSentRecords.notificationHistory(_date, meal);

    _customerWisePackages.removeWhere(
      (customer) =>
          notificationHistory.sent.contains(customer.customerID) ||
          notificationHistory.notSent.contains(customer.customerID),
    );

    towers = _customerWisePackages.map((cp) => cp.tower).toSet().toList();

    _notification1 = NotificationToSend(
      'Your ${meal.name} has arrived',
      'Please Pick your ${meal.name} from reception',
    );

    _notification2 = NotificationToSend(
      'Your ${meal.name} will arrive in 5 minutes',
      'Please Come to Pick up Your ${meal.name}',
    );

    _notification = _notification1;

    _customersToSendNotification.clear();
  }

  final _customersToSendNotification = Set<CustomerPackage>();

  late NotificationToSend _notification;

  @override
  void dispose() {
    super.dispose();
    _customNotificationTitle.dispose();
    _customNotificationBody.dispose();
  }

  bool _showCustomNotification = false;

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: showProgressIndicator,
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error);

            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });
          }

          if (snapshot.connectionState == ConnectionState.done) {
            return Padding(
              padding: const EdgeInsets.all(16),
              child: ListView(
                children: [
                  Center(
                    child: TextButton(
                      child: Text(
                        DateFormat.yMMMd().format(_date),
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      onPressed: () async {
                        final temp = await showDatePicker(
                          context: context,
                          firstDate: DateTime(2000),
                          initialDate: widget.now,
                          lastDate: DateTime(2100),
                        );

                        if (temp != null) {
                          setState(() {
                            _date = temp;
                            _future =
                                _customerWisePackagesFor(_currentMeal, _date);
                          });
                        }
                      },
                    ),
                  ),
                  DropdownButtonFormField<Meal>(
                    items: _meals
                        .map(
                          (meal) => DropdownMenuItem<Meal>(
                            child: Text(meal.name),
                            value: meal,
                          ),
                        )
                        .toList(),
                    onChanged: (meal) async {
                      setState(() {
                        _currentMeal = meal!;

                        _future = _customerWisePackagesFor(meal, _date);
                      });
                    },
                    value: _currentMeal,
                    decoration: InputDecoration(
                      labelText: 'Meal',
                    ),
                  ),
                  RadioListTile<NotificationToSend>(
                    onChanged: (value) {
                      setState(() {
                        _notification = value!;
                        _showCustomNotification = false;
                      });
                    },
                    value: _notification1,
                    groupValue: _notification,
                    title: Text(_notification1.title),
                  ),
                  RadioListTile<NotificationToSend>(
                    onChanged: (value) {
                      setState(() {
                        _notification = value!;
                        _showCustomNotification = false;
                      });
                    },
                    value: _notification2,
                    groupValue: _notification,
                    title: Text(_notification2.title),
                  ),
                  RadioListTile<NotificationToSend>(
                    onChanged: (value) {
                      setState(() {
                        _notification = value!;
                        _showCustomNotification = true;
                      });
                    },
                    value: _customNotification,
                    groupValue: _notification,
                    title: Text('Custom'),
                  ),
                  Visibility(
                    visible: _showCustomNotification,
                    child: Form(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _customNotificationTitle,
                            validator: (value) => value!.trim().isEmpty
                                ? 'Title cannot be empty'
                                : null,
                            decoration: InputDecoration(
                              labelText: 'Title',
                            ),
                          ),
                          TextFormField(
                            controller: _customNotificationBody,
                            validator: (value) =>
                                value!.isEmpty ? 'Body cannot be empty' : null,
                            decoration: InputDecoration(
                              labelText: 'Body',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  ...towers.map(
                    (tower) {
                      final customers = _customerWisePackages
                          .where((cp) => cp.tower == tower)
                          .toList();

                      if (customers.isEmpty) {
                        return Container();
                      }

                      final groupings =
                          customers.map((p) => p.grouping).toSet().toList();

                      groupings.sort((a, b) => b.compareTo(a));

                      return Column(
                        children: [
                          CheckboxListTile(
                            title: Text(
                              tower,
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                            onChanged: (bool? value) {
                              setState(() {
                                if (value!) {
                                  customers.forEach((p) {
                                    _customersToSendNotification.add(p);
                                  });
                                } else {
                                  _customersToSendNotification.removeWhere(
                                    (c) => c.tower == tower,
                                  );
                                }
                              });
                            },
                            value: customers
                                    .where((c) => _customersToSendNotification
                                        .contains(c))
                                    .length ==
                                customers.length,
                          ),
                          ...groupings.map(
                            (g) {
                              /// Display customers who have only booked add ons
                              final packages = customers
                                  .where((p) => p.grouping == g)
                                  .toList();

                              final tempGroupedCustomers =
                                  customers.where((p) => p.grouping == g);

                              final groupedCustomers = <CustomerPackage>[];
                              final uniqueCustomerIDs = <String>{};

                              for (final customer in tempGroupedCustomers) {
                                if (!uniqueCustomerIDs
                                    .contains(customer.customerID)) {
                                  groupedCustomers.add(customer);
                                  uniqueCustomerIDs.add(customer.customerID);
                                }
                              }

                              if (packages.isEmpty) {
                                return Container();
                              }

                              /// But dont count add on quantity
                              final nonAddOnPackages =
                                  packages.where((e) => !e.addOn).toList();

                              final quantity = nonAddOnPackages.isEmpty
                                  ? 0
                                  : nonAddOnPackages
                                      .map((e) => e.quantity)
                                      .reduce((a, b) => a + b);

                              return Column(
                                children: [
                                  CheckboxListTile(
                                    title: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 10, 0, 4),
                                      child: Text(
                                        '$g - $quantity',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                    ),
                                    value: packages
                                            .where((p) =>
                                                _customersToSendNotification
                                                    .where((c) =>
                                                        c.customerID ==
                                                        p.customerID)
                                                    .length !=
                                                0)
                                            .length ==
                                        packages.length,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        if (value!) {
                                          packages.forEach((p) {
                                            _customersToSendNotification.add(p);
                                          });
                                        } else {
                                          _customersToSendNotification
                                              .removeWhere(
                                            (c) => c.grouping == g,
                                          );
                                        }
                                      });
                                    },
                                  ),
                                  ...groupedCustomers
                                      .map(
                                        (customer) => CheckboxListTile(
                                          title: Row(
                                            children: [
                                              Text(
                                                '${customer.firstName} ${customer.lastName}',
                                                style: TextStyle(fontSize: 17),
                                              ),
                                              Spacer(),
                                              IconButton(
                                                icon: Icon(Icons.call),
                                                onPressed: () async {
                                                  final url =
                                                      'tel:+91${customer.phoneNumber}';
                                                  if (await canLaunchUrl(
                                                      Uri.parse(url))) {
                                                    await launchUrl(
                                                        Uri.parse(url));
                                                  } else {
                                                    throw 'Could not launch $url';
                                                  }
                                                },
                                              ),
                                              if (customer.doorStepDelivery)
                                                Icon(Icons
                                                    .door_front_door_outlined)
                                            ],
                                          ),
                                          value: _customersToSendNotification
                                              .contains(customer),
                                          onChanged: (bool? value) {
                                            setState(() {
                                              if (value!) {
                                                _customersToSendNotification
                                                    .add(customer);
                                              } else {
                                                _customersToSendNotification
                                                    .remove(customer);
                                              }
                                            });
                                          },
                                        ),
                                      )
                                      .toList(),
                                ],
                              );
                            },
                          ).toList(),
                        ],
                      );
                    },
                  ).toList(),
                  Column(
                    children: [
                      ElevatedButton(
                        child: Text(
                          'Notify',
                        ),
                        onPressed: _onNotify,
                      ),
                    ],
                  ),
                ],
              ),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  static final _log = Logger('Notifications');

  Future<void> _onNotify() async {
    if (_customersToSendNotification.length == 0) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please select at least one customer to notify'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    if (!_notification.isValid) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Text fields cannot be empty'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    _log.info('Getting user');

    setState(() {
      showProgressIndicator = true;
    });

    final addToNotificationLog =
        _notification == _notification1 || _notification == _notification2;

    try {
      final result = await widget.notificationSentRecords.sendNotification(
        _customersToSendNotification.map((e) => e.customerID).toSet().toList(),
        _currentMeal,
        _notification,
        addToNotificationLog,
      );

      if (result == Result.UnauthorizedAccess) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text('Unauthorized access'),
            actions: [
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
        return;
      }

      if (result == Result.UnknownError) {
        showDialog(
          context: context,
          builder: (_) => SomethingWentWrong(),
        );

        return;
      }

      setState(() {
        final phoneNumbers =
            _customersToSendNotification.map((e) => e.phoneNumber).toList();

        if (addToNotificationLog) {
          _customerWisePackages
              .removeWhere((e) => phoneNumbers.contains(e.phoneNumber));
        }

        _customersToSendNotification.clear();
      });

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Notifications Delivered'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    } catch (e) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text('Oops'),
                content: Text('Could not send notifications'),
                actions: [
                  TextButton(
                    child: Text('Okay'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ));
    } finally {
      setState(() {
        showProgressIndicator = false;
      });
    }
  }
}
