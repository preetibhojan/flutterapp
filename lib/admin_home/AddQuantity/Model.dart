

import 'package:preetibhojan/common/packages/Model.dart';

class PackageQuantity {
  final Package package;
  int quantity;

  PackageQuantity(this.package, [this.quantity = 0]);

  Map<String, dynamic> toJson() {
    return {
      'package_id': package.id,
      'quantity': quantity,
    };
  }
}
