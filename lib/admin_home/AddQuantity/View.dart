import 'dart:convert';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/cuisines/Model.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

import 'Model.dart';

class AddQuantity extends StatefulWidget {
  final MealsRepository mealsRepository;
  final PackageRepository packageRepository;
  final PackageRepository packageForMenuRepository;
  final CuisineRepository cuisineRepository;
  final DateTime now;

  const AddQuantity(
    this.mealsRepository,
    this.packageRepository,
    this.packageForMenuRepository,
    this.cuisineRepository,
    this.now,
  );

  @override
  _AddQuantityState createState() => _AddQuantityState();
}

const NotAllPackagesCouldBeUpdated = 4;
const DuplicatesFound = 5;

class _AddQuantityState extends State<AddQuantity> {
  late List<Cuisine> cuisines;
  late List<Meal> meals;
  late List<Package> allPackages, packagesForMenu;
  late List<PackageQuantity> packagesQuantity;

  static final _log = Logger('Set quantity');

  late Future _future;

  DateTime? _date;

  @override
  void initState() {
    super.initState();
    _future = init();
  }

  Future init() async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    final temp = await Future.wait([
      widget.cuisineRepository.cuisines,
      widget.mealsRepository.meals,
      widget.packageRepository.packages,
      widget.packageForMenuRepository.packages,
    ]);

    cuisines = temp[0] as List<Cuisine>;
    meals = temp[1] as List<Meal>;
    meals.sort();

    if (_date == null) {
      _date = setTimeToZero(dateToUse(meals, widget.now));
    }

    allPackages = temp[2] as List<Package>;

    if (_selectedCuisine == null) {
      _selectedCuisine = cuisines[0];
    }

    packagesForMenu = (temp[3] as List<Package>)
        .where((package) =>
            package.date == _date && package.cuisineID == _selectedCuisine!.id)
        .toList();

    packagesQuantity = <PackageQuantity>[];

    for (final package in allPackages) {
      packagesQuantity.add(
        PackageQuantity(
          packagesForMenu.firstWhereOrNull(
                (p) => package.id == p.id,
              ) ??
              package,
        ),
      );
    }

    setState(() {
      showCircularProgressIndicator = false;
    });

    return temp;
  }

  Cuisine? _selectedCuisine;

  bool showCircularProgressIndicator = true;

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: showCircularProgressIndicator,
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          _log.info(snapshot);

          if (snapshot.hasError) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });
          }

          if (snapshot.hasData) {
            return ListView(
              children: [
                Center(
                  child: TextButton(
                    child: Text(
                      DateFormat.yMMMMd().format(_date!),
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    onPressed: () async {
                      final temp = await showDatePicker(
                        context: context,
                        initialDate: _date!,
                        firstDate: widget.now,
                        lastDate: DateTime(2100),
                      );

                      if (temp != null) {
                        setState(() {
                          _date = temp;

                          _future = init();
                        });
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<Cuisine>(
                    items: cuisines
                        .map(
                          (cuisine) => DropdownMenuItem<Cuisine>(
                            child: Text(
                              cuisine.name,
                              style: TextStyle(fontSize: 17),
                            ),
                            value: cuisine,
                          ),
                        )
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        this._selectedCuisine = value;
                        _future = init();
                      });
                    },
                    value: _selectedCuisine,
                    hint: Text('Cuisine'),
                    decoration: InputDecoration(
                      labelText: 'Cuisine',
                    ),
                  ),
                ),
                ...meals.map(
                  (meal) {
                    final packagesForMeal = packagesQuantity
                        .where((packageQuantity) =>
                            packageQuantity.package.mealId == meal.id &&
                            packageQuantity.package.cuisineID ==
                                _selectedCuisine!.id)
                        .toList();

                    if (packagesForMeal.length == 0) {
                      return Container();
                    }

                    return Column(
                      children: [
                        Text(
                          meal.name,
                          style: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                        _GroupPackage(
                          packages: packagesForMeal,
                        ),
                      ],
                    );
                  },
                ).toList(),
                Column(
                  children: [
                    ElevatedButton(
                      child: Text('Save changes'),
                      onPressed: () async {
                        _log.info('Getting user');
                        final user = await getUser();

                        /// if the _date (date to add quantity) is same as today's date,
                        /// use widget.now so current time is also taken into account to
                        /// find mealsThatCanBeOrderedNow. If now.date != _date,
                        /// the date is guaranteed to be after now. So, we do not care about
                        /// exact time stamps.
                        final datetimeToCheck =
                            setTimeToZero(widget.now) == _date ? widget.now : _date;

                        final mealsThatCanBeOrderedNow = meals
                            .where((meal) => meal.canOrder(datetimeToCheck!))
                            .map((meal) => meal.id)
                            .toList();

                        final packagesToUpdate = packagesQuantity
                            .where((pq) => mealsThatCanBeOrderedNow
                                .contains(pq.package.mealId))
                            .toList();

                        if (packagesToUpdate.isEmpty) {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Oops'),
                              content:
                                  Text('No packages can be updated right now'),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('Okay'),
                                ),
                              ],
                            ),
                          );

                          return;
                        }

                        setState(() {
                          showCircularProgressIndicator = true;
                        });

                        final response = await http.post(
                          Uri.parse(base_url + '/admin/add_quantity'),
                          headers: {'Content-Type': 'application/json'},
                          body: jsonEncode({
                            'phone_number': user.phoneNumber,
                            'password': user.password,
                            'packages_to_set': packagesToUpdate,
                            'date': DateFormat('yyyy-MM-dd').format(_date!),
                          }),
                        );

                        _log.info('status code: ${response.statusCode}');
                        _log.info('response: ${response.body}');

                        widget.packageForMenuRepository.invalidateCache();

                        setState(() {
                          _future = init();
                          showCircularProgressIndicator = false;
                        });

                        if (response.statusCode != 200) {
                          final body = jsonDecode(response.body);
                          final error = body['error'];
                          if (error == NotAllPackagesCouldBeUpdated) {
                            _showError('Not all packages could be updated');
                            return;
                          }

                          if (error == UnauthorizedAccessError) {
                            _showError('Unauthorized access');
                            return;
                          }

                          if (error == DuplicatesFound) {
                            final duplicates =
                                (body['duplicates'] as List<dynamic>)
                                    .map((value) => value as int)
                                    .toList();

                            _showError('Duplicates found: $duplicates');

                            return;
                          }

                          showDialog(
                            context: context,
                            builder: (_) => SomethingWentWrong(),
                          );

                          return;
                        }

                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            title: Text('Quantity successfully updated'),
                            actions: [
                              TextButton(
                                child: Text('Okay'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            );
          }

          return Container();
        },
      ),
    );
  }

  void _showError(String error) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Oops'),
        content: Text(error),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}

class _GroupPackage extends StatefulWidget {
  final List<PackageQuantity> packages;

  const _GroupPackage({required this.packages});

  @override
  __GroupPackageState createState() => __GroupPackageState();
}

class __GroupPackageState extends State<_GroupPackage> {
  final controllers = <PackageQuantity, TextEditingController>{};

  @override
  void initState() {
    super.initState();
    initControllers();
  }

  @override
  void dispose() {
    super.dispose();

    disposeControllers();
  }

  void disposeControllers() {
    for (final controller in controllers.values) {
      controller.dispose();
    }
  }

  @override
  void didUpdateWidget(_) {
    super.didUpdateWidget(_);

    initControllers();
  }

  void initControllers() {
    widget.packages.sort((a, b) => a.package.id.compareTo(b.package.id));

    for (final packageQuantity in widget.packages) {
      final controller = TextEditingController();
      controllers[packageQuantity] = controller;

      if (packageQuantity.quantity > 0) {
        controller.text = packageQuantity.quantity.toString();
      }

      controller.addListener(() {
        /// tryParse throws error if input is null
        final quantity = int.tryParse(controller.text);
        if (quantity != null) {
          packageQuantity.quantity = quantity;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final groups = widget.packages
        .map((packageQuantity) => packageQuantity.package.group)
        .toSet()
        .toList();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ExpansionPanelList.radio(
        children: groups.map(
          (group) {
            final packages = widget.packages
                .where(
                    (packageQuantity) => packageQuantity.package.group == group)
                .toList();
            packages.sort((a, b) => a.package.id.compareTo(b.package.id));
            return ExpansionPanelRadio(
              canTapOnHeader: true,
              headerBuilder: (context, isExpanded) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    group,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                );
              },
              body: DefaultTextStyle(
                style: TextStyle(fontSize: 17),
                child: Column(
                  children: packages
                      .map(
                        (packageQuantity) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Flexible(
                                flex: 5,
                                fit: FlexFit.tight,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      packageQuantity.package.name,
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.fromLTRB(0, 4, 0, 0),
                                    ),
                                    Text(
                                      packageQuantity.package.description,
                                      style: TextStyle(fontSize: 14),
                                    )
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Text(
                                  packageQuantity.package.quantityAvailable
                                      .toString(),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Container(
                                  width: 30,
                                  child: TextFormField(
                                    validator: (value) =>
                                        int.tryParse(value!) == null
                                            ? 'Quantity must be an integer'
                                            : null,
                                    controller: controllers[packageQuantity],
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
              value: group,
            );
          },
        ).toList(),
      ),
    );
  }
}
