/// Anything that has a name and a count. ReportItem and
/// PackageReport implement Reportable

abstract class Reportable {
  String get name;

  int get count;
}

class ReportItem implements Reportable {
  final String name;
  final int count;
  final int? value;

  const ReportItem(this.name, this.count, [this.value = 0]);

  factory ReportItem.fromJson(Map<String, dynamic> json) {
    return ReportItem(
      json['name'],
      json['count'],
      json['value'],
    );
  }
}

class Quantities {
  final String name;
  final String category;
  final int quantity;
  final int count;
  final int containerID;

  const Quantities(this.name, this.category, this.quantity, this.count, this.containerID);

  factory Quantities.fromJson(Map<String, dynamic> json) {
    return Quantities(
      json['name'],
      json['category'],
      json['quantity'],
      json['count'],
      json['container_id'],
    );
  }
}

class PackageContent {
  final String name;
  final String category;

  const PackageContent(this.name, this.category);
}

class PackageReport implements Reportable {
  final String name, group;
  final int id, count, cuisineID;
  final bool addOn;
  final List<PackageContent> contents;

  PackageReport(
    this.name,
    this.group,
    this.id,
    this.count,
    this.cuisineID,
    this.addOn,
    this.contents,
  );

  factory PackageReport.fromJson(Map<String, dynamic> json) {
    return PackageReport(
      json['name'],
      json['group'],
      json['package_id'],
      json['count'],
      json['cuisine_id'],
      json['add_on'],
      (json['content'] as List)
          .map((e) => PackageContent(e['name'], e['category']))
          .toList(),
    );
  }
}

class CustomerPackage implements Comparable {
  final String customerID,
      phoneNumber,
      firstName,
      lastName,
      package,
      description,
      tower,
      grouping,
      displayName;
  final int id, quantity, mealID, cuisineID, price;
  final bool doorStepDelivery, addOn;

  CustomerPackage(
    this.phoneNumber,
    this.firstName,
    this.lastName,
    this.package,
    this.quantity,
    this.id,
    this.tower,
    this.grouping,
    this.customerID,
    this.mealID,
    this.cuisineID,
    this.price,
    this.description,
    this.doorStepDelivery,
    this.displayName,
    this.addOn,
  );

  factory CustomerPackage.fromJson(Map<String, dynamic> json) {
    return CustomerPackage(
      json['phone_number'],
      json['first_name'],
      json['last_name'],
      json['package'],
      json['quantity'],
      json['package_id'],
      json['tower'],
      json['grouping'],
      json['customer_id'],
      json['meal_id'],
      json['cuisine_id'],
      json['price'],
      json['description'],
      json['door_step_delivery'],
      json['display_name'],
      json['add_on'],
    );
  }

  /// Used to order customers in report
  @override
  int compareTo(other) {
    final _firstNameResult = this.firstName.compareTo(other.firstName);

    if (_firstNameResult != 0) {
      return _firstNameResult;
    }

    final _lastNameResult = this.lastName.compareTo(other.lastName);

    if (_lastNameResult != 0) {
      return _lastNameResult;
    }

    return this.package.compareTo(other.package);
  }

  @override
  String toString() {
    return 'CustomerPackage{firstName: $firstName, lastName: $lastName, package: $package, id: $id, quantity: $quantity}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CustomerPackage &&
          runtimeType == other.runtimeType &&
          customerID == other.customerID &&
          phoneNumber == other.phoneNumber &&
          firstName == other.firstName &&
          lastName == other.lastName &&
          package == other.package &&
          description == other.description &&
          tower == other.tower &&
          grouping == other.grouping &&
          id == other.id &&
          quantity == other.quantity &&
          mealID == other.mealID &&
          cuisineID == other.cuisineID &&
          price == other.price;

  @override
  int get hashCode =>
      customerID.hashCode ^
      phoneNumber.hashCode ^
      firstName.hashCode ^
      lastName.hashCode ^
      package.hashCode ^
      description.hashCode ^
      tower.hashCode ^
      grouping.hashCode ^
      id.hashCode ^
      quantity.hashCode ^
      mealID.hashCode ^
      cuisineID.hashCode ^
      price.hashCode;
}

class ReportTime implements Comparable {
  final int mealID;
  final Duration till;

  ReportTime(this.mealID, this.till);

  factory ReportTime.fromJson(Map<String, dynamic> json) {
    return ReportTime(
      json['meal_id'],
      Duration(seconds: json['till']),
    );
  }

  @override
  int compareTo(other) {
    return this.till.compareTo(other.till);
  }
}
