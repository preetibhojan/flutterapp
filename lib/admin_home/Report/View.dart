import 'package:collection/collection.dart'
    show IterableExtension, IterableNumberExtension;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/admin_home/Report/Repository.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/exceptions.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

import 'Model.dart';

class ReportScreen extends StatefulWidget {
  final MealsRepository mealsRepository;
  final CuisineRepository cuisineRepository;
  final ReportRepository reportRepository;
  final ReportTimeRepository reportTimeRepository;
  final DateTime now;

  const ReportScreen(
    this.mealsRepository,
    this.cuisineRepository,
    this.reportRepository,
    this.reportTimeRepository,
    this.now,
  );

  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<ReportScreen> {
  late Future _future;

  static final _log = Logger('Admin Home');

  late List<Meal> _meals;
  late List<ReportTime> _reportTime;

  late DateTime _date;

  late Meal _selectedMeal;

  bool showCircularProgressIndicator = true;

  @override
  void initState() {
    super.initState();

    _future = initFields();
  }

  Future initFields() async {
    _meals = await widget.mealsRepository.meals;
    _reportTime = await widget.reportTimeRepository.reportTime;

    _meals.sort();
    _reportTime.sort();

    final report = _reportTime.firstWhereOrNull(
      /// first report time whose till date is yet to come
      (reportTime) => widget.now.isBefore(
        setTimeToZero(widget.now).add(reportTime.till),
      ),
    );

    _date = widget.now;

    if (report == null) {
      _date = widget.now.nextWorkingDay;
      _selectedMeal = _meals[0];
    } else {
      _selectedMeal = _meals.firstWhere((meal) => meal.id == report.mealID);
    }

    return await reportsFor(_selectedMeal, _date);
  }

  Future reportsFor(Meal meal, DateTime date) async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    final summary = await widget.reportRepository.summary(meal, date);

    final customerWisePackages =
        await widget.reportRepository.allCustomerWisePackages(meal, date);

    setState(() {
      showCircularProgressIndicator = false;
    });

    return [summary, customerWisePackages];
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: showCircularProgressIndicator,
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          _log.info(snapshot);

          if (snapshot.hasError) {
            if (snapshot.error is UnauthorizedAccess) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text('You cannot generate report'),
                    actions: [
                      TextButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              });
            } else {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });
            }
          }

          if (snapshot.hasData) {
            final summary = (snapshot.data as List)[0] as Summary;

            summary.quantities
              ..sort((a, b) => a.containerID - b.containerID);

            final categories = summary.quantities
                .map((e) => e.category)
                .toSet()
                .toList();

            final groups = summary.packages
                .map((package) => package.group)
                .toSet()
                .toList();

            groups.sort();

            var normalPackages = summary.packages
                .where((p) => !p.addOn)
                .map((package) => package.count);
            final total = normalPackages.length != 0
                ? normalPackages.reduce((a, b) => a + b)
                : 0;

            final customerPackages =
                (snapshot.data as List)[1] as List<CustomerPackage>;

            final towers =
                customerPackages.map((cp) => cp.tower).toSet().toList();

            return Padding(
              padding: const EdgeInsets.all(16),
              child: ListView(
                children: [
                  Center(
                    child: TextButton(
                      child: Text(
                        DateFormat.yMMMd().format(_date),
                        style:
                            Theme.of(context).textTheme.headlineSmall!.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      onPressed: () async {
                        final temp = await showDatePicker(
                          context: context,
                          initialDate: _date,
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2100),
                          builder: (BuildContext context, Widget? child) {
                            return Theme(
                              data: ThemeData.dark().copyWith(
                                colorScheme: ColorScheme.fromSwatch(
                                  primaryColorDark: primaryColor,
                                ).copyWith(onSurface: Colors.white),
                                dialogBackgroundColor: secondaryColor,
                                iconTheme: IconThemeData(
                                  color: Colors.white,
                                ),
                              ),
                              child: child!,
                            );
                          },
                        );

                        if (temp != null) {
                          setState(() {
                            _date = temp;

                            _future = reportsFor(_selectedMeal, _date);
                          });
                        }
                      },
                    ),
                  ),
                  DropdownButtonFormField<Meal>(
                    items: _meals
                        .map(
                          (meal) => DropdownMenuItem<Meal>(
                            child: Text(meal.name),
                            value: meal,
                          ),
                        )
                        .toList(),
                    onChanged: (meal) async {
                      _future = reportsFor(meal!, _date);

                      setState(() {
                        _selectedMeal = meal;
                      });
                    },
                    value: _selectedMeal,
                    decoration: InputDecoration(
                      labelText: 'Meal',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                  ),
                  Divider(),
                  Center(
                    child: Text(
                      'Quantity Required',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Divider(),
                  ...categories.map(
                    (category) {
                      final quantities = summary.quantities
                          .where((e) => e.category == category)
                          .toList();
                      return Column(
                        children: [
                          Row(
                            children: [
                              Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Text(
                                  category,
                                  style: TextStyle(fontSize: 18),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                      children: quantities
                                          .map(
                                            (e) => Text(
                                              e.name,
                                              style: TextStyle(fontSize: 18),
                                            ),
                                      )
                                          .toList(),
                                    ),
                                    Spacer(),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: quantities
                                          .map(
                                            (e) => Text(
                                          '${e.quantity}',
                                          style: TextStyle(fontSize: 18),
                                        ),
                                      )
                                          .toList(),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 4),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              'Total: ${quantities.map((e) => e.quantity * e.count).sum}',
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.right,
                            ),
                          ),
                          SizedBox(height: 4),
                          Divider(),
                        ],
                      );
                    },
                  ).toList(),
                  Center(
                    child: Text(
                      'Package Wise Quantity',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Divider(),
                  ExpansionPanelList.radio(
                    initialOpenPanelValue: groups.isNotEmpty ? groups[0] : null,
                    children: groups.map((group) {
                      final packagesToDisplay = summary.packages
                          .where((package) => package.group == group)
                          .toList();

                      final total = packagesToDisplay
                          .map((package) => package.count)
                          .reduce((a, b) => a + b);

                      return ExpansionPanelRadio(
                        canTapOnHeader: true,
                        body: Padding(
                          padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                          child: Column(
                            children: [
                              ...packagesToDisplay.map(
                                (item) {
                                  return ExpansionPanelList.radio(
                                    children: [
                                      ExpansionPanelRadio(
                                        canTapOnHeader: true,
                                        value: item,
                                        headerBuilder: (context, _) => Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              12, 0, 12, 0),
                                          child: Row(
                                            children: [
                                              Text(
                                                item.name,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleLarge,
                                              ),
                                              Spacer(),
                                              Text(
                                                item.count.toString(),
                                                style: TextStyle(fontSize: 18),
                                              ),
                                            ],
                                          ),
                                        ),
                                        body: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              12, 0, 12, 0),
                                          child: Column(
                                            children: item.contents
                                                .map(
                                                  (e) => Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(0, 0, 0, 4),
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                          e.category,
                                                          style: TextStyle(
                                                              fontSize: 18),
                                                        ),
                                                        Spacer(),
                                                        Text(
                                                          e.name,
                                                          style: TextStyle(
                                                              fontSize: 18),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                                .toList(),
                                          ),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              ).toList(),
                              SizedBox(height: 8),
                              Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'Total :   $total',
                                  style: TextStyle(fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                        ),
                        value: group,
                        headerBuilder: (BuildContext context, bool isExpanded) {
                          return Padding(
                            padding: const EdgeInsets.fromLTRB(12, 4, 0, 0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                group,
                                style: Theme.of(context).textTheme.titleLarge,
                              ),
                            ),
                          );
                        },
                      );
                    }).toList(),
                  ),
                  Divider(),
                  Center(
                    child: Text(
                      'Customer Wise Package Detail',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Divider(),
                  ...towers.map(
                    (tower) {
                      final packages = customerPackages
                          .where((cp) => cp.tower == tower)
                          .toList();

                      final groupings =
                          packages.map((p) => p.grouping).toSet().toList();

                      groupings.sort((a, b) => b.compareTo(a));

                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              tower,
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                          ),
                          ...groupings.map(
                            (g) {
                              final temp = packages
                                  .where((p) => p.grouping == g && !p.addOn)
                                  .map((e) => e.quantity);

                              final quantity = temp.isEmpty
                                  ? 0
                                  : temp.reduce((a, b) => a + b);
                              return Column(
                                children: [
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 10, 0, 4),
                                        child: Text(
                                          g,
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleLarge,
                                        ),
                                      ),
                                      Spacer(),
                                      Text(
                                        quantity.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                    ],
                                  ),
                                  ...packages
                                      .where((p) => p.grouping == g)
                                      .map(
                                        (customer) => Row(
                                          children: [
                                            Flexible(
                                              flex: 4,
                                              fit: FlexFit.tight,
                                              child: Text(
                                                '${customer.firstName} ${customer.lastName.substring(0, 1)}',
                                                style: TextStyle(fontSize: 18),
                                              ),
                                            ),
                                            Flexible(
                                              flex: 3,
                                              fit: FlexFit.tight,
                                              child: Text(
                                                customer.displayName,
                                                style: TextStyle(fontSize: 18),
                                              ),
                                            ),
                                            Flexible(
                                              flex: 1,
                                              fit: FlexFit.tight,
                                              child: customer.doorStepDelivery
                                                  ? Icon(Icons
                                                      .door_front_door_outlined)
                                                  : Container(),
                                            ),
                                            Flexible(
                                              flex: 1,
                                              fit: FlexFit.tight,
                                              child: Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  customer.quantity.toString(),
                                                  style:
                                                      TextStyle(fontSize: 18),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                      .toList(),
                                ],
                              );
                            },
                          ).toList(),
                        ],
                      );
                    },
                  ).toList(),
                  if (summary.packages.length != 0)
                    Column(
                      children: [
                        Divider(),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            'Total :   $total',
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
