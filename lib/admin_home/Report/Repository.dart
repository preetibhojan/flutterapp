import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/exceptions.dart';
import 'package:preetibhojan/common/meals/Model.dart';

import 'Model.dart';

class Summary {
  final List<PackageReport> packages;
  final List<Quantities> quantities;

  Summary(this.packages, this.quantities);
}

abstract class ReportRepository {
  Future<Summary> summary(Meal meal, DateTime date);

  Future<List<CustomerPackage>> allCustomerWisePackages(
      Meal meal, DateTime date);

  Future<List<CustomerPackage>> customerWisePackagesForDeliveryExecutive(
      Meal meal, DateTime date);
}

class NetworkReportRepository implements ReportRepository {
  static final _log = Logger('Report Repository');

  static const UNAUTHORIZED_ACCESS = 3;

  @override
  Future<Summary> summary(Meal meal, DateTime date) async {
    _log.info('getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/admin/report/summary2'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'meal_id': meal.id,
        'date': DateFormat('yyyy-MM-dd').format(date),
      }),
    );

    _log.info('response code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    final json = jsonDecode(response.body);
    if (response.statusCode != 200) {
      final error = json['error'];

      if (error == UNAUTHORIZED_ACCESS) {
        throw UnauthorizedAccess();
      }

      throw Exception();
    }

    final packages = (json['packages'] as List)
        .map((e) => PackageReport.fromJson(e))
        .toList();

    final quantities = (json['quantities'] as List)
        .map((e) => Quantities.fromJson(e))
        .toList();
    return Summary(packages, quantities);
  }

  @override
  Future<List<CustomerPackage>> allCustomerWisePackages(
      Meal meal, DateTime date) async {
    _log.info('getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/admin/report/customer_wise_packages'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'meal_id': meal.id,
        'date': DateFormat('yyyy-MM-dd').format(date),
      }),
    );

    _log.info('response code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      final error = jsonDecode(response.body)['error'];

      if (error == UNAUTHORIZED_ACCESS) {
        throw UnauthorizedAccess();
      }

      throw Exception();
    }

    return (jsonDecode(response.body) as List)
        .map((json) => CustomerPackage.fromJson(json))
        .toList();
  }

  @override
  Future<List<CustomerPackage>> customerWisePackagesForDeliveryExecutive(
      Meal meal, DateTime date) async {
    _log.info('getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url +
          '/admin/report/customer_wise_packages_for_delivery_executive'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'meal_id': meal.id,
        'date': DateFormat('yyyy-MM-dd').format(date),
      }),
    );

    _log.info('response code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      final error = jsonDecode(response.body)['error'];

      if (error == UNAUTHORIZED_ACCESS) {
        throw UnauthorizedAccess();
      }

      throw Exception();
    }

    return (jsonDecode(response.body) as List)
        .map((json) => CustomerPackage.fromJson(json))
        .toList();
  }
}

abstract class ReportTimeRepository {
  Future<List<ReportTime>> get reportTime;
}

class NetworkReportTimeRepository implements ReportTimeRepository {
  List<ReportTime>? _cache;
  static final _log = Logger('Report Repository');

  @override
  Future<List<ReportTime>> get reportTime async {
    _log.info('Getting report time');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache!;
    }

    final response = await http.get(
      Uri.parse('$base_url/admin/report_time'),
    );

    _log.info('response code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('Could not get report times');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => ReportTime.fromJson(json))
        .toList();

    return _cache!;
  }
}
