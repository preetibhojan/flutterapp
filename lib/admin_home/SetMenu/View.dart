import 'dart:convert';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/cuisines/Model.dart';
import 'package:preetibhojan/common/cuisines/Repository.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/menu/Model.dart';
import 'package:preetibhojan/common/menu/Repositories.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/util/datetime.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class SetMenu extends StatefulWidget {
  final MenuRepository menuRepository;
  final MealsRepository mealsRepository;
  final CuisineRepository cuisineRepository;

  final DateTime now;

  const SetMenu(
    this.menuRepository,
    this.mealsRepository,
    this.cuisineRepository,
    this.now,
  );

  @override
  _SetMenuState createState() => _SetMenuState();
}

class _SetMenuState extends State<SetMenu> {
  late Future _future;

  late DateTime _date;

  final Map<Meal, Map<String, TextEditingController>> _controller = {};

  @override
  void initState() {
    super.initState();

    _date = setTimeToZero(widget.now.add(Duration(days: 1)));
    _future = init();
  }

  bool showCircularProgressIndicator = true;

  late List<Menu> _menu;
  late List<Meal> _meals;
  late List<Cuisine> _cuisines;

  late Cuisine _selectedCuisine;

  Future init() async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    _menu = await widget.menuRepository.menu;
    _meals = await widget.mealsRepository.meals;
    _meals.sort();

    _cuisines = await widget.cuisineRepository.cuisines;
    _selectedCuisine = _cuisines[0];

    initControllers();

    setState(() {
      showCircularProgressIndicator = false;
    });

    return true;
  }

  /// Initializes _controllers variables. Assumes, _meals, _menu and
  /// _selectedCuisine is initialized
  void initControllers() {
    for (final meal in _meals) {
      final sabji = TextEditingController();
      final daal = TextEditingController();
      final rice = TextEditingController();
      final sweet = TextEditingController();

      final temp = _menu.length != 0
          ? _menu.firstWhereOrNull((menu) =>
              menu.date == _date &&
              menu.mealID == meal.id &&
              menu.cuisineID == _selectedCuisine.id)
          : null;

      if (temp != null) {
        sabji.text = temp.sabji;
        daal.text = temp.daal ?? "";
        rice.text = temp.rice ?? "";
        sweet.text = temp.sweet ?? "";
      }

      _controller[meal] = {
        'sabji': sabji,
        'daal': daal,
        'rice': rice,
        'sweet': sweet,
      };
    }
  }

  void resetControllers() {
    for (final meal in _meals) {
      final temp = _menu.length != 0
          ? _menu.firstWhereOrNull((menu) =>
              menu.date == _date &&
              menu.mealID == meal.id &&
              menu.cuisineID == _selectedCuisine.id)
          : null;

      if (temp != null) {
        _controller[meal]!['sabji']!.text = temp.sabji;
        _controller[meal]!['daal']!.text = temp.daal ?? '';
        _controller[meal]!['rice']!.text = temp.rice ?? '';
        _controller[meal]!['sweet']!.text = temp.sweet ?? '';

        continue;
      }

      _controller[meal]!['sabji']!.text = '';
      _controller[meal]!['daal']!.text = '';
      _controller[meal]!['rice']!.text = '';
      _controller[meal]!['sweet']!.text = '';
    }
  }

  @override
  void dispose() {
    super.dispose();
    for (final meal in _meals) {
      for (final controller in _controller[meal]!.values) {
        controller.dispose();
      }
    }
  }

  static final _log = Logger('Set Menu');

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldAdmin(
      showCircularProgressIndicator: showCircularProgressIndicator,
      body: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          _log.info(snapshot);

          if (snapshot.hasError) {
            _log.severe(
              'something went wrong',
              snapshot.error,
              StackTrace.current,
            );

            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });
          }

          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
              child: ListView(
                children: [
                  Center(
                    child: TextButton(
                      child: Text(
                        DateFormat.yMMMMd().format(_date),
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      onPressed: () async {
                        final temp = await showDatePicker(
                          context: context,
                          initialDate: _date,
                          firstDate: widget.now,
                          lastDate: DateTime(2100),
                        );

                        if (temp != null) {
                          setState(() {
                            _date = temp;
                            resetControllers();
                          });
                        }
                      },
                    ),
                  ),
                  DropdownButtonFormField<Cuisine>(
                    items: _cuisines
                        .map(
                          (cuisine) => DropdownMenuItem<Cuisine>(
                            child: Text(
                              cuisine.name,
                              style: TextStyle(fontSize: 17),
                            ),
                            value: cuisine,
                          ),
                        )
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        _selectedCuisine = value!;
                        resetControllers();
                      });
                    },
                    value: _selectedCuisine,
                    hint: Text('Cuisine'),
                    decoration: InputDecoration(
                      labelText: 'Cuisine',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4),
                  ),
                  ..._meals
                      .map(
                        (meal) => Padding(
                          padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                          child: Form(
                            child: Column(
                              children: [
                                Text(
                                  meal.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5!
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                                TextFormField(
                                  controller: _controller[meal]!['sabji'],
                                  decoration:
                                      InputDecoration(labelText: 'Sabji'),
                                ),
                                TextFormField(
                                  controller: _controller[meal]!['daal'],
                                  decoration:
                                      InputDecoration(labelText: 'Daal'),
                                ),
                                TextFormField(
                                  controller: _controller[meal]!['rice'],
                                  decoration:
                                      InputDecoration(labelText: 'Rice'),
                                ),
                                TextFormField(
                                  controller: _controller[meal]!['sweet'],
                                  decoration:
                                      InputDecoration(labelText: 'Sweet'),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                      .toList(),
                  Column(
                    children: [
                      ElevatedButton(
                        child: Text('Set Menu'),
                        onPressed: onSetMenu,
                      ),
                    ],
                  ),
                ],
              ),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  void onSetMenu() async {
    final itemsToSend = [];

    for (final meal in _meals) {
      final atLeastOneValueSet = _controller[meal]!
              .values
              .where((controller) => controller.text.trim() != '')
              .length !=
          0;
      if (atLeastOneValueSet) {
        final controller = _controller[meal]!;
        itemsToSend.add({
          'meal_id': meal.id,
          'cuisine_id': _selectedCuisine.id,
          'date': DateFormat('yyyy-MM-dd').format(_date),
          'sabji': controller['sabji']!.text.trim(),
          'daal': controller['daal']!.text.trim(),
          'rice': controller['rice']!.text.trim(),
          'sweet': controller['sweet']!.text.trim(),
        });
      }
    }

    if (itemsToSend.length == 0) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please add menu for at least one meal'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      showCircularProgressIndicator = true;
    });

    _log.info('Getting user');
    final user = await getUser();

    final response = await http.post(
      Uri.parse(base_url + '/admin/set_menu'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': user.phoneNumber,
        'password': user.password,
        'menu': itemsToSend,
      }),
    );

    setState(() {
      for (final item in itemsToSend) {
        int index = _menu.indexOfElementWhere((m) => m.date == _date &&
            m.mealID == item['meal_id'] &&
            m.cuisineID == item['cuisine_id']);

        if (index > -1) {
          _menu[index] = Menu.fromJson(item);
        } else {
          _menu.add(Menu.fromJson(item));
        }
      }
      showCircularProgressIndicator = false;
    });

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      final error = jsonDecode(response.body)["error"];

      if (error == UnauthorizedAccessError) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text('Unauthorized access'),
            actions: [
              TextButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }

      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Menu set'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}

typedef bool Predicate<T>(T t);

extension<T> on List<T> {
  int indexOfElementWhere(Predicate predicate) {
    for (int i = 0; i < length; i++) {
      if (predicate(this[i])) {
        return i;
      }
    }

    return -1;
  }
}