import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/misc.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

enum Stage {
  SendOTP,
  ResetPassword,
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final _phoneNumber = TextEditingController(),
      _email = TextEditingController(),
      _otp = TextEditingController(),
      _newPassword = TextEditingController();

  @override
  void dispose() {
    _phoneNumber.dispose();
    _email.dispose();
    super.dispose();
  }

  Stage _stage = Stage.SendOTP;

  static final _log = Logger('Forgot password');

  bool _loading = false;
  bool _passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: _loading,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: _stage == Stage.SendOTP,
              child: Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _phoneNumber,
                      decoration: InputDecoration(
                        labelText: 'Phone Number',
                        icon: Icon(Icons.phone),
                      ),
                      validator: (str) => isPhoneNumberValid(str!.trim()),
                      keyboardType: TextInputType.number,
                    ),
                    TextFormField(
                      controller: _email,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        icon: Icon(Icons.email),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: (str) => isEmailValid(str!.trim()),
                    ),
                    ElevatedButton(
                      child: Text('Send OTP'),
                      onPressed: _onSendOTP,
                    )
                  ],
                ),
              ),
            ),
            Visibility(
              visible: _stage == Stage.ResetPassword,
              child: Column(
                children: [
                  TextFormField(
                    controller: _otp,
                    decoration: InputDecoration(
                      labelText: 'OTP',
                      icon: Icon(Icons.security_rounded),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                  TextFormField(
                    controller: _newPassword,
                    decoration: InputDecoration(
                      labelText: 'New Password',
                      icon: Icon(Icons.lock),
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility_off
                              : Icons.visibility,
                          semanticLabel: _passwordVisible
                              ? 'hide password'
                              : 'show password',
                        ),
                        onPressed: () {
                          setState(
                            () {
                              /// Copied from stack overflow. a xor 1 is same as !a. i.e. short hand for a = !a
                              _passwordVisible ^= true;
                            },
                          );
                        },
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: !_passwordVisible,
                  ),
                  ElevatedButton(
                    child: Text('Reset Password'),
                    onPressed: _onResetPassword,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onSendOTP() async {
    final phoneNumber = _phoneNumber.text.trim();
    final email = _email.text.trim();

    if ((isPhoneNumberValid(phoneNumber) != null) ||
        (isEmailValid(email) != null)) {
      _showCorrectErrorsBeforeContinuing();

      return;
    }

    setState(() {
      _loading = true;
    });

    _log.info('Sending post request to send_forgot_password_email');
    final response = await http.post(
      Uri.parse(base_url + '/send_forgot_password_email'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': _phoneNumber.text.trim(),
        'email': _email.text.trim(),
      }),
    );

    setState(() {
      _loading = false;
    });

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('OTP sent on your email'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              setState(() {
                _stage = Stage.ResetPassword;
              });

              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  void _showCorrectErrorsBeforeContinuing() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Oops'),
        content: Text('Please correct the errors before continuing'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  Future<void> _onResetPassword() async {
    final otp = _otp.text.trim();
    final password = _newPassword.text.trim();

    if (isPasswordValid(password) != null) {
      _showCorrectErrorsBeforeContinuing();
    }

    setState(() {
      _loading = true;
    });

    _log.info('Sending post request to reset_password');
    final response = await http.post(
      Uri.parse(base_url + '/reset_password'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'reset_token': otp,
        'new_password': password,
      }),
    );

    setState(() {
      _loading = false;
    });

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Oops'),
          content: Text('OTP is incorrect or has expired'),
          actions: [
            TextButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    final storage = FlutterSecureStorage();
    await storage.write(key: 'password', value: password);

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Password reset successful'),
        actions: [
          TextButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        ],
      ),
    );
  }
}
