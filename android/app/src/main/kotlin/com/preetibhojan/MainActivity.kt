package com.preetibhojan

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.core.app.NotificationCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

const val HIGH_PRIORITY_NOTIFICATIONS = "com.preetibhojan/delivery"
const val OTHER_NOTIFICATIONS = "com.preetibhojan/other"

class MainActivity : FlutterActivity() {
    object Channels {
        const val NOTIFICATION = "custom notification channel"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createNotificationChannels()
    }

    private fun createNotificationChannels() {
        createDeliveryChannel()
        createNormalNotificationChannel()
    }

    private fun createNormalNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Other Notifications"
            val descriptionText = "Other Notifications"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(OTHER_NOTIFICATIONS, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createDeliveryChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "High Priority Notifications"
            val descriptionText = "Notifications to pick up your order"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(HIGH_PRIORITY_NOTIFICATIONS, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }


    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, Channels.NOTIFICATION)
                .setMethodCallHandler { call, result ->
                    when (call.method) {
                        "displayNotification" -> {
                            val title = call.argument<String>("title")
                            val body = call.argument<String>("body")
                            val highPriority = call.argument<Boolean>("highPriority")

                            displayNotification(title, body!!, highPriority!!)
                        }
                        "else" -> result.notImplemented()
                    }

                }
    }

    private var notificationID = 1

    private fun displayNotification(title: String?, body: String, highPriority: Boolean) {
        // Create an explicit intent for an Activity in your app
        val intent = Intent(this.applicationContext, MainActivity::class.java)

        val pendingIntent: PendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                0,
        )

        val componentName = intent.resolveActivity(packageManager)

        if (componentName.packageName != "com.preetibhojan" || componentName.className != "com.preetibhojan.MainActivity") {
            throw Exception("component name's package or class is unexpected")
        }

        if (highPriority) {
            val builder = NotificationCompat.Builder(this.applicationContext, HIGH_PRIORITY_NOTIFICATIONS)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setAutoCancel(true)
                    .setFullScreenIntent(pendingIntent, true)
                    .setCategory(NotificationCompat.CATEGORY_EVENT)

            showNotification(builder)

        } else {
            val builder = NotificationCompat.Builder(this.applicationContext, OTHER_NOTIFICATIONS)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_EVENT)

            showNotification(builder)
        }
    }

    private fun showNotification(builder: NotificationCompat.Builder) {
        val notificationManager = this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(notificationID, builder.build())
        notificationID++
    }
}
